# micoolta
micoolta is a compiler for COOL (Classroom Object-Oriented Language). It will produce Java code from COOL sources file.

### Installation
```sh
$ ant compile jar
```

### Execution
```sh
$ java -jar build/jar/micoolta.jar -h
```
It will produce:
```sh
$ java -jar build/jar/micoolta.jar -h
usage: micoolta
 -h                   Print this help.
 -ie                  Ignore extensions of output file (Java file) and sources file (COOL file).
 -main <Main file>    Specify Cool file which contains "Main" class.
 -o <Output file>     Write Java code in <Output file> (specify Java extension or option "-ie" to ignore it).
 -package <Package>   Specify package of the Java class.
```

### Examples
COOL file:

(Arith.cl)
```java
class Arith {
    add (a : Int, b : Int) : Int {
        a + b
    };
    
    sub (a : Int, b : Int) : Int {
        a - b
    };
    
    mul (a : Int, b : Int) : Int {
        a * b
    };
    
    div (a : Int, b : Int) : Int {
        a / b
    };
};
```

(Main.cl)
```java
class Main inherits IO {
    main () : Object {
        {
            let a : Int, a : Int<-12, b : Int, flag : Bool <- true, char_ : String, operator : String in
            {
                while (flag) loop
                    {
                        out_string ("Insert first number:  ");
                        a <- in_int ();
                        out_string ("Insert operator (+, -, *, /):  ");
                        operator <- in_string ();
                        out_string ("Insert second number:  ");
                        b <- in_int ();
                        
                        if operator = "+" then
                            out_int (new Arith.add (a, b))
                        else
                            if operator = "-" then
                                out_int (new Arith.sub (a, b))
                            else
                                if operator = "*" then
                                    out_int (new Arith.mul (a, b))
                                else
                                    if operator = "/" then
                                        out_int (new Arith.div (a, b))
                                    else
                                        {
                                            out_string ("\nUnknown operator!");
                                            abort ();
                                        }
                                    fi
                                fi
                            fi
                        fi;
                        
                        out_string ("\nQuit ('y' to quit)? ");
                        char_ <- in_string ();
                        
                        if char_ = "y" then
                            {
                                flag <- false;
                                out_string ("\nQuit!");
                            }
                        else
                            flag <- true
                        fi;
                    }
                pool;
            };
        }
    };
};
```

with the command:

```sh
$ java -jar build/jar/micoolta.jar -o Calc.java examples/cool/calculator/Arith.cl examples/cool/calculator/Main.cl
```

the compiler generates:

```java
// File generated on 25/05/2015 17:19:26

import java.util.Scanner;


class Object extends java.lang.Object {
    public Object () {
    }
    
    public Object abort () {
        System.out.println ("Abort ().");
        System.exit (-1);
        
        return this;
    }
    
    public String type_name () {
        return new String (this.getClass ().getSimpleName ());
    }
    
    public Object copy () {
        return this.copy ();
    }
}

class String extends Object {
    public java.lang.String value;
    
    public String () {
        this.value = "";
    }
    
    public String (java.lang.String value) {
        this.value = value;
    }
    
    public Integer length () {
        return new Integer (this.value.length ());
    }
    
    public String concat (String s) {
        return new String (this.value.concat (s.value));
    }
    
    // substr returns the substring of its self parameter beginning at position i with length l (string positions are numbered beginning at 0)
    public String substr (Integer i, Integer l) {
        if ((i < 0 || l < 0) ||
            (i + l > this.length ()) ||
            (i > this.length () || l > this.length ()))
            throw new RuntimeException ("substr (...), illegal use of indexes (i=" + i + ", l=" + l + ", len= " + this.length () + ")");
        
        java.lang.String res = "";
        
        for (int k = i; k < (i + l); k++)
            res += new Character (this.value.charAt (k)).toString ();
        
        return new String (res);
    }
    @Override
    public boolean equals (java.lang.Object obj) {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (this.getClass () != obj.getClass ()) {
            if (obj instanceof Boolean)
                return new Boolean (this.value).booleanValue () == ((Boolean) obj).booleanValue ();
            if (obj instanceof Integer)
                return new Integer (this.value).intValue () == ((Integer) obj).intValue ();
            
            return false;
        }
        
        String other = (String) obj;
        
        if (this.value == null) {
            if (other.value != null)
                return false;
        } 
        else if (!this.value.equals (other.value))
            return false;
        
        return true;
    }
}

class IO extends Object {
    public IO out_string (String x) {
        System.out.print (x.value);
        
        return this;
    }
    
    public IO out_int (Integer x) {
        System.out.print (x);
        
        return this;
    }
    
    @SuppressWarnings("resource")
    public String in_string () {
        return new String (new Scanner (System.in).nextLine ());
    }
    
    @SuppressWarnings("resource")
    public Integer in_int () {
        return new Integer (new Scanner (System.in).nextInt ());
    }
}

class Arith extends Object {
   // Attributes not specified

   // Global variables - Start
   protected Integer Arith_add_a_0;
   protected Integer Arith_add_b_0;
   protected Integer Arith_sub_a_0;
   protected Integer Arith_sub_b_0;
   protected Integer Arith_mul_a_0;
   protected Integer Arith_mul_b_0;
   protected Integer Arith_div_a_0;
   protected Integer Arith_div_b_0;
   // Global variables - End


   // Default constructor not specified


   // Constructor Methods not specified

   // Methods - Start
   protected Integer add (Integer a, Integer b) {
      this.Arith_add_a_0 = a;
      this.Arith_add_b_0 = b;
      return (Integer) new Integer (Arith_add_a_0 + Arith_add_b_0);
   }
   protected Integer sub (Integer a, Integer b) {
      this.Arith_sub_a_0 = a;
      this.Arith_sub_b_0 = b;
      return (Integer) new Integer (Arith_sub_a_0 - Arith_sub_b_0);
   }
   protected Integer mul (Integer a, Integer b) {
      this.Arith_mul_a_0 = a;
      this.Arith_mul_b_0 = b;
      return (Integer) new Integer (Arith_mul_a_0 * Arith_mul_b_0);
   }
   protected Integer div (Integer a, Integer b) {
      this.Arith_div_a_0 = a;
      this.Arith_div_b_0 = b;
      return (Integer) new Integer (Arith_div_a_0 / Arith_div_b_0);
   }
   // Methods - End

   // Private methods not specified
}

class Main extends IO {
   // Attributes not specified

   // Global variables - Start
   protected Integer Main_main_a_0;
   protected Integer Main_main_b_0;
   protected Boolean Main_main_flag_0;
   protected String Main_main_char__0;
   protected String Main_main_operator_0;
   // Global variables - End


   // Default constructor - Start
   public Main () {
      this.Main_main_a_0 = new Integer (0);
      this.Main_main_b_0 = new Integer (0);
      this.Main_main_flag_0 = new Boolean (false);
      this.Main_main_char__0 = new String ();
      this.Main_main_operator_0 = new String ();
   }


   // Constructor Methods not specified

   // Methods - Start
   protected java.lang.Object main () {
      {
         Integer Main_main_a_0 = new Integer (0);
         Main_main_a_0 = new Integer (12);
         Integer Main_main_b_0 = new Integer (0);
         Boolean Main_main_flag_0 = new Boolean (false);
         Main_main_flag_0 = new Boolean (true);
         String Main_main_char__0 = new String ();
         String Main_main_operator_0 = new String ();
         {
            while ((Main_main_flag_0)) {
               {
                  out_string (new String ("Insert first number:  "));
                  Main_main_a_0 = in_int ();
                  out_string (new String ("Insert operator (+, -, *, /):  "));
                  Main_main_operator_0 = in_string ();
                  out_string (new String ("Insert second number:  "));
                  Main_main_b_0 = in_int ();
                  if (Main_main_operator_0.equals (new String ("+"))) {
                     out_int ((new Arith ()).add (Main_main_a_0, Main_main_b_0));
                  }
                  else {
                     if (Main_main_operator_0.equals (new String ("-"))) {
                        out_int ((new Arith ()).sub (Main_main_a_0, Main_main_b_0));
                     }
                     else {
                        if (Main_main_operator_0.equals (new String ("*"))) {
                           out_int ((new Arith ()).mul (Main_main_a_0, Main_main_b_0));
                        }
                        else {
                           if (Main_main_operator_0.equals (new String ("/"))) {
                              out_int ((new Arith ()).div (Main_main_a_0, Main_main_b_0));
                           }
                           else {
                              {
                                 out_string (new String ("\nUnknown operator!\n"));
                                 abort ();
                              }
                           }
                        }
                     }
                  }
                  out_string (new String ("\nQuit ('y' to quit)? "));
                  Main_main_char__0 = in_string ();
                  if (Main_main_char__0.equals (new String ("y"))) {
                     {
                        Main_main_flag_0 = new Boolean (false);
                        out_string (new String ("\nQuit!\n"));
                     }
                  }
                  else {
                     Main_main_flag_0 = new Boolean (true);
                  }
               }
            }
            return new Object ();
         }
      }
   }
   // Methods - End

   // Private methods not specified
}

class Calc extends Object {
   // Attributes not specified

   // Global variable not specified

   // Default constructor not specified


   // Constructor Methods not specified

   // Methods - Start
   public static void main (java.lang.String[] args) {
      Main m = new Main ();
      m.main ();
   }
   // Methods - End

   // Private methods not specified
}
```

### Summary of commands

![commands]
(https://bytebucket.org/mmicu/micoolta/raw/f993e6dd49712b23b772387e498979e9201ab4d8/images/commands.png?token=7526d95370387e94f7cbe7bcf5207a4ab0f96823)



### Authors
  - mmicu;
  - tala.
