package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.antlr.v4.runtime.ANTLRFileStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;

import utils.CommandLineUtils;
import utils.FileUtils;
import utils.GenericUtils;
import utils.IOUtils;
import utils.StringUtils;
import utils.TestUtils;
import utils.IOUtils.Color;

import cool.data.structure.object.CoolClass;
import cool.data.structure.object.CoolMethod;
import cool.data.structure.object.CoolParameter;
import cool.data.structure.types.Type;

import cool.error.ErrorMessage;

import cool.listeners.error.CoolErrorListener;
import cool.listeners.walkers.ScopeDefinitionPhase;
import cool.listeners.walkers.ScopeResolvingPhase;
import cool.listeners.walkers.SyntaxTranslationPhase;
import cool.listeners.walkers.TypeCheckingListener;
import cool.listeners.walkers.WalkerListenerSemanticAnalysis;

import cool.scope.ClassScope;
import cool.scope.Scope;

import antlr.coolLexer;
import antlr.coolParser;

public class micoolta
{
    private static final boolean DEBUG = false;
    
    private static Options apacheCliOptions = new Options ();
    
    private static ArrayList<CoolClass> coolClasses;
    
    private static Map<String, String> mapInstanceParentClass;
    
    public static boolean ERROR = false;
    
    
    private static void printHelp ()
    {
        HelpFormatter formatter = new HelpFormatter ();
        
        formatter.setWidth (120);
        
        formatter.printHelp ("micoolta", micoolta.apacheCliOptions);
    }
    
    
    @SuppressWarnings("static-access")
    private static void setupOptions (String[][] options)
    {
        for (int k = 0; k < options.length; k++) {
            if (options[k][2].length () == 0)
                micoolta.apacheCliOptions.addOption (options[k][0], false, options[k][1]);
            else
                micoolta.apacheCliOptions.addOption (
                    OptionBuilder.withArgName (options[k][2])
                        .hasArg ()
                        .withDescription (options[k][1])
                        .create (options[k][0])
                );
        }
    }

    
    private static void setupStandardClasses ()
    {
        // Setup standard classes
        coolClasses = new ArrayList<CoolClass> ();
        CoolClass c;
        CoolMethod m;
        
        // --- Object class
        CoolClass o = new CoolClass ("Object");
        
        // Method abort in class Object
        m = new CoolMethod ("abort");
        m.setReturnValue (new Type ("Object"));
        o.addMethod (m);
        
        // Method type_name in class Object
        m = new CoolMethod ("type_name");
        m.setReturnValue (new Type ("String"));
        o.addMethod (m);
        
        // Method copy in class Object
        m = new CoolMethod ("copy");
        m.setReturnValue (new Type ("SELF_TYPE"));
        o.addMethod (m);
        
        o.setParent (null);
        micoolta.coolClasses.add (o);
        // --- Object class
        
        
        // --- IO class
        c = new CoolClass ("IO");
        
        // Method out_string in class IO
        m = new CoolMethod ("out_string");
        m.addFormalParameter (new CoolParameter ("x", new Type ("String")));
        m.setReturnValue (new Type ("SELF_TYPE"));
        c.addMethod (m);
        
        // Method out_int in class IO
        m = new CoolMethod ("out_int");
        m.addFormalParameter (new CoolParameter ("x", new Type ("Int")));
        m.setReturnValue (new Type ("SELF_TYPE"));
        c.addMethod (m);

        // Method in_string in class IO
        m = new CoolMethod ("in_string");
        m.setReturnValue (new Type ("String"));
        c.addMethod (m);
        
        // Method in_int in class IO
        m = new CoolMethod ("in_int");
        m.setReturnValue (new Type ("Int"));
        c.addMethod (m);
        
        c.setParent (o);
        micoolta.coolClasses.add (c);
        // --- IO class
        
        
        // Int class, no methods
        c = new CoolClass ("Int");
        c.setParent (o);
        micoolta.coolClasses.add (c);
        
        
        // Bool class, no methods
        c = new CoolClass ("Bool");
        c.setParent (o);
        micoolta.coolClasses.add (c);
        
        
        // --- String class
        c = new CoolClass ("String");
        
        // Method length in class String
        m = new CoolMethod ("length");
        m.setReturnValue (new Type ("Int"));
        c.addMethod (m);
        
        // Method concat in class String
        m = new CoolMethod ("concat");
        m.addFormalParameter (new CoolParameter ("s", new Type ("String")));
        m.setReturnValue (new Type ("String"));
        c.addMethod (m);
        
        // Method substr in class String
        m = new CoolMethod ("substr");
        m.addFormalParameter (new CoolParameter ("i", new Type ("Int")));
        m.addFormalParameter (new CoolParameter ("l", new Type ("Int")));
        m.setReturnValue (new Type ("String"));
        c.addMethod (m);
        
        c.setParent (o);
        micoolta.coolClasses.add (c);
        // --- String class
    }
    
    
    @SuppressWarnings("resource")
    public static void main (String[] args) throws IOException
    {
        String[][] options = new String[][] { 
                { "h", "Print this help.", "" }, 
                { "o", "Write Java code in <Output file> (specify Java extension or option \"-ie\" to ignore it).", "Output file" },
                { "main", "Specify Cool file which contains \"Main\" class.", "Main file" },
                //{ "dir", "Specify output path of Java classes.", "Output dir" },
                { "package", "Specify package of the Java class.", "Package" },
                { "ie", "Ignore extensions of output file (Java file) and sources file (COOL file).", "" },
        };
        
        // First: child class
        // Second: parent class
        micoolta.mapInstanceParentClass = new HashMap<String, String> ();
        
        micoolta.setupOptions (options);
        
        String[] onlyOptions = new String[options.length];
        
        for (int k = 0; k < options.length; k++)
            onlyOptions[k] = options[k][0];
        
        // Option "-h" selected || no correct options || no option
        if (CommandLineUtils.hasOption (args, "h") || CommandLineUtils.noOptions (args, onlyOptions) || args.length == 0)
            micoolta.printHelp ();
        
        // Option "-o" selected
        else if (CommandLineUtils.hasOption (args, "o")) {
            //ArrayList<String> valuesOptionDir = CommandLineUtils.getOptionValues (args, "dir", onlyOptions);
            //int sizeValuesOptionDir           = valuesOptionDir.size ();
            
            ArrayList<String> valuesOptionO = CommandLineUtils.getOptionValues (args, "o", onlyOptions);
            int sizeValuesOptionO           = valuesOptionO.size ();
            
            if (micoolta.DEBUG) {
                IOUtils.printInfo ("valuesOptionO.toString ()=" + valuesOptionO.toString ());
                IOUtils.printInfo ("sizeValuesOptionO=" + sizeValuesOptionO);
            }
            
            String mainFile   = null,
                   outputFile = null;//,
                   //outputDir  = null;
            
            /*
            if (!CommandLineUtils.hasOption (args, "dir") || sizeValuesOptionDir == 0)
                IOUtils.printError ("You must specify an output dir", true);
            else if (sizeValuesOptionDir == 1) {
                outputDir = valuesOptionDir.get (0);
                
                if (!FileUtils.pathExists (outputDir))
                    IOUtils.printError ("You must specify a valid path for output dir. \"" + outputDir + "\" does not exist", true);
            }
            else
                IOUtils.printError ("Too many arguments for \"-dir\" option", true);
            */
            
            // No arguments in "-o" option
            if (sizeValuesOptionO == 0)
                IOUtils.printError ("You must specify an output file when you select \"-o\" option", true);
            // Only one arguments in "-o" option
            else if (sizeValuesOptionO == 1) {
                // In this case there are two options:
                //      - user wrote only the java output file;
                //      - user wrote only "cl" file without the java output file.
                String extension = FileUtils.getExtension (valuesOptionO.get (0));
                
                if (extension.equals ("java"))
                    IOUtils.printError ("No Cool file specified", true);
                else if (extension.equals ("cl"))
                    IOUtils.printError ("No Java output file specified", true);
                else
                    IOUtils.printError ("You must specify an output file and a list of Cool file", true);
            }
            // sizeValuesOption >= 2 and user don't ignore the extensions of the file.
            // Java => .java
            // Cool => .cl
            else if (!CommandLineUtils.hasOption (args, "ie")) {
                // Must be .java
                String extension = FileUtils.getExtension (valuesOptionO.get (0));
                
                if (!extension.equals ("java"))
                    IOUtils.printError ("You must specify a Java file instead of \"" + 
                                        (extension.length () == 0 ? "<no extension>" : extension) + "\" file", true);
                
                boolean exit = false;
                
                for (int k = 1; k < sizeValuesOptionO; k++) {
                    extension = FileUtils.getExtension (valuesOptionO.get (k));
                    
                    if (!extension.equals ("cl")) {
                        IOUtils.printError ("You must specify a Cool file instead of \"" + 
                                            (extension.length () == 0 ? "<no extension>" : extension) + "\"");
                        
                        exit = true;
                    }
                }
                
                if (exit)
                    System.exit (-1);
            }
            
            // If we arrive here means that sizeValuesOption >= 2, so user specify correctly output file and Cool file
            outputFile = valuesOptionO.get (0);
            
            // Option "-main" selected
            if (CommandLineUtils.hasOption (args, "main")) {
                ArrayList<String> mainFileApp = CommandLineUtils.getOptionValues (args, "main", onlyOptions);
                
                if (mainFileApp.size () == 0)
                    IOUtils.printError ("You must specify the main file if you use \"main\" option", true);
                else if (mainFileApp.size () > 1)
                    IOUtils.printError ("You can only specify one main file if you use \"main\" option", true);
                
                // size = 1
                mainFile = mainFileApp.get (0);
                
                if (!valuesOptionO.contains (mainFile))
                    IOUtils.printError ("Main file [" + mainFile + "] does not appear in the list of COOL file", true);
            }
            else
                // User did not specify main file so, we assume that is the last of the list
                mainFile = valuesOptionO.get (sizeValuesOptionO - 1);
            
            if (micoolta.DEBUG) {
                IOUtils.printInfo ("Main File: " + mainFile);
                IOUtils.printInfo ("Output File: " + outputFile);
                
                IOUtils.printInfo ("First check.");
            }
                
            
            ArrayList<ParseTree> parseTrees = new ArrayList<ParseTree> ();  // Store the parse tree of the k-th file
            
            // Setup standard classes of Cool
            micoolta.setupStandardClasses ();
            
            // Check if there are COOL file with the same name
            ArrayList<Integer> indexes = new ArrayList<Integer> ();
            
            // First file is Java file
            for (int k = 1; k < sizeValuesOptionO; k++)
                for (int j = k + 1; j < sizeValuesOptionO; j++)
                    if (valuesOptionO.get (k).equals (valuesOptionO.get (j)))
                        indexes.add (k);
            
            // First argument is the output file
            for (int k = 1; k < sizeValuesOptionO; k++) {
                // More than one COOL file with the same name
                if (indexes.contains (k))
                    continue;
                
                String file = valuesOptionO.get (k);
                
                System.out.println (IOUtils.getColoredString ("[Compile] ", IOUtils.Color.CYAN) + file);
                
                // File does not exist
                if (!FileUtils.fileExists (file)) {
                    ErrorMessage.printError (ErrorMessage.Error.FILE_NOT_EXISTS, file);
                    
                    System.exit (-1);
                }
                
                // Get stream k-th file
                ANTLRFileStream input = new ANTLRFileStream (file);
                
                // Construct the lexer
                coolLexer lexer = new coolLexer (input);
                
                // Construct the tokens based on the lexer
                CommonTokenStream tokens = new CommonTokenStream (lexer);
                
                // Initialize the parser with tokens
                coolParser parser = new coolParser (tokens);
                
                // Remove "standard" error listeners
                parser.removeErrorListeners ();
                // Add my error listener
                parser.addErrorListener (new CoolErrorListener ());
                
                
                // 1 ---- WalkerListenerSemanticAnalysis ---- Start
                try {
                    // Begin parsing at "program" rule
                    ParseTree tree = parser.program ();
                    
                    parseTrees.add (tree);
                    
                    // Create the walker for calling the listener
                    ParseTreeWalker walker = new ParseTreeWalker ();
                    
                    // Check semantic errors such as:
                    //      - inheritance relationship;
                    //      - class defined only once;
                    //      - attributes/methods defined only once
                    WalkerListenerSemanticAnalysis wl = new WalkerListenerSemanticAnalysis (
                            micoolta.coolClasses,
                            micoolta.mapInstanceParentClass,
                            file.equals (mainFile),
                            mainFile
                    );
                    
                    walker.walk (wl, tree);
                    
                    // Check cyclic inheritance. Example: class A inherits B { ... } class B inherits A { ... } 
                    GenericUtils.checkCyclicInheritance (micoolta.coolClasses);
                    
                    // Add instance of parent class in the child class. This happen when the user write:
                    //      class A inherits B { ... }
                    // then declare:
                    //      class B { ... }
                    GenericUtils.resolveMapInstanceParentClass (micoolta.coolClasses, micoolta.mapInstanceParentClass);
                } 
                catch (RuntimeException e) {
                    IOUtils.printError ("RuntimeException. Message: " + e.getMessage (), true);
                }
                // 1 ---- WalkerListenerSemanticAnalysis ---- End
            }
            
            // At this point we must order the tree in case "mapInstanceParentClass" size is > 0.
            // The reason is that, the user can declare class A inherits B { ... } and then B. In this case, the tree
            // of A is handle before the tree of B ==> we'll have scope errors
            
            if (micoolta.DEBUG) {
                IOUtils.printInfo ("-- List classes (start):");
                
                for (int k = 0, size = micoolta.coolClasses.size (); k < size; k++)
                    System.out.println (micoolta.coolClasses.get (k).toString ());
                
                IOUtils.printInfo ("-- List classes (end).");
            }
            // End first check
            
            // Assert that parseTrees.size () == (fileSize - 1). fileSize - 1, because first file of the list is the output file
            TestUtils._assert ("parseTrees.size () != (valuesOptionO.size () - indexes.size () - 1). [" + parseTrees.size () + 
                               " != " + (valuesOptionO.size () - 1) + "]", 
                               parseTrees.size () == (valuesOptionO.size () - indexes.size () - 1));
            
            
            // Second check: scope
            if (micoolta.DEBUG)
                IOUtils.printInfo ("Scope.");
            
            ParseTreeProperty<Scope> scopes          = new ParseTreeProperty<Scope> ();
            ArrayList<ClassScope> examinedClassScope = new ArrayList<ClassScope> ();
            
            if (!ErrorMessage.ERROR_PRINTED) {  // No errors in the first check
                ErrorMessage.ERROR_PRINTED = false;

                
                // 2 ---- ScopeDefinitionPhase ---- Start
                // Definition scope phase
                for (int k = 0, size = parseTrees.size (); k < size; k++) {
                    ParseTree tree = parseTrees.get (k);
                        
                    // Create the walker for calling the listener
                    ParseTreeWalker walker = new ParseTreeWalker ();

                    // Scope definition walker
                    walker.walk (new ScopeDefinitionPhase (scopes, examinedClassScope, micoolta.coolClasses), tree);
                }
                // 2 ---- ScopeDefinitionPhase ---- End
                
                if (!ErrorMessage.ERROR_PRINTED) {  // No errors in the definition of the scope
                    ErrorMessage.ERROR_PRINTED = false;
                    
                    // 3 ---- ScopeResolvingPhase ---- Start
                    // Resolving scope phase
                    for (int k = 0, size = parseTrees.size (); k < size; k++) {
                        ParseTree tree = parseTrees.get (k);
                            
                        // Create the walker for calling the listener
                        ParseTreeWalker walker = new ParseTreeWalker ();
    
                        // Scope definition walker
                        walker.walk (new ScopeResolvingPhase (scopes, micoolta.coolClasses), tree);
                    }
                    // 3 ---- ScopeResolvingPhase ---- End
                }
            }
            // End second check 
            
            
            // Type checking
            if (micoolta.DEBUG)
                IOUtils.printInfo ("Type checking.");
            
            if (!ErrorMessage.ERROR_PRINTED) {  // No errors in the third check (scope)
                ErrorMessage.ERROR_PRINTED = false;
                
                // 4 ---- TypeCheckingListener ---- Start
                // Type Checking
                for (int k = 0, size = parseTrees.size (); k < size; k++) {
                    ParseTree tree = parseTrees.get (k);
                        
                    // Create the walker for calling the listener
                    ParseTreeWalker walker = new ParseTreeWalker ();

                    // Type checker definition walker
                    walker.walk (new TypeCheckingListener (micoolta.coolClasses, scopes), tree);
                }
                // 4 ---- TypeCheckingListener ---- End
            }
            // End type checking
            
            // Translation
            if (micoolta.DEBUG)
                IOUtils.printInfo ("Translation.");
            
            if (!ErrorMessage.ERROR_PRINTED) {  // No errors in the fourth check (type checking)
                ErrorMessage.ERROR_PRINTED = false;
                
                // Handle output file - Start
                String classNameOutputFile = FileUtils.getFileNameByPath (outputFile),
                       pathOutputFile      = "";
                
                int lastIndexOutputFile = classNameOutputFile.lastIndexOf ("."),
                    lastIndexPath       = outputFile.lastIndexOf (File.separator);
                
                classNameOutputFile = lastIndexOutputFile == -1 ? classNameOutputFile : classNameOutputFile.substring (0, lastIndexOutputFile);
                pathOutputFile      = lastIndexPath == -1 ? "" : outputFile.substring (0, lastIndexPath);
                
                if (classNameOutputFile.length () > 0 && StringUtils.isLowerCaseChar (classNameOutputFile.charAt (0)) && !CommandLineUtils.hasOption (args, "ie"))
                    IOUtils.printWarning ("By convention, Java type names usually start with an uppercase letter.");
                if (classNameOutputFile.length () > 0 && GenericUtils.getInstanceClassByClassName (coolClasses, classNameOutputFile) != null)
                    IOUtils.printError ("Class name " + classNameOutputFile + " already exists as COOL class. Rename output file.", true);
                if (!FileUtils.pathExists (pathOutputFile) && pathOutputFile.replace (" ", "").length () > 0)
                    IOUtils.printError ("Path on output file does not exist. Path: " + pathOutputFile, true);
                if (FileUtils.fileExists (outputFile)) {
                    System.out.print ("\n" + IOUtils.getColoredString ("[Warning]", Color.YELLOW) + " Output file \"" + outputFile + "\" already exists.");
                    
                    String response = null;
                    
                    do {
                        System.out.print ((response == null ? "" : IOUtils.getColoredString ("[Warning]", Color.YELLOW)) + " Do you want rewrite it [y/n]?  ");
                        
                        response = new Scanner (System.in).nextLine ().toLowerCase ();
                    } while (!response.equals ("y") && !response.equals ("n"));
                    
                    if (response.equals ("y")) 
                        FileUtils.deleteContentFile (outputFile);
                    else
                        System.exit (0);
                    
                    // The creation of the file will be handled in "SyntaxTranslationPhase" listener
                }
                // Handle output file - End
                
                // Write standard classes inside the file
                PrintWriter writer = new PrintWriter (new FileOutputStream (new File (outputFile), true)); // true means 'append'
                
                try {
                    ArrayList<String> packageOption = CommandLineUtils.getOptionValues (args, "package", onlyOptions);
                    
                    FileUtils.writeStandardClassesIntoOutputFile (writer, packageOption.size () > 0 ? packageOption.get (0) : null);
                    
                    // 5 ---- SyntaxTranslationPhase ---- Start
                    // Type Checking
                    for (int k = 0, size = parseTrees.size (); k < size; k++) {
                        ParseTree tree = parseTrees.get (k);
                            
                        // Create the walker for calling the listener
                        ParseTreeWalker walker = new ParseTreeWalker ();

                        // Translation of the parse tree
                        walker.walk (new SyntaxTranslationPhase (outputFile, micoolta.coolClasses, scopes), tree);
                    }
                    // 5 ---- SyntaxTranslationPhase ---- End
                    
                    writer = new PrintWriter (new FileOutputStream (new File (outputFile), true));
                    
                    FileUtils.writeMainClassIntoOutputFile (writer, classNameOutputFile);
                    
                    writer.close ();
                    
                    System.out.println ("\n" + IOUtils.getColoredString ("[Success]", Color.GREEN) + " Output file: " + outputFile);
                }
                catch (IOException e) {
                    IOUtils.printError (e.getMessage (), true);
                }
                finally {
                    writer.close ();
                }
            }
            // End Translation
        }
        
        System.exit (0);
    }
}
