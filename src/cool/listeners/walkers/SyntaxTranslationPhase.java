package cool.listeners.walkers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import cool.data.structure.object.CoolClass;
import cool.data.structure.translation.PrivateVariable;
import cool.data.structure.types.Type;

import cool.scope.Scope;

import cool.translation.java.TemplateJavaClass;
import cool.translation.java.Translation;

import utils.GenericUtils;
import utils.IOUtils;
import utils.TestUtils;
import utils.TranslationUtils;

import antlr.coolListener;

import antlr.coolParser.AttributeDeclarationContext;
import antlr.coolParser.ClassDeclarationContext;
import antlr.coolParser.ExpressionContext;
import antlr.coolParser.FeatureContext;
import antlr.coolParser.FormalContext;
import antlr.coolParser.MethodDeclarationContext;
import antlr.coolParser.ProgramContext;

public class SyntaxTranslationPhase implements coolListener
{
    private PrintWriter writer;
    
    private TemplateJavaClass templateJClass;
    
    private ArrayList<TemplateJavaClass> templates;
    
    private Stack<ArrayList<PrivateVariable>> stackAttributes;
    
    // Scope variables ~ Start
    private AttributeDeclarationContext attributeContext;
    
    private MethodDeclarationContext methodContext;
    
    private CoolClass class_;
    
    private ArrayList<CoolClass> coolClasses;
    
    private ParseTreeProperty<Scope> scope;
    
    private Scope currentScope;
    // Scope variables ~ End
    
    private static final String errorMessage = "[Syntax translation Listener] Error! ";
    
    
    public SyntaxTranslationPhase (String outputFile, ArrayList<CoolClass> coolClasses, ParseTreeProperty<Scope> scope) throws FileNotFoundException, UnsupportedEncodingException
    {
        this.coolClasses     = coolClasses;
        this.scope           = scope;
        this.writer          = new PrintWriter (new FileOutputStream (new File (outputFile), true)); // true for 'append'
        this.stackAttributes = new Stack<ArrayList<PrivateVariable>> ();
        this.templates       = new ArrayList<TemplateJavaClass> ();
    }
    
    
    @Override
    public void enterClassDeclaration (ClassDeclarationContext ctx)
    {
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String className       = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        // If class has not parent class, Object is its parent
        String classParentName = (identifiers.size () > 1) ? identifiers.get (1).toString () : "Object";
        
        TestUtils._assert_not_null (errorMessage + "Class name is null.", className);
        
        // We don't add the '{' on the template
        this.templateJClass = new TemplateJavaClass (className, TranslationUtils.getJavaSyntaxOfClass (className, classParentName));
        
        // Update scope
        this.currentScope = this.scope.get (ctx);
        
        // Update class
        this.class_ = GenericUtils.getInstanceClassByClassName (this.coolClasses, className);
        TestUtils._assert_not_null (errorMessage + "Class_ is null.", this.class_);
    }

    
    @Override
    public void exitClassDeclaration (ClassDeclarationContext ctx)
    {
        // Update scope
        this.currentScope = this.currentScope.getEnclosingScope ();
        
        // Update class
        this.class_ = null;
        
        // Add current template in the array
        this.templates.add (this.templateJClass);
    }
    
    
    @Override
    public void enterAttributeDeclaration (AttributeDeclarationContext ctx)
    {
        // Update attribute context
        this.attributeContext = ctx;
        
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String attributeName = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        String attributeType = (identifiers.size () > 1) ? identifiers.get (1).toString () : null;
        
        TestUtils._assert_not_null (errorMessage + "Attribute name is null.", attributeName, true);
        TestUtils._assert_not_null (errorMessage + "Attribute type is null.", attributeType, true);
        
        ArrayList<PrivateVariable> entryStack = new ArrayList<PrivateVariable> ();
        
        entryStack.add (new PrivateVariable (attributeName, attributeType));
        this.stackAttributes.push (entryStack);
        
        String javaTranslation = TranslationUtils.getJavaSyntaxOfAttribute (attributeName, TranslationUtils.resolveType (attributeType));
        
        this.templateJClass.addAttributeDeclaration (javaTranslation);
        
        if (this.templateJClass.getDefaultConstructor () == null) {
            String arguments = TranslationUtils.getJavaSyntaxOfArguments (null);
            
            this.templateJClass.setDefaultConstructor (this.templateJClass.getClassName (), new Type (), arguments, TranslationUtils.Visibility.PUBLIC);
        }
        
        if (ctx.expression () != null) {
            Translation translation = new Translation (this.templateJClass, this.stackAttributes, this.templates);
            
            // Set scope variables
            translation.setAttributeContext (this.attributeContext);
            translation.setMethodContext (this.methodContext);
            translation.setCoolClasses (this.coolClasses);
            translation.setCurrentClass (this.class_);
            translation.setEntireScope (this.scope);
            translation.setScope (this.scope);
            translation.setCurrentScope (this.currentScope);
            
            translation.setIsConstructor (true);
            translation.handleExpressionConstructor (ctx.expression (), attributeName, attributeType);
        }
        else
            this.templateJClass.addContentToConstructor ("this." + attributeName + " = " + TranslationUtils.addDefaultConstructor (TranslationUtils.resolveType (attributeType)) + ";\n");
    }

    
    @Override
    public void exitAttributeDeclaration (AttributeDeclarationContext ctx) 
    { 
        // Update attribute context
        this.attributeContext = null;
    }

    
    @Override
    public void enterMethodDeclaration (MethodDeclarationContext ctx)
    {
        // Update method context
        this.methodContext = ctx;
        
        // Update scope
        this.currentScope = this.scope.get (ctx);
        
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String methodName        = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        // If class has not parent class, Object is its parent
        String methodReturnValue = (identifiers.size () > 1) ? identifiers.get (1).toString () : null;
        
        TestUtils._assert_not_null (errorMessage + "Method name is null.", methodName, true);
        TestUtils._assert_not_null (errorMessage + "Method return value is null.", methodReturnValue, true);
        
        if ((methodReturnValue.equals ("SELF_TYPE")))
            methodReturnValue = this.templateJClass.getClassName ();

        // Add the method to the template
        this.templateJClass.addMethod (
                methodName, 
                new Type (TranslationUtils.resolveType (methodReturnValue)), 
                TranslationUtils.getJavaSyntaxOfArguments (ctx.formal ())
        );
        
        // Create new stack for parameters
        Stack<ArrayList<PrivateVariable>> stackParameters = new Stack<ArrayList<PrivateVariable>> ();
        
        ArrayList<PrivateVariable> entryStack = new ArrayList<PrivateVariable> ();
        
        List<FormalContext> formalParameters = ctx.formal ();
        for (int k = 0, size = formalParameters.size (); k < size; k++) {
            List<TerminalNode> params = formalParameters.get (k).Identifier ();
            
            String paramName = (params.size () > 0) ? params.get (0).toString () : null;
            String paramType = (params.size () > 1) ? params.get (1).toString () : null;
            
            TestUtils._assert_not_null (errorMessage + "Parameter name is null.", paramName, true);
            TestUtils._assert_not_null (errorMessage + "Parameter type is null.", paramType, true);
            
            String paramNewName = this.templateJClass.getClassName () + "_" + methodName + "_" + paramName + "_0";
            
            this.templateJClass.addGlobalVariableDeclaration (
                    TranslationUtils.getJavaSyntaxOfAttribute (
                            paramNewName, 
                            TranslationUtils.resolveType (paramType),
                            TranslationUtils.Visibility.PROTECTED
                    )
            );
            
            this.templateJClass.addContentToMethod ("this." + paramNewName + " = " + paramName + ";\n", true);
            
            entryStack.add (new PrivateVariable (paramName, paramNewName));
        }
        
        stackParameters.push (entryStack);
        
        // Apply the translation
        Translation translation = new Translation (this.templateJClass, stackParameters, templates);
        
        // Set scope variables
        translation.setAttributeContext (this.attributeContext);
        translation.setMethodContext (this.methodContext);
        translation.setCoolClasses (this.coolClasses);
        translation.setCurrentClass (this.class_);
        translation.setEntireScope (this.scope);
        translation.setScope (this.scope);
        translation.setCurrentScope (this.currentScope);
        
        translation.applyJavaTranslation (ctx.expression ());
    }

    
    @Override
    public void exitMethodDeclaration (MethodDeclarationContext ctx) 
    {
        // Update scope
        this.currentScope = this.currentScope.getEnclosingScope ();
        
        // Update method context
        this.methodContext = null;
    }

    
    @Override
    public void enterProgram (ProgramContext ctx) { }

    
    @Override
    public void exitProgram (ProgramContext ctx) 
    {
        // Write COOL classes
        for (int k = 0, size_t = this.templates.size (); k < size_t; k++)
            this.writer.write (this.templates.get (k).toString ());
        
        this.writer.close ();
    }
    
    
    @Override
    public void enterEveryRule (ParserRuleContext arg0) { }

    
    @Override
    public void exitEveryRule (ParserRuleContext arg0) { }

    
    @Override
    public void visitErrorNode (ErrorNode arg0) { }

    
    @Override
    public void visitTerminal (TerminalNode arg0) { }

    
    @Override
    public void enterFeature (FeatureContext ctx) { }

    
    @Override
    public void exitFeature (FeatureContext ctx) { }
    
    
    @Override
    public void enterExpression (ExpressionContext ctx) { }
    
    
    @Override
    public void exitExpression (ExpressionContext ctx) { }
    
    
    @Override
    public void enterFormal (FormalContext ctx) { }

    
    @Override
    public void exitFormal (FormalContext ctx) { }
}
