package cool.listeners.walkers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import utils.GenericUtils;
import utils.IOUtils;
import utils.StringUtils;
import utils.TestUtils;

import cool.data.structure.object.CoolAttribute;
import cool.data.structure.object.CoolClass;
import cool.data.structure.object.CoolMethod;
import cool.data.structure.object.CoolParameter;
import cool.data.structure.types.ExpressionTypes;
import cool.data.structure.types.Type;

import cool.error.ErrorMessage;

import antlr.coolListener;

import antlr.coolParser.AttributeDeclarationContext;
import antlr.coolParser.ClassDeclarationContext;
import antlr.coolParser.ExpressionContext;
import antlr.coolParser.FeatureContext;
import antlr.coolParser.FormalContext;
import antlr.coolParser.MethodDeclarationContext;
import antlr.coolParser.ProgramContext;

public class WalkerListenerSemanticAnalysis implements coolListener
{
    private ArrayList<CoolClass> classes;
    
    private Map<String, String> mapInstanceParentClass;
    
    private ArrayList<String> localClassesNames;
    
    private boolean isMainClass;
    
    private CoolClass coolClass;
    
    private String mainFile;
    
    private static final String errorMessage = "[Walker Listener] Error! ";
    
    
    public WalkerListenerSemanticAnalysis (ArrayList<CoolClass> classes, Map<String, String> mapInstanceParentClass, boolean isMainClass, String mainFile)
    {
        this.classes                = classes;
        this.mapInstanceParentClass = mapInstanceParentClass;
        this.isMainClass            = isMainClass;
        this.mainFile               = mainFile;
        this.localClassesNames      = new ArrayList<String> ();
    }

    
    @Override
    public void enterProgram (ProgramContext ctx) { }

    
    @Override
    public void exitProgram (ProgramContext ctx)
    {
        CoolClass mainClass = GenericUtils.getInstanceClassByClassName (this.classes, "Main");
        
        if (this.isMainClass) {
            if (mainClass == null)
                ErrorMessage.printError (ErrorMessage.Error.ERROR, "\"Main\" class not found");

            if (!mainClass.hasMethod ("main"))
                ErrorMessage.printError (ErrorMessage.Error.ERROR, "\"Main\" class has no method \"main\"");
        }
        else if (!this.isMainClass && this.localClassesNames.contains ("Main"))
            ErrorMessage.printError (
                    ErrorMessage.Error.ERROR, 
                    "\"Main\" class must be defined in file that you have specified (\"" + this.mainFile + "\")"
            );
    }

    
    @Override
    public void enterClassDeclaration (ClassDeclarationContext ctx)
    {
        @SuppressWarnings("serial")
        ArrayList<String> classNoRedefine = new ArrayList<String> () {{
            add ("Object");
            add ("IO");
            add ("Int");
            add ("String");
            add ("Bool");
        }};
        
        @SuppressWarnings("serial")
        ArrayList<String> classNoInherit = new ArrayList<String> () {{
            add ("Int");
            add ("String");
            add ("Bool");
        }};
        
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String className       = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        // If class has not parent class, Object is its parent
        String classParentName = (identifiers.size () > 1) ? identifiers.get (1).toString () : "Object";
        
        TestUtils._assert_not_null (errorMessage + "Class is null.", className);
        
        // Check if class already exists
        // In this phase, we do not add the instance of the parent. Example:
        // class B inherits A { ... } then we declared class A { ... }, with the listener, we enter first in B and A does not exist in that moment
        // Anyway, if class is already declared I add the instance of the parent class else we will add the instance in the definition phase
        if (GenericUtils.getInstanceClassByClassName (this.classes, className) == null) {
            // Class does not exist, I add the name to the instance
            this.coolClass = new CoolClass (className);
            
            if (className.length () > 0 && StringUtils.isLowerCaseChar (className.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.CLASS_NAME_UPPER_CASE, ctx.getStart ().getLine (), className);
            
            CoolClass parentClass = GenericUtils.getInstanceClassByClassName (this.classes, classParentName);
            // Parent class has already been defined
            if (parentClass != null) {
                if (classParentName.length () > 0 && StringUtils.isLowerCaseChar (classParentName.charAt (0)))
                    ErrorMessage.printError (ErrorMessage.Error.CLASS_NAME_UPPER_CASE, ctx.getStart ().getLine (), classParentName);
                
                // Parent class exists, I add it into the instance of the class
                this.coolClass.setParent (parentClass);
            }
            // Parent class has not been defined yet. We add names and resolve them, when we iterate all classes
            else
                mapInstanceParentClass.put (className, classParentName);
        }
        // Class "className" is already defined
        else
            ErrorMessage.printError (ErrorMessage.Error.CLASS_REDEFINED, ctx.getStart ().getLine (), className);
        
        // We cannot redefine from some classes
        if (classNoRedefine.contains (className))
            ErrorMessage.printError (ErrorMessage.Error.CLASS_STD_ERROR_REDEFINE, ctx.getStart ().getLine (), className);
        
        // We cannot inherit from some classes
        if (classNoInherit.contains (classParentName))
            ErrorMessage.printError (ErrorMessage.Error.CLASS_STD_ERROR_INHERIT, ctx.getStart ().getLine (), classParentName);
       
        this.localClassesNames.add (className);
    }

    
    @Override
    public void exitClassDeclaration (ClassDeclarationContext ctx)
    {
        // Add the Class inside the array
        this.classes.add (this.coolClass);
        
        // We finish to handle this.coolClass, so we set it to null
        this.coolClass = null;
    }

    
    @Override
    public void enterAttributeDeclaration (AttributeDeclarationContext ctx)
    {
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String attributeName = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        String attributeType = (identifiers.size () > 1) ? identifiers.get (1).toString () : null;
        
        TestUtils._assert_not_null (errorMessage + "Attribute name is null.", attributeName, true);
        TestUtils._assert_not_null (errorMessage + "Attribute type is null.", attributeType, true);
        TestUtils._assert_not_null (errorMessage + "coolClass is null.", this.coolClass, true);
        
        if (attributeName.length () > 0 && StringUtils.isUpperCaseChar (attributeName.charAt (0)))
            ErrorMessage.printError (ErrorMessage.Error.FEATURE_NAME_LOWER, ctx.getStart ().getLine (), attributeName);
        
        if (attributeType.length () > 0 && StringUtils.isLowerCaseChar (attributeType.charAt (0)))
            ErrorMessage.printError (ErrorMessage.Error.TYPE_IDENTIFIER_CAPITAL_LETTER, ctx.getStart ().getLine (), attributeType);
            
        if (this.coolClass.hasAttribute (attributeName))
            ErrorMessage.printError (ErrorMessage.Error.ATTRIBUTE_REPEATED, ctx.getStart ().getLine (), attributeName);
        
        if (attributeName.equals ("self"))
            ErrorMessage.printError (ErrorMessage.Error.SELF_ATTRIBUTE_NAME, ctx.getStart ().getLine ());
        
        if (StringUtils.isJavaKeyword (attributeName))
            ErrorMessage.printError (ErrorMessage.Error.JAVA_KEYWORD, ctx.getStart ().getLine (), attributeName, "declaring an attribute");
        
        if (this.coolClass.hasAttributeInParentsClasses (attributeName))
            ErrorMessage.printError (ErrorMessage.Error.ATTRIBUTE_REDEFINED, ctx.getStart ().getLine (), attributeName);
        
        this.coolClass.addAttribute (new CoolAttribute (attributeName, new Type (attributeType)));
    }

    
    @Override
    public void exitAttributeDeclaration (AttributeDeclarationContext ctx) { }


    @Override
    public void enterMethodDeclaration (MethodDeclarationContext ctx)
    {
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String methodName        = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        // If class has not parent class, Object is its parent
        String methodReturnValue = (identifiers.size () > 1) ? identifiers.get (1).toString () : null;
        
        TestUtils._assert_not_null (errorMessage + "Method name is null.", methodName, true);
        TestUtils._assert_not_null (errorMessage + "Method return value is null.", methodReturnValue, true);
        TestUtils._assert_not_null (errorMessage + "coolClass is null.", this.coolClass, true);
        
        if (methodName.length () > 0 && StringUtils.isUpperCaseChar (methodName.charAt (0)))
            ErrorMessage.printError (ErrorMessage.Error.FEATURE_NAME_LOWER, ctx.getStart ().getLine (), methodName);
        
        if (methodReturnValue.length () > 0 && StringUtils.isLowerCaseChar (methodReturnValue.charAt (0)))
            ErrorMessage.printError (ErrorMessage.Error.TYPE_IDENTIFIER_CAPITAL_LETTER, ctx.getStart ().getLine (), methodReturnValue);
        
        if (this.coolClass.hasMethod (methodName))
            ErrorMessage.printError (ErrorMessage.Error.METHOD_REPEATED, ctx.getStart ().getLine (), methodName);
        
        if (this.coolClass.hasAttribute (methodName))
            ErrorMessage.printError (ErrorMessage.Error.ATTRIBUTE_METHOD_SAME_NAME, ctx.getStart ().getLine (), methodName);
        
        if (StringUtils.isJavaKeyword (methodName))
            ErrorMessage.printError (ErrorMessage.Error.JAVA_KEYWORD, ctx.getStart ().getLine (), methodName, "declaring a method");
        
        CoolMethod m = new CoolMethod (methodName, new Type (methodReturnValue));
        
        // Check formal parameters method
        List<FormalContext> fp = ctx.formal ();
        
        for (int k = 0, size = fp.size (); k < size; k++) {
            List<TerminalNode> params = fp.get (k).Identifier ();
            
            String paramName = (params.size () > 0) ? params.get (0).toString () : null;
            String paramType = (params.size () > 1) ? params.get (1).toString () : null;
            
            TestUtils._assert_not_null (errorMessage + "Parameter name is null.", paramName, true);
            TestUtils._assert_not_null (errorMessage + "Parameter type is null.", paramType, true);
            
            if (paramName.length () > 0 && StringUtils.isUpperCaseChar (paramName.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.FEATURE_NAME_LOWER, ctx.getStart ().getLine (), paramName);
            
            if (paramType.length () > 0 && StringUtils.isLowerCaseChar (paramType.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.TYPE_IDENTIFIER_CAPITAL_LETTER, ctx.getStart ().getLine (), paramType);
            
            if (m.hasParameter (paramName))
                ErrorMessage.printError (ErrorMessage.Error.IDENTIFIERS_IN_FORMAL_PARAMETER, ctx.getStart ().getLine (), paramName);
            
            if (paramName.equals ("self"))
                ErrorMessage.printError (ErrorMessage.Error.SELF_ASSING_BIND, ctx.getStart ().getLine (), "formal parameter");
            
            if (StringUtils.isJavaKeyword (paramName))
                ErrorMessage.printError (ErrorMessage.Error.JAVA_KEYWORD, ctx.getStart ().getLine (), paramName, "declaring a parameter");
            
            m.addFormalParameter (new CoolParameter (paramName, new Type (paramType)));
        }
        
        if (this.coolClass.hasMethodWithDifferentSignatureInParentsClasses (m))
            ErrorMessage.printError (ErrorMessage.Error.METHOD_REPEATED_CHILD_CLASS, ctx.getStart ().getLine (), methodName, this.coolClass.getName ());
            
        this.coolClass.addMethod (m);
    }
    
    
    /*
        Expression productions:
        
        1. expression ('@' Identifier)? '.' Identifier '(' (expression (',' expression)*)? ')' (';')?
        2. Identifier '(' (expression (',' expression)*)? ')' (';')?
        3. 'if' expression 'then' expression 'else' expression 'fi' (';')?
        4. 'while' expression 'loop' expression 'pool' (';')?
        5. '{' expression+ '}' (';')?
        6. 'let' Identifier ':' Identifier ('<-' expression)? (',' Identifier ':' Identifier ('<-' expression)?)* 'in' expression (';')? 
        7. 'case' expression 'of' (Identifier ':' Identifier '=>' expression ';')+ 'esac' (';')?
        8. 'new' Identifier (';')?
        9. '~' expression (';')?
        10. 'isvoid' expression (';')?
        11. expression ('*' | '/') expression (';')?
        12. expression ('+' | '-') expression (';')?
        13. expression ('<=' | '<' | '=') expression (';')?
        14. 'not' expression (';')?
        15. '(' expression ')' (';')?
        16. <assoc=right> Identifier '<-' expression (';')?
        17. Identifier (';')?
        18. Integer (';')?
        19. String (';')?
        20. True (';')?
        21. False (';')?;
    */
    @SuppressWarnings("incomplete-switch")
    @Override
    public void enterExpression (ExpressionContext ctx)
    {
        ExpressionTypes eType = GenericUtils.identifyTypeExpression (ctx);
        TestUtils._assert_false (errorMessage + "Expression is unknown (enterExpression).", eType == ExpressionTypes.UNKNOWN, true);
        
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        int size_i = (identifiers == null) ? 0 : identifiers.size ();
        
        switch (eType) {
        // 1. After '@' we must declare a Type, after '.' there is an Identifier (STATIC DISPATCH)
        case STATIC_DISPATCH:
        {
            ArrayList<String> nodes = GenericUtils.getListChildNodeFromExpressionContext (ctx);

            if (nodes.lastIndexOf ("@") == -1)
                IOUtils.printError ("No '@' found in static dispatch. Line: " + ctx.getStart ().getLine (), true);
            if (size_i != 2)
                IOUtils.printError ("Number of identifiers must be 2 instead of " + size_i + ". Line: " + ctx.getStart ().getLine (), true);
            
            String type = identifiers.get (0).toString (),
                   id   = identifiers.get (1).toString ();
            
            // isIdentifier method, check only if the string matchs this: "[A-Za-z][A-Za-z0-9_]*"
            if (!StringUtils.isIdentifier (type))
                ErrorMessage.printError (ErrorMessage.Error.ERROR, ctx.getStart ().getLine (), type + " is not a Type.");
            if (!StringUtils.isIdentifier (id))
                ErrorMessage.printError (ErrorMessage.Error.ERROR, ctx.getStart ().getLine (), id + " is not an Identifier.");
            
            if (type.length () > 0 && !StringUtils.isUpperCaseChar (type.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.TYPE_IDENTIFIER_CAPITAL_LETTER, ctx.getStart ().getLine (), type);
            if (id.length () > 0 && !StringUtils.isLowerCaseChar (id.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), id);

            break;
        }
        // 2. Before '(' we must declare an Identifier (DISPATCH)
        case DISPATCH:
        {
            if (size_i != 1)
                IOUtils.printError ("Number of identifiers must be 1 instead of " + size_i + ". Line: " + ctx.getStart ().getLine (), true);
            
            String id = identifiers.get (0).toString ();
            
            if (!StringUtils.isIdentifier (id))
                ErrorMessage.printError (ErrorMessage.Error.ERROR, ctx.getStart ().getLine (), id + " is not an Identifier.");
            
            if (id.length () > 0 && !StringUtils.isLowerCaseChar (id.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), id);
            
            break;
        }
        // Skip 3, 4, 5. There is no Identifier
        
        // 6. For each pair Identifier ':' Identifier, first is Identifier while second is Type (LET)
        // 7. For each pair Identifier ':' Identifier, first is Identifier while second is Type (CASE)
        case LET_INIT:
        case LET_NO_INIT:
        case CASE:
        {
            if (size_i % 2 != 0)
                IOUtils.printError ("Walker semantic, expression " + eType.name () + " not all identifiers have a type", true);
            
            int k = 0;
            
            while (k < size_i) {
                String id   = identifiers.get (k++).toString (),
                       type = identifiers.get (k++).toString ();
                
                if (!StringUtils.isIdentifier (id))
                    ErrorMessage.printError (ErrorMessage.Error.ERROR, ctx.getStart ().getLine (), id + " is not an Identifier.");
                if (!StringUtils.isIdentifier (type))
                    ErrorMessage.printError (ErrorMessage.Error.ERROR, ctx.getStart ().getLine (), type + " is not a Type.");
                
                if (id.length () > 0 && !StringUtils.isLowerCaseChar (id.charAt (0)))
                    ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), id);
                if (type.length () > 0 && !StringUtils.isUpperCaseChar (type.charAt (0)))
                    ErrorMessage.printError (ErrorMessage.Error.TYPE_IDENTIFIER_CAPITAL_LETTER, ctx.getStart ().getLine (), type);
            }
            
            break;
        }
        // 8. After 'new' we must declare a Type (NEW)
        case NEW:
        {
            if (size_i != 1)
                IOUtils.printError ("Number of identifiers must be 1 instead of " + size_i + ". Line: " + ctx.getStart ().getLine (), true);
            
            String type = identifiers.get (0).toString ();
            
            if (!StringUtils.isIdentifier (type))
                ErrorMessage.printError (ErrorMessage.Error.ERROR, ctx.getStart ().getLine (), type + " is not a Type.");
            
            if (type.length () > 0 && !StringUtils.isUpperCaseChar (type.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), type);
            
            break;
        }
        // 16. Before '<-' we must declare an Identifier (ASSIGN)
        case ASSIGN:
        // 17. Identifier (IDENTIFIER)
        case IDENTIFIER:
        {
            if (size_i != 1)
                IOUtils.printError ("Number of identifiers must be 1 instead of " + size_i + ". Line: " + ctx.getStart ().getLine (), true);
            
            String id = identifiers.get (0).toString ();
            
            if (!StringUtils.isIdentifier (id))
                ErrorMessage.printError (ErrorMessage.Error.ERROR, ctx.getStart ().getLine (), id + " is not an Identifier.");
            
            if (id.length () > 0 && !StringUtils.isLowerCaseChar (id.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), id);
            
            // The identifier self may be referenced, but it is an error to assign to self
            if (eType == ExpressionTypes.ASSIGN)
                if (id.equals ("self"))
                    ErrorMessage.printError (ErrorMessage.Error.SELF_ASSING_BIND, ctx.getStart ().getLine (), "Assign");
            
            break;
        }
        
        // Skip 9, 10, 11, 12, 13, 14, 15. There is no Identifier
            
        // Skip 18, 19, 20, 21. There is no Identifier
        }
        
        for (int k = 0; k < size_i; k++) {
            String id = identifiers.get (k).toString ();
            
            if (StringUtils.isJavaKeyword (id))
                ErrorMessage.printError (ErrorMessage.Error.JAVA_KEYWORD, ctx.getStart ().getLine (), id, "declaring an identifier");
        }
    }

    
    @Override
    public void exitExpression (ExpressionContext ctx) 
    {
        ExpressionTypes eType = GenericUtils.identifyTypeExpression (ctx);
        TestUtils._assert_false (errorMessage + "Expression is unknown (enterExpression).", eType == ExpressionTypes.UNKNOWN, true);
        
        if (eType == ExpressionTypes.STRING) {
            TestUtils._assert_not_null (errorMessage + "Expression is unknown (enterExpression).", ctx != null, true);
            TestUtils._assert_not_null (errorMessage + "Expression is unknown (enterExpression).", ctx.String ().toString () != null, true);
            
            if (ctx.String ().toString ().length () > 1024)
                ErrorMessage.printError (ErrorMessage.Error.STRING_CONSTANT_LIMIT, ctx.getStart ().getLine ());
        }
    }

    
    @Override
    public void exitMethodDeclaration (MethodDeclarationContext ctx) { }
    
    
    @Override
    public void enterEveryRule (ParserRuleContext arg0) { }
    
    
    @Override
    public void enterFeature (FeatureContext ctx) { }

    
    @Override
    public void exitFeature (FeatureContext ctx) { }

    
    @Override
    public void exitEveryRule (ParserRuleContext arg0) { }

    
    @Override
    public void visitErrorNode (ErrorNode arg0) { }

    
    @Override
    public void visitTerminal (TerminalNode arg0) { }

    
    @Override
    public void enterFormal (FormalContext ctx) { }

    
    @Override
    public void exitFormal (FormalContext ctx) { }
}
