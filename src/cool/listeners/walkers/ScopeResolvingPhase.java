package cool.listeners.walkers;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import utils.GenericUtils;
import utils.IOUtils;
import utils.StringUtils;
import utils.TestUtils;

import cool.data.structure.object.CoolClass;
import cool.data.structure.symbols.Symbol;
import cool.data.structure.types.ExpressionTypes;

import cool.error.ErrorMessage;

import cool.scope.Scope;

import antlr.coolListener;

import antlr.coolParser.AttributeDeclarationContext;
import antlr.coolParser.ClassDeclarationContext;
import antlr.coolParser.ExpressionContext;
import antlr.coolParser.FeatureContext;
import antlr.coolParser.FormalContext;
import antlr.coolParser.MethodDeclarationContext;
import antlr.coolParser.ProgramContext;

public class ScopeResolvingPhase implements coolListener
{
    private ParseTreeProperty<Scope> scopes;
    
    private Scope currentScope;
    
    private ArrayList<CoolClass> coolClasses;
    
    private CoolClass currentClass;
    
    private static final String errorMessage = "[Scope resolving phase] Error! ";
    
    
    public ScopeResolvingPhase (ParseTreeProperty<Scope> scopes, ArrayList<CoolClass> coolClasses)
    {
        this.scopes             = scopes;
        this.coolClasses        = coolClasses;
        this.currentScope       = null;
        this.currentClass       = null;
    }
    
    
    /*
        Expression productions:
        
        1. expression ('@' Identifier)? '.' Identifier '(' (expression (',' expression)*)? ')' (';')?
        2. Identifier '(' (expression (',' expression)*)? ')' (';')?
                (NO ID) 3. 'if' expression 'then' expression 'else' expression 'fi' (';')?
                (NO ID) 4. 'while' expression 'loop' expression 'pool' (';')?
                (NO ID) 5. '{' expression+ '}' (';')?
                (NO ID) 6. 'let' Identifier ':' Identifier ('<-' expression)? (',' Identifier ':' Identifier ('<-' expression)?)* 'in' expression (';')? 
                (NO ID) 7. 'case' expression 'of' (Identifier ':' Identifier '=>' expression ';')+ 'esac' (';')?
        8. 'new' Identifier (';')?
                (NO ID) 9. '~' expression (';')?
                (NO ID) 10. 'isvoid' expression (';')?
                (NO ID) 11. expression ('*' | '/') expression (';')?
                (NO ID) 12. expression ('+' | '-') expression (';')?
                (NO ID) 13. expression ('<=' | '<' | '=') expression (';')?
                (NO ID) 14. 'not' expression (';')?
                (NO ID) 15. '(' expression ')' (';')?
        16. <assoc=right> Identifier '<-' expression (';')?
        17. Identifier (';')?
                (NO ID) 18. Integer (';')?
                (NO ID) 19. String (';')?
                (NO ID) 20. True (';')?
                (NO ID) 21. False (';')?;
    */
    @SuppressWarnings("incomplete-switch")
    @Override
    public void enterExpression (ExpressionContext ctx)
    {
        // Expression could be inside attribute (not mandatory) or inside method (mandatory).
        // Every identifier can be declared inside expression
        ExpressionTypes eType = GenericUtils.identifyTypeExpression (ctx);
        TestUtils._assert_false (errorMessage + "Expression is unknown (enterExpression).", eType == ExpressionTypes.UNKNOWN, true);
        
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        int size_i = identifiers.size ();
        
        switch (eType) {
        case SEQUENCE:
        case LET_INIT:
        case LET_NO_INIT:
        case CASE:
            this.currentScope = this.scopes.get (ctx);
            
            return; // We must not check the identifier if we are inside sequence, let and case. It is enough to change the scope
            
        // 1. Identifier (is a Type) after '@' must begin with a upper case character. Moreover, the Type must exist
        //    Identifier after '.' is a name of a method
        case STATIC_DISPATCH:
        {
            if (size_i != 2)
                IOUtils.printError ("Resolving phase, number of identifiers must be 2 instead of " + size_i + 
                                    ". Line: " + ctx.getStart ().getLine (), true);
            
            // First identifier is a Type, second is the name of the method of the Type
            String type       = identifiers.get (0).toString (),
                   methodName = identifiers.get (1).toString ();
            
            if (type.length () > 0 && !StringUtils.isUpperCaseChar (type.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), type);
            if (methodName.length () > 0 && !StringUtils.isLowerCaseChar (methodName.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), methodName);
            
            CoolClass class_ = GenericUtils.getInstanceClassByClassName (this.coolClasses, type);
            TestUtils._assert_not_null (errorMessage + "class_ is null.", class_, true);
            TestUtils._assert_true (errorMessage + "Method " + methodName + " not found in class " + this.currentClass.getName (), class_.hasMethod (methodName), true);
            
            break;
        }
            
        // 2. Identifier must be a name of a method
        case DISPATCH:
        {
            // Identifier is the name of a method. This method must be defined in the class itself or in the parent class or in the parent of parent
            // class and so on
            // Dispatch might be:
            //      exp.method_name ()
            // or:
            //      method_name ()
            // In this listener, we will check only the second form. The first, we will be checked in the type checking
            if (size_i != 1)
                IOUtils.printError ("Resolving phase, number of identifiers must be 1 instead of " + size_i + 
                                    ". Line: " + ctx.getStart ().getLine (), true);
            
            String methodName = identifiers.get (0).toString ();
            
            int nChild = ctx.getChildCount ();
            
            // If the first child is the name of the method, we are inside the second form
            if (nChild > 0 && ctx.getChild (0).toString ().equals (methodName)) {
                if (methodName.length () > 0 && !StringUtils.isLowerCaseChar (methodName.charAt (0)))
                    ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), methodName);
                
                TestUtils._assert_not_null (errorMessage + "Current class is null.", this.currentClass, true);
                
                if (!this.currentClass.hasMethod (methodName))
                    if (!GenericUtils.parentClassesHaveMethod (this.currentClass, methodName))
                        IOUtils.printError ("Method " + methodName + " not found in class " + this.currentClass.getName () + " and in its parent class" +
                                            ". Line: " + ctx.getStart ().getLine (), true);
            }
            
            break;
        }
        
        // 8. Identifier is a Type
        case NEW:
        {
            if (size_i != 1)
                IOUtils.printError ("Resolving phase, number of identifiers must be 1 instead of " + size_i + 
                                    ". Line: " + ctx.getStart ().getLine (), true);
            
            String type = identifiers.get (0).toString ();
            
            if (type.length () > 0 && !StringUtils.isUpperCaseChar (type.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.TYPE_IDENTIFIER_CAPITAL_LETTER, ctx.getStart ().getLine (), type);
            
            break;
        }
        
        // 16. Identifier is a variable (attribute or local object)
        // 17. Identifier is a variable (attribute or local object)
        case ASSIGN:
        case IDENTIFIER:
        {
            if (size_i != 1)
                IOUtils.printError ("Resolving phase, number of identifiers must be 1 instead of " + size_i + 
                                    ". Line: " + ctx.getStart ().getLine (), true);
            
            String id = identifiers.get (0).toString ();
            
            if (id.length () > 0 && !StringUtils.isLowerCaseChar (id.charAt (0)))
                ErrorMessage.printError (ErrorMessage.Error.OBJECT_IDENTIFIER_LOWER_LETTER, ctx.getStart ().getLine (), id);
            
            if (!id.equals ("self")) {
                Symbol s = this.currentScope.resolve (id);
                
                if (s == null)
                    ErrorMessage.printError (ErrorMessage.Error.ERROR, ctx.getStart ().getLine (), id + " is not declared");
            }
            
            break;
        }
        }
    }

    
    @SuppressWarnings("incomplete-switch")
    @Override
    public void exitExpression (ExpressionContext ctx)
    {
        ExpressionTypes eType = GenericUtils.identifyTypeExpression (ctx);
        TestUtils._assert_false (errorMessage + "Expression is unknown (exitExpression).", eType == ExpressionTypes.UNKNOWN, true);
        
        switch (eType) {
        case SEQUENCE:
        case LET_INIT:
        case LET_NO_INIT:
        case CASE:
            this.currentScope = this.currentScope.getEnclosingScope (); // Pop scope
        }
    }

    
    @Override
    public void enterClassDeclaration (ClassDeclarationContext ctx)
    {
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String className = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        TestUtils._assert_not_null (errorMessage + "Class name is null.", className, true);
        
        this.currentClass = GenericUtils.getInstanceClassByClassName (this.coolClasses, className);
        TestUtils._assert_not_null (errorMessage + "Current class name is null.", this.currentClass, true);
        
        this.currentScope = this.scopes.get (ctx);
    }

    
    @Override
    public void exitClassDeclaration (ClassDeclarationContext ctx)
    {
        this.currentScope = this.currentScope.getEnclosingScope ();
        
        this.currentClass = null;
    }
    
    
    @Override
    public void enterAttributeDeclaration (AttributeDeclarationContext ctx) { }

    
    @Override
    public void exitAttributeDeclaration (AttributeDeclarationContext ctx) { }

    
    @Override
    public void enterMethodDeclaration (MethodDeclarationContext ctx)
    {
        this.currentScope = this.scopes.get (ctx);
    }

    
    @Override
    public void exitMethodDeclaration (MethodDeclarationContext ctx)
    {
        this.currentScope = this.currentScope.getEnclosingScope ();
    }
    
    
    @Override
    public void enterFormal (FormalContext ctx) { }

    
    @Override
    public void exitFormal (FormalContext ctx) { }

    
    @Override
    public void enterProgram (ProgramContext ctx) { }

    
    @Override
    public void exitProgram (ProgramContext ctx) { }
    
    
    @Override
    public void enterEveryRule (ParserRuleContext arg0) { }

    
    @Override
    public void exitEveryRule (ParserRuleContext arg0) { }

    
    @Override
    public void visitErrorNode (ErrorNode arg0) { }

    
    @Override
    public void visitTerminal (TerminalNode arg0) { }

    
    @Override
    public void enterFeature (FeatureContext ctx) { }

    
    @Override
    public void exitFeature (FeatureContext ctx) { }
}
