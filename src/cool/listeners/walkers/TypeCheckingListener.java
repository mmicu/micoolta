package cool.listeners.walkers;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import cool.data.structure.object.CoolClass;
import cool.data.structure.types.ExpressionTypes;

import cool.scope.Scope;

import cool.typechecking.TypeEnvironment;

import utils.GenericUtils;
import utils.TestUtils;

import antlr.coolListener;

import antlr.coolParser.AttributeDeclarationContext;
import antlr.coolParser.ClassDeclarationContext;
import antlr.coolParser.ExpressionContext;
import antlr.coolParser.FeatureContext;
import antlr.coolParser.FormalContext;
import antlr.coolParser.MethodDeclarationContext;
import antlr.coolParser.ProgramContext;

public class TypeCheckingListener implements coolListener
{
    public static final boolean DEBUG = false;
    
    private AttributeDeclarationContext attributeContext;
    
    private MethodDeclarationContext methodContext;
    
    private CoolClass class_;
    
    private ArrayList<CoolClass> coolClasses;
    
    private ParseTreeProperty<Scope> scope;
    
    private Scope currentScope;
    
    private static final String errorMessage = "[Type checking Listener] Error! ";
    
    
    public TypeCheckingListener ()
    {
        this (new ArrayList<CoolClass> (), new ParseTreeProperty<Scope> ());
    }
    
    
    public TypeCheckingListener (ArrayList<CoolClass> coolClasses, ParseTreeProperty<Scope> scope)
    {
        this.coolClasses      = coolClasses;
        this.scope            = scope;
        this.attributeContext = null;
        this.methodContext    = null;
        this.class_           = null;
        this.currentScope     = null;
    }
    
    
    @Override
    public void enterClassDeclaration (ClassDeclarationContext ctx)
    {
        this.currentScope = this.scope.get (ctx);
        
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String className = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        TestUtils._assert_not_null (errorMessage + "Class name is null.", className);
        
        this.class_ = GenericUtils.getInstanceClassByClassName (this.coolClasses, className);
        TestUtils._assert_not_null (errorMessage + "Class is null.", this.class_);
    }

    
    @Override
    public void exitClassDeclaration (ClassDeclarationContext ctx)
    {
        this.currentScope = this.currentScope.getEnclosingScope ();
        
        this.class_ = null;
    }
    
    
    @Override
    public void enterAttributeDeclaration (AttributeDeclarationContext ctx)
    {
        this.attributeContext = ctx;
        
        // Attribute might not have expression to initialize it
        if (ctx.expression () != null)
            this.callTypeEnvironment (ctx.expression (), ExpressionTypes.ATTR_INIT);
    }

    
    @Override
    public void exitAttributeDeclaration (AttributeDeclarationContext ctx)
    {
        this.attributeContext = null;
    }

    
    @Override
    public void enterMethodDeclaration (MethodDeclarationContext ctx)
    {
        this.methodContext = ctx;
        
        this.currentScope = this.scope.get (ctx);
        
        this.callTypeEnvironment (ctx.expression (), ExpressionTypes.METHOD);
    }

    
    @Override
    public void exitMethodDeclaration (MethodDeclarationContext ctx)
    {
        this.currentScope = this.currentScope.getEnclosingScope ();
        
        this.methodContext = null;
    }
    
    
    @Override
    public void enterFeature (FeatureContext ctx) { }

    
    @Override
    public void exitFeature (FeatureContext ctx) { }
    
    
    @Override
    public void enterEveryRule (ParserRuleContext arg0) { }

    
    @Override
    public void exitEveryRule (ParserRuleContext arg0) { }

    
    @Override
    public void visitErrorNode (ErrorNode arg0) { }

    
    @Override
    public void visitTerminal (TerminalNode arg0) { }

    
    @Override
    public void enterExpression (ExpressionContext ctx) { }

    
    @Override
    public void exitExpression (ExpressionContext ctx) { }

    
    @Override
    public void enterFormal (FormalContext ctx) { }

    
    @Override
    public void exitFormal (FormalContext ctx) { }

    
    @Override
    public void enterProgram (ProgramContext ctx) { }

    
    @Override
    public void exitProgram (ProgramContext ctx) { }
    
    
    private void callTypeEnvironment (ExpressionContext e, ExpressionTypes eType)
    {
        // Asserts ~ Start
        TestUtils._assert_not_null (errorMessage + "Expression is null.", e, true);
        TestUtils._assert_not_null (errorMessage + "Class is null.", this.class_, true);
        TestUtils._assert_not_null (errorMessage + "Scope is null.", this.scope, true);
        TestUtils._assert_not_null (errorMessage + "Current scope is null.", this.currentScope, true);
        
        TestUtils._assert_false (
                errorMessage + "Expression refers to attribute or method.", 
                eType != ExpressionTypes.ATTR_INIT && eType != ExpressionTypes.ATTR_NO_INIT && eType != ExpressionTypes.METHOD, 
                true
        );
        TestUtils._assert_false (errorMessage + "Context is null.", this.attributeContext == null && this.methodContext == null, true);
        TestUtils._assert_false (errorMessage + "Cool Classes empty.", this.coolClasses == null || this.coolClasses.size () == 0, true);
        // Asserts ~ End
        
        // Create type environment for attribute or method
        TypeEnvironment te = new TypeEnvironment (this.class_.getName (), this.coolClasses, this.scope);
        
        // Set the current scope
        te.setCurrentScope (this.currentScope);
        
        // Set the entire scope
        te.setEntireScope (this.scope);
        
        // Set attribute context
        te.setAttributeContext (this.attributeContext);
        
        // Set method context
        te.setMethodContext (this.methodContext);
        
        // Evaluate expression
        te.evaluate (e, eType);
    }
}
