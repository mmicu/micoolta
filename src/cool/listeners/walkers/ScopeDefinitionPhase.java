package cool.listeners.walkers;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import utils.GenericUtils;
import utils.IOUtils;
import utils.TestUtils;

import cool.data.structure.object.CoolClass;
import cool.data.structure.symbols.IdentifierSymbol;
import cool.data.structure.symbols.MethodSymbol;
import cool.data.structure.types.ExpressionTypes;
import cool.data.structure.types.Type;

import cool.scope.*;

import antlr.coolListener;

import antlr.coolParser.AttributeDeclarationContext;
import antlr.coolParser.ClassDeclarationContext;
import antlr.coolParser.ExpressionContext;
import antlr.coolParser.FeatureContext;
import antlr.coolParser.FormalContext;
import antlr.coolParser.MethodDeclarationContext;
import antlr.coolParser.ProgramContext;

public class ScopeDefinitionPhase implements coolListener
{
    public static final boolean DEBUG = false;
    
    private ParseTreeProperty<Scope> scopes;
    
    private ArrayList<ClassScope> examinedClassScope;
    
    private Scope currentScope;
    
    private ArrayList<CoolClass> coolClasses;
    
    private CoolClass currentClass;
    
    private static final String errorMessage = "[Scope definition phase] Error! ";
    
    
    public ScopeDefinitionPhase (ParseTreeProperty<Scope> scopes, ArrayList<ClassScope> examinedClassScope, ArrayList<CoolClass> coolClasses)
    {
        this.scopes             = scopes;
        this.examinedClassScope = examinedClassScope;
        this.coolClasses        = coolClasses;
        this.currentScope       = null;
        this.currentClass       = null;
        
        this.setupScopeStandardClasses ();
    }
    

    // We must check type in:
    //  - STATIC_DISPATCH:  @TYPE;
    //  - LET:              ID : TYPE, ..., ID : TYPE;
    //  - CASE:             ID : TYPE, ..., ID : TYPE;
    //  - NEW:              TYPE;
    @SuppressWarnings("incomplete-switch")
    @Override
    public void enterExpression (ExpressionContext ctx)
    {
        ExpressionTypes eType = GenericUtils.identifyTypeExpression (ctx);
        TestUtils._assert_false (errorMessage + "Expression is unknown (enterExpression).", eType == ExpressionTypes.UNKNOWN, true);
        
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        int size_i = identifiers.size ();
        
        switch (eType) {
        case STATIC_DISPATCH:
        {
            ArrayList<String> nodes = GenericUtils.getListChildNodeFromExpressionContext (ctx);

            if (nodes.lastIndexOf ("@") == -1)
                IOUtils.printError ("No '@' found in static dispatch. Line: " + ctx.getStart ().getLine (), true);
            if (size_i != 2)
                IOUtils.printError ("Number of identifiers must be 2 instead of " + size_i + ". Line: " + ctx.getStart ().getLine (), true);
            
            // @TYPE, TYPE must exist
            String type = identifiers.get (0).toString ();
            
            if (!GenericUtils.classExists (this.coolClasses, type))
                IOUtils.printError ("Type " + type + " in the static dispatch does not exist. Line: " + ctx.getStart ().getLine ());
            
            break;
        }
        
        case NEW:
        {
            if (size_i != 1)
                IOUtils.printError ("Number of identifiers must be 2 instead of " + size_i + ". Line: " + ctx.getStart ().getLine (), true);
            
            // new TYPE, TYPE must exist
            String type = identifiers.get (0).toString ();
            
            if (!GenericUtils.classExists (this.coolClasses, type))
                IOUtils.printError ("Type " + type + " in new expression does not exist. Line: " + ctx.getStart ().getLine ());
            
            break;
        }
        
        case SEQUENCE:
        case LET_INIT:
        case LET_NO_INIT:
        case CASE:
            this.currentScope = new LocalScope (this.currentScope);
            
            this.saveScope (ctx, this.currentScope);
            
            // Add identifiers inside current scope
            int k = 0;
            
            // 'let' Identifier ':' Identifier ('<-' expression)? (',' Identifier ':' Identifier ('<-' expression)?)* 'in' expression (';')?
            // 'case' expression 'of' (Identifier ':' Identifier '=>' expression ';')+ 'esac' (';')?
            if (size_i % 2 != 0)
                IOUtils.printError ("Scope definition phase, expression " + eType.name () + " not all identifiers have a type", true);
            
            while (k < size_i) {
                String identifier = identifiers.get (k++).toString (),
                       type       = identifiers.get (k++).toString ();
                
                if (!GenericUtils.classExists (this.coolClasses, type))
                    IOUtils.printError ("Type " + type + " does not exist. Line: " + ctx.getStart ().getLine ());

                this.defineIdentifier (identifier, type);
            }
            
            break;
        }
    }

    
    @SuppressWarnings("incomplete-switch")
    @Override
    public void exitExpression (ExpressionContext ctx)
    {
        ExpressionTypes eType = GenericUtils.identifyTypeExpression (ctx);
        TestUtils._assert_false (errorMessage + "Expression is unknown (enterExpression).", eType == ExpressionTypes.UNKNOWN, true);
        
        switch (eType) {
        case SEQUENCE:
        case LET_INIT:
        case LET_NO_INIT:
        case CASE:
            this.currentScope = this.currentScope.getEnclosingScope ();
        }
    }

    
    @Override
    public void enterClassDeclaration (ClassDeclarationContext ctx)
    {
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String className       = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        // If class has not parent class, Object is its parent
        String classParentName = (identifiers.size () > 1) ? identifiers.get (1).toString () : "Object";
        
        TestUtils._assert_not_null (errorMessage + "Class is null.", className);
        
        this.currentClass = GenericUtils.getInstanceClassByClassName (this.coolClasses, className);
        TestUtils._assert_not_null (errorMessage + "this.currentClass is null.", this.currentClass);
        
        // There is the possibility that we do not add the parent class in the WalkerLister.
        // So, we can add the parent in this listener
        if (this.currentClass.getParent () == null) {
            CoolClass parent = GenericUtils.getInstanceClassByClassName (this.coolClasses, classParentName);
            
            for (int k = 0, size_c = this.coolClasses.size (); k < size_c; k++)
                if (this.coolClasses.get (k).getName ().equals (className))
                    this.coolClasses.get (k).setParent (parent);
        }
        
        ClassScope scopeParentClass = GenericUtils.getClassScopeByClassName (this.examinedClassScope, classParentName);
        TestUtils._assert_not_null (errorMessage + "Impossible to get scope of the parent class.", scopeParentClass);
        
        // First class that I check
        if (this.currentScope == null) {
            ClassScope c = new ClassScope (scopeParentClass, className);
            
            this.currentScope = c;
            
            this.saveScope (ctx, c);
        }
    }

    
    @Override
    public void exitClassDeclaration (ClassDeclarationContext ctx)
    {
        this.examinedClassScope.add ((ClassScope) this.currentScope);
        
        this.currentScope = null;
        
        this.currentClass = null;
    }


    @Override
    public void enterAttributeDeclaration (AttributeDeclarationContext ctx)
    {
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String attributeName = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        String attributeType = (identifiers.size () > 1) ? identifiers.get (1).toString () : null;
        
        TestUtils._assert_not_null (errorMessage + "Attribute name is null.", attributeName, true);
        TestUtils._assert_not_null (errorMessage + "Attribute type is null.", attributeType, true);
        TestUtils._assert_not_null (errorMessage + "currentClass is null.", this.currentClass, true);
        
        // Type of attribute must be declared
        if (!GenericUtils.classExists (this.coolClasses, attributeType))
            IOUtils.printError ("Attribute " + attributeName + " has an undeclared type " + attributeType + ". Line: " + ctx.getStart ().getLine ());
        
        // Insert attribute in the scope
        this.defineIdentifier (attributeName, attributeType);
    }

    
    @Override
    public void exitAttributeDeclaration (AttributeDeclarationContext ctx) { }

    
    @Override
    public void enterMethodDeclaration (MethodDeclarationContext ctx)
    {
        List<TerminalNode> identifiers = ctx.Identifier ();
        
        String methodName        = (identifiers.size () > 0) ? identifiers.get (0).toString () : null;
        String methodReturnValue = (identifiers.size () > 1) ? identifiers.get (1).toString () : null;
        
        TestUtils._assert_not_null (errorMessage + "Method name is null.", methodName, true);
        TestUtils._assert_not_null (errorMessage + "Return value of method is null.", methodReturnValue, true);
        TestUtils._assert_not_null (errorMessage + "Current scope is null.", this.currentScope, true);
        TestUtils._assert_not_null (errorMessage + "currentClass type is null.", this.currentClass, true);
        TestUtils._assert_true (errorMessage + "Class " + this.currentClass.getName () + " has no method " + methodName + ".", this.currentClass.hasMethod (methodName), true);
        
        // Type of the return value of the method must be declared
        if (!GenericUtils.classExists (this.coolClasses, methodReturnValue))
            IOUtils.printError ("Method " + methodName + " has an undeclared return value type " + methodReturnValue + 
                                ". Line: " + ctx.getStart ().getLine ());
        
        MethodSymbol m = new MethodSymbol (methodName, new Type (methodReturnValue), this.currentScope);
        
        // Define method scope
        this.currentScope.define (m);
        // Save the context in the scope
        this.saveScope (ctx, m);
        // Update the current scope
        this.currentScope = m;
        
        
         // Checking formal parameters method
        List<FormalContext> fp = ctx.formal ();
        
        for (int k = 0, size = fp.size (); k < size; k++) {
            List<TerminalNode> params = fp.get (k).Identifier ();
            
            String paramName = (params.size () > 0) ? params.get (0).toString () : null;
            String paramType = (params.size () > 1) ? params.get (1).toString () : null;
            
            TestUtils._assert_not_null (errorMessage + "Parameter name is null.", paramName, true);
            TestUtils._assert_not_null (errorMessage + "Parameter type is null.", paramType, true);
            
            // Type of the return value of the method must be declared
            if (!GenericUtils.classExists (this.coolClasses, paramType))
                IOUtils.printError ("Parameter " + paramName + " has an undeclared type " + paramType + ". Line: " + ctx.getStart ().getLine ());
            
            this.defineIdentifier (paramName, paramType);
        }
    }
    
    
    @Override
    public void exitMethodDeclaration (MethodDeclarationContext ctx)
    {
        this.currentScope = this.currentScope.getEnclosingScope (); // Pop scope
    }
    
    
    @Override
    public void enterFeature (FeatureContext ctx) { }

    
    @Override
    public void exitFeature (FeatureContext ctx) { }


    @Override
    public void enterFormal (FormalContext ctx) { }


    @Override
    public void exitFormal (FormalContext ctx) { }


    @Override
    public void enterProgram (ProgramContext ctx) { }


    @Override
    public void exitProgram (ProgramContext ctx) { }

    
    @Override
    public void enterEveryRule (ParserRuleContext arg0) { }


    @Override
    public void exitEveryRule (ParserRuleContext arg0) { }


    @Override
    public void visitErrorNode (ErrorNode arg0) { }

    
    @Override
    public void visitTerminal (TerminalNode arg0) { }

    
    private void setupScopeStandardClasses ()
    {
        if (examinedClassScope.size () > 0)
            return;
        
        // First call to this listener
        // This listener is called for each file that we specify in the "o" option
        ClassScope c;
        
        // Object class
        c = new ClassScope (null, "Object");
        c.define (new MethodSymbol ("abort", new Type ("Object"), c));
        c.define (new MethodSymbol ("type_name", new Type ("String"), c));
        c.define (new MethodSymbol ("copy", new Type ("SELF_TYPE"), c));
        this.examinedClassScope.add (c);
        
        
        // IO class
        c = new ClassScope (null, "IO");
        c.define (new MethodSymbol ("out_string", new Type ("String"), c));
        c.define (new MethodSymbol ("out_int", new Type ("String"), c));
        c.define (new MethodSymbol ("in_string", new Type ("String"), c));
        c.define (new MethodSymbol ("in_int", new Type ("Int"), c));
        this.examinedClassScope.add (c);
        
        
        // Int class
        // No methods
        c = new ClassScope (null, "Int");
        this.examinedClassScope.add (c);
        
        
        // String class
        c = new ClassScope (null, "String");
        c.define (new MethodSymbol ("length", new Type ("Int"), c));
        c.define (new MethodSymbol ("concat", new Type ("String"), c));
        c.define (new MethodSymbol ("substr", new Type ("String"), c));
        this.examinedClassScope.add (c);
        
        
        // Bool class
        // No methods
        c = new ClassScope (null, "Bool");
        this.examinedClassScope.add (c);
    }
  
    
    private void defineIdentifier (String name, String type)
    {
        TestUtils._assert_not_null (errorMessage + "Current scope is null (defineIdentifier).", this.currentScope, true);
        
        this.currentScope.define (new IdentifierSymbol (name, new Type (type)));
    }
    
    
    private void saveScope (ParserRuleContext ctx, Scope scope)
    {
        this.scopes.put (ctx, scope);
    }
}
