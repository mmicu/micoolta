package cool.listeners.error;

import java.util.Collections;
import java.util.List;

import main.micoolta;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import cool.error.ErrorMessage;
import utils.IOUtils;
import antlr.coolParser;

public class CoolErrorListener extends BaseErrorListener
{
    @Override
    public void syntaxError (Recognizer<?, ?> recognizer, Object offendingSymbol, int line, 
                             int charPositionInLine, String msg, RecognitionException e)
    {
        List<String> stack = ((coolParser) recognizer).getRuleInvocationStack (); Collections.reverse (stack);
        
        System.err.println ("\t" + IOUtils.getColoredString ("[ERROR] ", IOUtils.Color.RED) + 
                            "Line " + line + ": " + charPositionInLine + ", " + msg);
        
        if (!micoolta.ERROR)
            micoolta.ERROR = true;
        if (!ErrorMessage.ERROR_PRINTED)
            ErrorMessage.ERROR_PRINTED = true;
    }
}
