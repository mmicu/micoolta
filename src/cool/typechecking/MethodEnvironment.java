package cool.typechecking;

import java.util.ArrayList;

import antlr.coolParser.ExpressionContext;

import utils.GenericUtils;
import utils.TestUtils;

import cool.data.structure.object.CoolClass;
import cool.data.structure.object.CoolMethod;
import cool.data.structure.object.CoolParameter;
import cool.data.structure.types.Type;


/**
 * In the type checking, method environment is a function:
 * 
 *          M (C, f) = (T_1, T_2, ..., T_n-1, T_n)
 * where:
 *      - C => class name;
 *      - f => method name;
 *      - T_1, ..., T_n-1 => types of parameters;
 *      - T_n => return value.
 */
public class MethodEnvironment
{
    // The array consists about types of the method: types of parameters and return value of the method
    // ==>
    // res.get (res.size () - 1) contains return value
    public static ArrayList<Type> mapping (String className, String methodName, ArrayList<CoolClass> coolClasses, ExpressionContext e)
    {
        int line = e.getStart ().getLine ();
        
        // Asserts ~ Start
        TestUtils._assert_not_null ("e is null", e);
        TestUtils._assert_not_null ("className is null. Line: " + line, className);
        TestUtils._assert_not_null ("methodName is null. Line: " + line, methodName);
        
        TestUtils._assert_false ("coolClasses is not set. Line: " + line, coolClasses == null || coolClasses.size () == 0);
        // Asserts ~ End
        
        
        ArrayList<Type> res = new ArrayList<Type> ();
        
        CoolClass class_ = GenericUtils.getInstanceClassByClassName (coolClasses, className);
        TestUtils._assert_not_null ("MethodEnvironment. Class=" + className + ",method=" + methodName + ". Class null. " + line, class_);
        
        boolean found = false;
        
        ArrayList<CoolMethod> methodsClass = class_.getMethods ();
        TestUtils._assert_not_null ("MethodEnvironment. Class=" + className + ",method=" + methodName + ". methodsClass null. " + line, methodsClass);
        
        for (int k = 0, size_m = methodsClass.size (); k < size_m; k++) {
            CoolMethod methodK = methodsClass.get (k);
            
            // Method found
            if (methodK.getName ().equals (methodName)) {
                ArrayList<CoolParameter> parameters = methodK.getFormalParameters ();
                TestUtils._assert_not_null ("MethodEnvironment. Class=" + className + ",method=" + methodName + ". Parameters null. " + line, parameters);
                
                for (int j = 0, size_p = parameters.size (); j < size_p; j++)
                    res.add (parameters.get (j).getType ());
                
                TestUtils._assert_not_null ("MethodEnvironment. Class=" + className + ",method=" + methodName + ". Return value null. " + line, methodK.getReturnValue ());
                
                // Final record is the return type of the method
                res.add (methodK.getReturnValue ());
                
                found = true;
                
                break;
            }
        }
        
        // Method not found in the current class. Is it defined in the parent class?
        if (!found) {
            CoolMethod method = GenericUtils.getMethodFromParentsClasses (class_, methodName);
            TestUtils._assert_not_null ("MethodEnvironment. Class=" + className + ",method=" + methodName + ". method null. " + line, method);
            
            // Method found in the parent class
            ArrayList<CoolParameter> parameters = method.getFormalParameters ();
            TestUtils._assert_not_null ("MethodEnvironment. Class=" + className + ",method=" + methodName + ". Parameters null (2). " + line, parameters);
            
            for (int j = 0, size_p = parameters.size (); j < size_p; j++)
                res.add (parameters.get (j).getType ());
            
            TestUtils._assert_not_null ("MethodEnvironment. Class=" + className + ",method=" + methodName + ". Return value null (2). " + line, method.getReturnValue ());
            
            // Final record is the return type of the method
            res.add (method.getReturnValue ());
        }
        
        return res;
    }
}
