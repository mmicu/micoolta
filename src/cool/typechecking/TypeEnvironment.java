package cool.typechecking;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import cool.data.structure.object.CoolClass;
import cool.data.structure.symbols.IdentifierSymbol;
import cool.data.structure.symbols.Symbol;
import cool.data.structure.types.Bool_;
import cool.data.structure.types.ExpressionTypes;
import cool.data.structure.types.Int_;
import cool.data.structure.types.String_;
import cool.data.structure.types.Type;

import cool.error.ErrorMessage;

import cool.scope.Scope;

import utils.GenericUtils;
import utils.IOUtils;
import utils.StringUtils;
import utils.TestUtils;

import antlr.coolParser.AttributeDeclarationContext;
import antlr.coolParser.ExpressionContext;
import antlr.coolParser.MethodDeclarationContext;

public class TypeEnvironment
{
    private String currentClass;
    
    private ArrayList<CoolClass> coolClasses;
    
    private ParseTreeProperty<Scope> scope;
    
    private Scope currentScope;
    
    private ParseTreeProperty<Scope> entireScope;
    
    private AttributeDeclarationContext attributeContext;
    
    private MethodDeclarationContext methodContext;
    
    
    public TypeEnvironment ()
    {
        this ("", new ArrayList<CoolClass> (), new ParseTreeProperty<Scope> ());
    }
    
    
    public TypeEnvironment (String currentClass, ArrayList<CoolClass> coolClasses, ParseTreeProperty<Scope> scope)
    {
        this.currentClass  = currentClass;
        this.coolClasses   = coolClasses;
        this.scope         = scope;
        this.currentScope  = null;
    }
    

    @SuppressWarnings("incomplete-switch")
    public Type evaluate (ExpressionContext expression, ExpressionTypes expressionType)
    {
        TestUtils._assert_not_null ("Variable expression is null.", expression);
        TestUtils._assert_not_null ("Variable currentClass is null.", this.currentClass);
        TestUtils._assert_not_null ("Variable scope is null.", this.scope);
        TestUtils._assert_not_null ("Variable currentScope is null.", this.currentScope);
        TestUtils._assert_not_null ("Variable entireScope is null.", this.entireScope);
        
        TestUtils._assert_false ("Variable expression is empty.", expression.isEmpty ());
        TestUtils._assert_false ("Variable coolClasses is null or empty.", this.coolClasses == null || this.coolClasses.size () == 0);
        
        // Change the scope
        switch (expressionType) {
        case SEQUENCE:
        case LET_INIT:
        case LET_NO_INIT:
        case CASE:
            this.currentScope = this.entireScope.get (expression);
            
            TestUtils._assert_not_null ("Variable currentScope is null", this.currentScope, true);
        }
        
        List<ExpressionContext> expressions = expression.expression ();
        List<TerminalNode> identifiers      = expression.Identifier ();
        
        TestUtils._assert_not_null ("Variable expressions is null", expressions, true);
        TestUtils._assert_not_null ("Variable identifiers is null", identifiers, true);
        
        int size_e  = expressions.size (),
            size_i  = identifiers.size (),
            lineExp = expression.getStart ().getLine ();
        
        switch (expressionType) {
        // --- Rule [arith] (pag. 22) - Start
        // expression '+' expression (';')?
        // |
        // expression '-' expression (';')?
        // |
        // expression '*' expression (';')?
        // |
        // expression '/' expression (';')?
        case ARITHMETIC:
        {
            if (size_e != 2)
                this.printError ("ARITHMETIC", "Number of expressions are " + size_e + " instead of 2", expression, true);
            
            ExpressionContext exp_1 = expressions.get (0),
                              exp_2 = expressions.get (1);
            
            Type type_1 = this.evaluate (exp_1, GenericUtils.identifyTypeExpression (exp_1)),
                 type_2 = this.evaluate (exp_2, GenericUtils.identifyTypeExpression (exp_2));
            
            TestUtils._assert_not_null ("ARITHMETIC, type_1 is null", type_1, true);
            TestUtils._assert_not_null ("ARITHMETIC, type_2 is null", type_2, true);
            
            if ((type_1 instanceof Int_ || type_1.getType ().equals ("Int")) && (type_2 instanceof Int_ || type_2.getType ().equals ("Int")))
                return new Type ("Int");
            
            // One or both of two types is/are not Int
            ErrorMessage.printError (
                    ErrorMessage.Error.ERROR, 
                    lineExp, 
                    "You cannot make arithmetic expression between types " + type_1.getType () + " and " + type_2.getType ()
            );

            break;
        }
        // --- Rule arith (pag. 22) - End
        
            
        // --- Rule [assign] (pag. 19) - Start
        // Identifier '<-' expression (';')?
        case ASSIGN:
        {
            if (size_e != 1)
                this.printError ("ASSIGN", "Number of expressions are " + size_e + " instead of 1. " + expression.toStringTree (), expression, true);
            if (size_i != 1)
                this.printError ("ASSIGN", "Number of identifiers are " + size_i + " instead of 1", expression, true);
        
            // Both size_e and size_i are equal to 1
            // get T
            Type type = ObjectEnvironment.mapping (this.coolClasses, 
                                                   this.currentClass, 
                                                   this.currentScope, 
                                                   expression, 
                                                   identifiers.get (0).toString ());
            
            TestUtils._assert_not_null ("ASSIGN, type is null", type, true);
            
            ExpressionContext exp = expressions.get (0);
            
            // get T'
            Type type_1 = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
            
            TestUtils._assert_not_null ("ASSIGN, type_1 is null", type_1, true);
            
            CoolClass class_  = GenericUtils.getInstanceClassByClassName (coolClasses, type.getType ()),
                      class_1 = GenericUtils.getInstanceClassByClassName (coolClasses, type_1.getType ());
            
            TestUtils._assert_not_null ("ASSIGN, class_ is null (" + type.getType () + ")", class_, true);
            TestUtils._assert_not_null ("ASSIGN, class_1 is null", class_1, true);
            
            // No conformance between classes
            if (!class_1.conformance (class_))
                ErrorMessage.printError (
                        ErrorMessage.Error.CONFORMANCE, 
                        expression.getStart ().getLine (), 
                        class_1.getName (), 
                        class_.getName (),
                        "ASSIGN"
                );
            
            return type_1;
        }
        // --- Rule [assign] (pag. 19) - End
            
            
        // --- Rule [attr-init] (pag. 22) - Start
        // Identifier ':' Identifier ('<-' expression)? (';')?;
        case ATTR_INIT:
        {
            TestUtils._assert_not_null ("ATTR_INIT, attributeContext is null", attributeContext, true);
            
            List<TerminalNode> identifiersAttr = this.attributeContext.Identifier ();
            if (identifiersAttr.size () < 2)
                this.printError ("ATTR_INIT", "Identifiers number must be >=2 instead of " + size_i, expression, true);

            // id (0) => name of attribute;
            // id (1) => type of attribute.
            Type t_0 = new Type (identifiersAttr.get (1).toString ());
            
            // O_C[SELF_TYPE_C/self], M, C |- e_1 : T_1
            // I've already made the substitution between SELF_TYPE_C and self in the ObjectEnvironment class (static method "mapping")
            Type t_1 = this.evaluate (expression, GenericUtils.identifyTypeExpression (expression));
            
            TestUtils._assert_not_null ("ATTR_INIT, t_1 is null", t_1, true);
            
            CoolClass class_0 = GenericUtils.getInstanceClassByClassName (coolClasses, t_0.getType ()),
                      class_1 = GenericUtils.getInstanceClassByClassName (coolClasses, t_1.getType ());
            
            TestUtils._assert_not_null ("ATTR_INIT, class_0 is null", class_0, true);
            TestUtils._assert_not_null ("ATTR_INIT, class_1 is null", class_1, true);
            
            // No conformance between classes
            if (!class_1.conformance (class_0))
                ErrorMessage.printError (
                        ErrorMessage.Error.CONFORMANCE, 
                        expression.getStart ().getLine (), 
                        class_1.getName (), 
                        class_0.getName (),
                        "ATTR_INIT"
                );
            
            return t_1;
        }
        // --- Rule [attr-init] (pag. 22) - End
           
            
        // --- Rule [attr-no-init] (pag. 22)
        // Identifier ':' Identifier (';')?;
        case ATTR_NO_INIT:
        {
            TestUtils._assert_not_null ("ATTR_INIT, attributeContext is null", attributeContext, true);
                        
            List<TerminalNode> identifiersAttr = this.attributeContext.Identifier ();
            
            if (identifiersAttr == null || identifiersAttr.size () < 2)
                this.printError ("ATTR_NO_INIT", "Identifiers number must be >=2 instead of " + size_i, expression, true);
            
            return new Type (identifiersAttr.get (1).toString ());
        }
        // --- Rule [attr-no-init] (pag. 22) - End
            
            
        // --- Rule [case] (pag. 21) - Start
        // 'case' expression 'of' (Identifier ':' Identifier '=>' expression ';')+ 'esac' (';')?
        case CASE:
        {
            // Min value of size_e (number of expression) are 2. One after 'case' and one after 'of' (there is a '+')
            if (size_e < 2)
                this.printError ("CASE", "Expressions number must be >= 2 instead of " + size_e, expression, true);
            // Min value of size_i (number of identifiers) are 2. After 'of' there is a '+' for each "case"
            if (size_i < 2)
                this.printError ("CASE", "Identifiers number must be >= 2 instead of " + size_i, expression, true);
            if (size_i % 2 != 0)
                this.printError ("CASE", "size_i % 2 != 0", expression, true);

            ExpressionContext exp_0 = expressions.get (0);
            
            Type t_0 = this.evaluate (exp_0, GenericUtils.identifyTypeExpression (exp_0));
            
            TestUtils._assert_not_null ("CASE, t_0 is null", t_0, true);
            
            // Case structure:
            // Id : Id => Exp;
            // ...
            // Id : Id => Exp;
            // For each line, first Id is an Identifier while second Id is a Type
            ArrayList<String> id    = new ArrayList<String> ();
            ArrayList<String> types = new ArrayList<String> ();
            for (int k = 0; k < size_i; k++) {
                if (k % 2 == 0)
                    id.add (identifiers.get (k).toString ());
                else
                    types.add (identifiers.get (k).toString ());
            }
            
            // Exp_0 does not count
            if (size_e - 1 != types.size ())
                this.printError ("CASE", "size_e - 1 != types.size (), " + (size_e - 1) + " != " + types.size (), expression, true);
            
            ArrayList<Type> typesResult = new ArrayList<Type> ();
            for (int k = 1; k < size_e; k++) {
                ExpressionContext e_k = expressions.get (k);
                String i_k            = id.get (k - 1);
                String t_k            = types.get (k - 1);
                
                // We must evaluate O[T_k'/x_k], M, C  |-  e_k : T_k
                // for 1 <= k < size_e
                Symbol sym = this.currentScope.resolve (i_k);   // Get identifier 'x_k'
                
                TestUtils._assert_not_null ("CASE, symbol not found (identifier=" + i_k + ")", sym, true);
                
                if (this.currentScope.remove (sym.getName ()) == null)
                    this.printError ("CASE", "Impossibile to remove symbol. " + sym + ". Scope: " + 
                                     this.currentScope.getScopeName (), expression, true);
                
                this.currentScope.define (new IdentifierSymbol (i_k, new Type (t_k)));
                
                Type res = this.evaluate (e_k, GenericUtils.identifyTypeExpression (e_k));
                
                TestUtils._assert_not_null ("CASE, res is null (identifier=" + i_k + ")", res, true);
                
                typesResult.add (res);
            }
            
            // Get classes for each type to make the "join" (definition of join at page 11)
            ArrayList<CoolClass> classes = new ArrayList<CoolClass> ();
            for (int k = 0, size_t = typesResult.size (); k < size_t; k++) {
                CoolClass class_ = GenericUtils.getInstanceClassByClassName (coolClasses, typesResult.get (k).getType ());
                
                TestUtils._assert_not_null ("CASE, class_ is null (identifier=" + id.get (k) + ")", class_, true);
                
                classes.add (class_);
            }
            
            Type joinType = GenericUtils.join (classes);
            
            TestUtils._assert_not_null ("CASE, joinType is null", joinType, true);
            
            return joinType;
        }
        // --- Rule [case] (pag. 21) - End
        
            
        // --- Rule [compare] (pag. 21) - Start
        // expression '<' expression (';')?
        // |
        // expression '<=' expression (';')?
        case COMPARE:
        {
            if (size_e != 2) 
                this.printError ("COMPARE", "Expressions number must be 2 instead of " + size_e, expression, true);
            
            ExpressionContext exp_1 = expressions.get (0),
                              exp_2 = expressions.get (1);
            
            Type type_1 = this.evaluate (exp_1, GenericUtils.identifyTypeExpression (exp_1)),
                 type_2 = this.evaluate (exp_2, GenericUtils.identifyTypeExpression (exp_2));
            
            TestUtils._assert_not_null ("COMPARE, type_1 is null", type_1, true);
            TestUtils._assert_not_null ("COMPARE, type_2 is null", type_2, true);
            
            if ((type_1 instanceof Int_ || type_1.getType ().equals ("Int")) && (type_2 instanceof Int_ || type_2.getType ().equals ("Int")))
                return new Type ("Bool");
            
            // One or both of two types is/are not Bool
            ErrorMessage.printError (
                    ErrorMessage.Error.ERROR, 
                    lineExp, 
                    "Impossible to make comparison between types " + type_1.getType () + " and " + type_2.getType ()
            );
        }
        // --- Rule [compare] (pag. 21) - End
        
        
        // --- Rule [dispatch] (pag. 19) - Start
        case DISPATCH:
        {
            ArrayList<Type> typesExpressions = new ArrayList<Type> ();
            
            // Number of expressions are >= 1, but we check anyway
            for (int k = 0; k < size_e; k++) {
                // Exp-k
                ExpressionContext exp = expressions.get (k);
                // Type of the expression k
                Type t_k = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
                
                TestUtils._assert_not_null ("DISPATCH, t_k is null", t_k, true);
            
                // Add type number k in the array
                typesExpressions.add (t_k);
            }
            
            // size_e may be equals to 0: 
            //      - ID.method ()
            
            // size_e may be equals to 1. Two reasons: 
            //      - expr.method ()
            //      - ID.(expr)
            
            // Get expressions (may be 0) inside formal parameters list
            ArrayList<ExpressionContext> expressionsFormalParameters = new ArrayList<ExpressionContext> ();
            // If add = true, I can add expression-k to array
            boolean add = false;
            // Name of the method
            String methodName = null;
            // I need this because an expression could be: (new D).method4(avar.value(), a_var.value());
            // In this case the method is after the first point
            boolean firstPoint = false;
            
            for (int k = 0, size_child = expression.getChildCount (); k < size_child; k++) {
                ParseTree childNode = expression.getChild (k);
                
                if (childNode.toString ().equals ("(")) {
                    add = true;
                    
                    if (k - 1 >= 0 && StringUtils.isIdentifier (expression.getChild (k - 1).toString ()))
                        methodName = expression.getChild (k - 1).toString ();

                    continue;
                }
                
                if (childNode instanceof ExpressionContext && add)
                    expressionsFormalParameters.add ((ExpressionContext) childNode);
                
                // One case: name of the method is the first child
                if (k == 0 && StringUtils.isIdentifier (childNode.toString ()))
                    methodName = childNode.toString ();
                
                if (childNode.toString ().equals (".") && !firstPoint) {
                    firstPoint = true;
                    if (k + 1 <= size_child - 1 && StringUtils.isIdentifier (expression.getChild (k + 1).toString ()))
                        methodName = expression.getChild (k + 1).toString ();
                }
            }
            
            // T_0
            Type T_0 = null;
            
            // (fist condition) T_0 must be calculated by the identifier if the number of expressions is 0 
            // (second condition) Number of the expression found in the production and in the formal parameters are the same.
            // So there is not expr_0 (see rule at page 19)
            if (size_e == 0 || size_e == expressionsFormalParameters.size ()) {
                if (size_i == 0)
                    this.printError ("DISPATCH", "No identifiers when size_e = 0", expression, true);

                // identifiers.get (0) = name of the method
                T_0 = GenericUtils.getTypeFromMethodName (this.coolClasses, methodName);
                
                TestUtils._assert_not_null ("DISPATCH, T_0 is null (1)", T_0, true);
            }
            // We use expr_0 to evaluate for calculating T_0'
            else {
                if (size_e == 0)
                    this.printError ("DISPATCH", "size_e is 0", expression, true);

                // At this point I have calculated the type from t_0 to t_n
                /*
                T_0 = this.evaluate (expressions.get (0), this.identifyTypeExpression (expressions.get (0)));
                
                if (T_0 == null)
                    this.printError ("DISPATCH", "T_0 is null (2)", expression, true);
                    */
                T_0 = typesExpressions.get (0);
            }
            
            TestUtils._assert_not_null ("DISPATCH, T_0 is null (2)", T_0, true);

            // Now, we must calculate T_0'. This is the standard case
            //          - C     if T_0 = SELF_TYPE_C
            // T_0' =
            //          - T_0   otherwise
            Type T_0_prime = (size_e == 0 || size_e == expressionsFormalParameters.size ())
                                ? T_0
                                : (T_0.equals (new Type (this.currentClass))) ? new Type (this.currentClass) 
                                                                              : T_0;
                                
            TestUtils._assert_not_null ("DISPATCH, T_0_prime is null", T_0_prime, true);
            
            // Calculate all types in the signature of the method
            // typesSignature [0..size-1] => signature ----- typesSignature[size] = return value
            // T_1', ..., T_N', T_(N+1)'
            ArrayList<Type> typesSignature = MethodEnvironment.mapping (T_0_prime.getType (), methodName, this.coolClasses, expression);
            
            // In the first line of this 'case', I've already calculated all the expressions. Results are inside typesExpression array
            // Now I must check the conformance between typesExpression (not all array) and the types of the
            // Check T_k <= T_k'
            // typesSignature.size () - 1 because last value of the array is the return value of the method
            for (int k = 0, size = typesSignature.size () - 1; k < size; k++) {
                // (typesSignature.size () - 1) == typesExpressions.size () means that the expressions are only declared
                // inside the signature of the method: ID (exp, ..., exp)
                int indexExpression = ((typesSignature.size () - 1) == typesExpressions.size ()) ? k : (k + 1);
                
                TestUtils._assert_false ("DISPATCH, indexExpression >= typesExpressions", indexExpression >= typesExpressions.size (), true);
                
                CoolClass class_k       = GenericUtils.getInstanceClassByClassName (coolClasses, typesExpressions.get (indexExpression).getType ()),
                          class_k_prime = GenericUtils.getInstanceClassByClassName (coolClasses, typesSignature.get (k).getType ());
                
                TestUtils._assert_not_null ("DISPATCH, class_k is null", class_k, true);
                TestUtils._assert_not_null ("DISPATCH, class_k_prime is null", class_k_prime, true);
                
                // No conformance between classes
                if (!class_k.conformance (class_k_prime))
                    ErrorMessage.printError (
                            ErrorMessage.Error.CONFORMANCE, 
                            expression.getStart ().getLine (), 
                            class_k.getName (), 
                            class_k_prime.getName (),
                            "DISPATCH"
                    );
            }
            
            // typesSignature.size () = n, "0..n-1" formal parameters, "n" return value. So, typesSignature.size () > 0
            Type T_N_plus_1 = null;
            
            // We check size anyway
            int sizeTS = typesSignature.size ();
            
            TestUtils._assert_false ("DISPATCH, sizeTS is 0", sizeTS == 0, true);
            
            Type typeReturnMethod = typesSignature.get (sizeTS - 1); // T_(N+1)'
            
            T_N_plus_1 = typeReturnMethod.getType ().equals ("SELF_TYPE") ? T_0 : typeReturnMethod;
            
            TestUtils._assert_not_null ("DISPATCH, T_N_plus_1 is null", T_N_plus_1, true);

            return T_N_plus_1;
        }
        // --- Rule [dispatch] (pag. 19) - End
        
        
        // --- Rule [equal] (pag. 22) - Start
        // expression '=' expression (';')?
        case EQUAL:
        {
            if (size_e != 2)
                this.printError ("EQUAL", "Number of expressions are " + size_e + " instead of 2", expression, true);
            
            ExpressionContext exp_1 = expressions.get (0),
                              exp_2 = expressions.get (1);
            
            Type type_1 = this.evaluate (exp_1, GenericUtils.identifyTypeExpression (exp_1)),
                 type_2 = this.evaluate (exp_2, GenericUtils.identifyTypeExpression (exp_2));
            
            TestUtils._assert_not_null ("EQUAL, type_1 is null", type_1, true);
            TestUtils._assert_not_null ("EQUAL, type_2 is null", type_2, true);
            
            if ((type_1.equals ("Int") || type_1 instanceof Int_) ||
                (type_1.equals ("Bool") || type_1 instanceof Bool_) ||
                (type_1.equals ("String") || type_1 instanceof String_))
                if (!type_1.getType ().equals (type_2.getType ()))
                    ErrorMessage.printError (
                            ErrorMessage.Error.ERROR, 
                            lineExp, 
                            "You cannot compare types " + type_1.getType () + " and " + type_2.getType ()
                    );
 
            return new Type ("Bool");
        }
        // --- Rule [equal] (pag. 22) - End
        
        
        // --- Rule [false] (pag. 19) - Start
        case FALSE:
            if (size_e != 0)
                this.printError ("FALSE", "Number of expressions are " + size_e + " instead of 0", expression, true);
            
            return new Bool_ (false);
        // --- Rule [false] (pag. 19) - End
            
        
        // --- Rule [identifier] (pag. 19) - Start
        case IDENTIFIER:
        {
            if (size_i != 1)
                this.printError ("IDENTIFIER", "Number of identifiers are " + size_i + " instead of 1", expression, true);
            
            Type type_0 = ObjectEnvironment.mapping (this.coolClasses, 
                                                     this.currentClass, 
                                                     this.currentScope, 
                                                     expression, 
                                                     identifiers.get (0).toString ());
            
            TestUtils._assert_not_null ("IDENTIFIER, type_0 is null", type_0, true);
            
            return type_0;
        }
        // --- Rule [identifier] (pag. 19) - End
        
        
        // --- Rule [if] (pag. 20) - Start
        // 'if' expression 'then' expression 'else' expression 'fi' (';')?
        case IF:
        {
            if (size_e != 3)
                this.printError ("IF", "Number of expressions are " + size_e + " instead of 3", expression, true);
            
            ExpressionContext exp_1 = expressions.get (0),
                              exp_2 = expressions.get (1),
                              exp_3 = expressions.get (2);
            
            Type type_1 = this.evaluate (exp_1, GenericUtils.identifyTypeExpression (exp_1)),
                 type_2 = this.evaluate (exp_2, GenericUtils.identifyTypeExpression (exp_2)),
                 type_3 = this.evaluate (exp_3, GenericUtils.identifyTypeExpression (exp_3));
            
            TestUtils._assert_not_null ("IF, type_1 is null", type_1, true);
            TestUtils._assert_not_null ("IF, type_2 is null", type_2, true);
            TestUtils._assert_not_null ("IF, type_3 is null", type_3, true);
            
            CoolClass class_2 = GenericUtils.getInstanceClassByClassName (coolClasses, type_2.getType ()),
                      class_3 = GenericUtils.getInstanceClassByClassName (coolClasses, type_3.getType ());
            
            TestUtils._assert_not_null ("IF, class_2 is null", class_2, true);
            TestUtils._assert_not_null ("IF, class_3 is null", class_3, true);
            
            ArrayList<CoolClass> c = new ArrayList<CoolClass> ();
            c.add (class_2);
            c.add (class_3);
            
            Type joinType = GenericUtils.join (c);
            
            return joinType;
        }
        // --- Rule [if] (pag. 20) - End
            
        
        // --- Rule [int] (pag. 19) - Start
        case INT:
            String int_ = (expression != null) ? expression.Integer ().toString () : null;
            
            TestUtils._assert_not_null ("INT, int_ is null", int_, true);
            TestUtils._assert_true ("INT, int_ is not an integer (" + int_ + ")", StringUtils.isNumber (int_), true);
            
            return new Int_ (Integer.parseInt (int_));
        // --- Rule [int] (pag. 19) - End
             
            
        // --- Rule [isvoid] (pag. 21) - Start
        // 'isvoid' expression (';')?
        case ISVOID:
        {
            if (size_e != 1)
                this.printError ("ISVOID", "Number of expressions are " + size_e + " instead of 1", expression, true);
            
            ExpressionContext exp = expressions.get (0);
            
            // Type of the expression: isvoid Exp
            Type type = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
            
            TestUtils._assert_not_null ("ISVOID, type is null", type, true);
            
            return new Type ("Bool");
        }
        // --- Rule [isvoid] (pag. 21) - End
        
        
        // --- Rule [let-init] (pag. 20) - Start
        // 'let' Identifier ':' Identifier ('<-' expression)? 'in' expression (';')? 
        // &
        // --- Rule [let-no-init] (pag. 21) - Start
        // 'let' Identifier ':' Identifier 'in' expression (';')? 
        case LET_INIT:
        case LET_NO_INIT:
        {
            if (size_i < 2)
                this.printError ("LET", "Number of identifiers are " + size_i + " instead of >= 2", expression, true);
            if (size_i % 2 != 0)
                this.printError ("LET", "size_i % 2 != 0", expression, true);
            
            Type retType = null;
            
            for (int k = 0, n_child = expression.getChildCount (); k < n_child; k++) {
                ParseTree child_k = expression.getChild (k);
                
                String id = "UNKNOWN_ID", type = "UNKNOWN_TYPE";
                
                // ID : TYPE (<- Exp)
                if (child_k.toString ().equals (":")) {
                    if (k - 1 >= 0)
                        id = expression.getChild (k - 1).toString ();
                    if (k + 1 < n_child - 1)
                        type = expression.getChild (k + 1).toString ();
                    
                    TestUtils._assert_not_null ("LET, id is null (type environment)", id, true);
                    TestUtils._assert_not_null ("LET, type is null (type environment)", id, true);
                    
                    // Same for both LET
                    Type t_0       = new Type (type);
                    Type t_0_prime = (t_0.getType ().equals ("SELF_TYPE")) ? new Type (this.currentClass) : t_0;
                    
                    // Let identifier has expression that initialize it
                    if (k + 2 < n_child - 1 && expression.getChild (k + 2).toString ().equals ("<-")) {
                        if (k + 3 < n_child - 1 && expression.getChild (k + 3) instanceof ExpressionContext) {
                            
                        }
                        else
                            this.printError ("LET", "expression.getChild (k + 3) is not instance of ExpressionContext", expression, true);
                        
                        // There is an expression after '<-'
                        ExpressionContext exp = expressions.get (0);
                        
                        Type t_1 = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
                        
                        TestUtils._assert_not_null ("LET, t_1 is null (type environment)", t_1, true);
                        
                        CoolClass class_1       = GenericUtils.getInstanceClassByClassName (coolClasses, t_1.getType ()),
                                  class_0_prime = GenericUtils.getInstanceClassByClassName (coolClasses, t_0_prime.getType ());
                        
                        TestUtils._assert_not_null ("LET, class_1 is null (type environment)", class_1, true);
                        TestUtils._assert_not_null ("LET, class_0_prime is null (type environment)", class_0_prime, true);
                        
                        if (!class_1.conformance (class_0_prime))
                            ErrorMessage.printError (
                                    ErrorMessage.Error.CONFORMANCE, 
                                    expression.getStart ().getLine (), 
                                    class_1.getName (), 
                                    class_0_prime.getName (),
                                    "LET"
                            );
                        
                        // We must evaluate O[T_0'/x], M, C  |-  e_2 : T_2
                        Symbol sym = this.currentScope.resolve (identifiers.get (0).toString ());   // Get identifier 'x'
                        
                        TestUtils._assert_not_null ("LET, symbol not found for the expression O[T_0'/x]", sym, true);
                        
                        if (this.currentScope.remove (sym.getName ()) == null)
                            this.printError ("LET_INIT", "Impossibile to remove symbol. " + sym + ". Scope: " + 
                                             this.currentScope.getScopeName (), expression, true);
                        
                        this.currentScope.define (new IdentifierSymbol (identifiers.get (0).toString (), t_0_prime));
                        
                        ExpressionContext inExp = expressions.get (size_e - 1);
                        
                        Type t_2 = this.evaluate (inExp, GenericUtils.identifyTypeExpression (inExp));
                        
                        TestUtils._assert_not_null ("LET, t_2 is null", t_2, true);

                        return t_2;
                    }
                    // Let identifier has not expression that initialize it
                    else {
                        // We must evaluate O[T_0'/x], M, C  |-  e_1 : T_1
                        Symbol sym = this.currentScope.resolve (id);   // Get identifier 'id'
                        
                        TestUtils._assert_not_null ("LET, symbol not found for the expression O[T_0'/x]", sym, true);
                        
                        if (this.currentScope.remove (sym.getName ()) == null)
                            this.printError ("LET_NO_INIT", "Impossibile to remove symbol. " + sym + ". Scope: " + 
                                             this.currentScope.getScopeName (), expression, true);
                        
                        this.currentScope.define (new IdentifierSymbol (identifiers.get (0).toString (), t_0_prime));

                        ExpressionContext exp_1 = expressions.get (0);
                        
                        Type t_1 = this.evaluate (exp_1, GenericUtils.identifyTypeExpression (exp_1));
                        
                        TestUtils._assert_not_null ("LET, t_1 is null", t_1, true);
                        
                        return t_1;
                    }
                }
            }
            
            TestUtils._assert_not_null ("LET, retType is null", retType, true);
            
            return retType;
        }
        // --- Rule [let-init] (pag. 20) - End
        // &
        // --- Rule [let-no-init] (pag. 21) - End  
        
        
        // --- Rule [loop] (pag. 21) - Start
        case LOOP:
        {
            if (size_e != 2)
                this.printError ("LOOP", "Number of expressions are " + size_e + " instead of 2", expression, true);

            ExpressionContext exp_1 = expressions.get (0),
                              exp_2 = expressions.get (1);
            
                 // Exp_1 --> Bool
            Type type_1 = this.evaluate (exp_1, GenericUtils.identifyTypeExpression (exp_1)),
                 // Exp_2 --> T_2
                 type_2 = this.evaluate (exp_2, GenericUtils.identifyTypeExpression (exp_2));
            
            TestUtils._assert_not_null ("LOOP, type_1 is null", type_1, true);
            TestUtils._assert_not_null ("LOOP, type_2 is null", type_2, true);

            if (type_1 instanceof Bool_ || type_1.getType ().equals ("Bool"))
                return new Type ("Object");
            
            ErrorMessage.printError (
                    ErrorMessage.Error.ERROR, 
                    lineExp, 
                    "Loop condition must have Bool type instead of " + type_1.getType ()
            );

            break;
        }
        // --- Rule [loop] (pag. 21) - End
        
        
        // --- Rule [method] (pag. 22) - Start
        // Identifier '(' (formal (',' formal)*)? ')' ':' Identifier '{' expression '}' (';')?;
        case METHOD:
        {
            TestUtils._assert_not_null ("METHOD, methodContext is null", this.methodContext, true);
            
            // Calculate identifiers
            List<TerminalNode> identifiersMethod = this.methodContext.Identifier ();
            if (identifiersMethod == null || identifiersMethod.size () == 0)
                this.printError ("METHOD", "Number of identifiers are 0", expression, true);
            
            String methodName = identifiersMethod.get (0).toString ();
            // Calculate all types in the signature of the method
            // typesSignature [0..size-1] => signature ----- typesSignature[size] = return value
            // T_1', ..., T_N', T_(N+1)'
            ArrayList<Type> typesSignature = MethodEnvironment.mapping (this.currentClass, methodName, this.coolClasses, expression);
            int sizeTS = typesSignature.size ();
            
            // Get return value of the method (is the last position of the array typesSignature)
            if (sizeTS == 0)
                this.printError ("METHOD", "typesSignatureSD.size () is 0", expression, true);
            
            Type t_0 = typesSignature.get (sizeTS - 1);
            
            TestUtils._assert_not_null ("METHOD, t_0 is null", t_0, true);
            
            if (t_0.getType ().equals ("SELF_TYPE"))
                t_0 = new Type (this.currentClass);
            
            // Calculate type T_0'
            Type t_0_prime = this.evaluate (expression, GenericUtils.identifyTypeExpression (expression));
            
            TestUtils._assert_not_null ("METHOD, t_0_prime is null", t_0_prime, true);
            
            CoolClass class_0       = GenericUtils.getInstanceClassByClassName (coolClasses, t_0.getType ()),
                      class_0_prime = GenericUtils.getInstanceClassByClassName (coolClasses, t_0_prime.getType ());
            
            TestUtils._assert_not_null ("METHOD, class_0 is null", class_0, true);
            TestUtils._assert_not_null ("METHOD, class_0_prime is null", class_0_prime, true);
            
            if (!class_0_prime.conformance (class_0))
                ErrorMessage.printError (
                        ErrorMessage.Error.CONFORMANCE, 
                        expression.getStart ().getLine (), 
                        class_0_prime.getName (), 
                        class_0.getName (),
                        "METHOD"
                );
            
            return t_0;
        }
        // --- Rule [method] (pag. 22) - End
            
        
        // --- Rule [neg] (pag. 21) - Start
        case NEG:
        {
            if (size_e != 1)
                this.printError ("NEG", "Number of expressions are " + size_e + " instead of 1", expression, true);
            
            ExpressionContext exp = expressions.get (0);
            
            Type type = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
            
            TestUtils._assert_not_null ("NEG, type is null", type, true);
            
            if (type instanceof Int_ || type.getType ().equals ("Int"))
                return new Type ("Int");
            
            ErrorMessage.printError (
                    ErrorMessage.Error.ERROR, 
                    lineExp, 
                    "Neg operator must be used with Int type instead of " + type.getType ()
            );
            
            break;
        }
        // --- Rule [neg] (pag. 21) - End
            
        
        // --- Rule [new] (pag. 19) - Start
        case NEW:
        {
            if (size_i != 1)
                this.printError ("NEW", "Number of identifiers are " + size_i + " instead of 1", expression, true);
            
            // Two cases, one for "new SELF_TYPE" and one for any other form
            String valueI = identifiers.get (0).toString ();
            
            TestUtils._assert_not_null ("NEW, valueI is null", valueI, true);
            
            return (valueI.equals ("SELF_TYPE")) ? new Type (this.currentClass) : new Type (valueI);
        }
        // --- Rule [new] (pag. 19) - End
            
        
        // --- Rule [not] (pag. 21) - Start
        case NOT:
        {
            if (size_e != 1)
                this.printError ("NOT", "Number of expressions are " + size_e + " instead of 1", expression, true);
            
            ExpressionContext exp = expressions.get (0);
            
            Type type = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
            
            TestUtils._assert_not_null ("NOT, type is null", type, true);
            
            if (type instanceof Bool_ || type.getType ().equals ("Bool"))
                return new Type ("Bool");
            
            ErrorMessage.printError (
                    ErrorMessage.Error.ERROR, 
                    lineExp, 
                    "Not operator must be used with Bool type instead of " + type.getType ()
            );
            
            break;
        }
        // --- Rule [not] (pag. 21) - End
            
        
        // --- Rule [sequence] (pag. 20) - Start
        case SEQUENCE:
        {
            if (size_e == 0)
                this.printError ("SEQUENCE", "size_e is 0", expression, true);
            
            // Number of expressions are >= 1, but we check anyway
            for (int k = 0; k < size_e; k++) {
                // Exp-k
                ExpressionContext exp = expressions.get (k);
                // Type of the expression k
                Type type = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
                
                TestUtils._assert_not_null ("SEQUENCE, type is null", type, true);
                
                // Last expression
                if (k == (size_e - 1))
                    return type;
            }
        }
        // --- Rule [sequence] (pag. 20) - End

        
        // --- Rule [static-dispatch] (pag. 20) - Start
        case STATIC_DISPATCH:
        {
            if (size_e == 0)
                this.printError ("STATIC_DISPATCH", "size_e is 0", expression, true);
            
            ArrayList<Type> typesExpressions = new ArrayList<Type> ();
            
            for (int k = 0; k < size_e; k++) {
                ExpressionContext exp = expressions.get (k);
                
                Type t = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
                
                TestUtils._assert_not_null ("STATIC_DISPATCH, type is null", t, true);
                
                typesExpressions.add (t);
            }
            
            if (typesExpressions.size () == 0)
                this.printError ("STATIC_DISPATCH", "typesSD.size () is 0", expression, true);
            
            // Check if there is '@' and calculate methodName
            Type typeAT = null;
            String methodName = null;
            // I need this because an expression could be: (new D).method4(avar.value(), a_var.value());
            // In this case the method is after the first point
            boolean firstPoint = false;
            
            for (int k = 0, size_child = expression.getChildCount (); k < size_child; k++) {
                ParseTree childNode = expression.getChild (k);
                
                if (childNode.toString ().equals ("@")) {
                    if (k + 1 <= size_child - 1 && StringUtils.isIdentifier (expression.getChild (k + 1).toString ())) {
                        typeAT = new Type (expression.getChild (k + 1).toString ());
                    }
                }
                else if (childNode.toString ().equals (".") && !firstPoint) {
                    firstPoint = true;
                    if (k + 1 <= size_child - 1 && StringUtils.isIdentifier (expression.getChild (k + 1).toString ()))
                        methodName = expression.getChild (k + 1).toString ();
                }
            }
            
            TestUtils._assert_not_null ("STATIC_DISPATCH, typeAT is null", typeAT, true);
            TestUtils._assert_not_null ("STATIC_DISPATCH, methodName is null", methodName, true);
            
            Type t_0 = typesExpressions.get (0); // We already check length > 0
            
            CoolClass class_t_0    = GenericUtils.getInstanceClassByClassName (coolClasses, t_0.getType ()),
                      class_typeAT = GenericUtils.getInstanceClassByClassName (coolClasses, typeAT.getType ());
            
            TestUtils._assert_not_null ("STATIC_DISPATCH, class_t_0 is null", class_t_0, true);
            TestUtils._assert_not_null ("STATIC_DISPATCH, class_typeAT is null", class_typeAT, true);          
          
            // No conformance between classes
            if (!class_t_0.conformance (class_typeAT))
                ErrorMessage.printError (
                        ErrorMessage.Error.CONFORMANCE, 
                        expression.getStart ().getLine (), 
                        class_t_0.getName (), 
                        class_typeAT.getName (),
                        "STATIC_DISPATCH (1)"
                );
            
            
            // Calculate all types in the signature of the method
            // typesSignature [0..size-1] => signature ----- typesSignature[size] = return value
            // T_1', ..., T_N', T_(N+1)'
            
            // Use of variable expression is not correct. 
            // We don't know, at this point, which is the class "class_typeAT" and method "methodName" and, so, the expression
            // that is used in that context.
            // Anyway, we can use "expression" since "mapping" method of "MethodEnvironment" use it to get the content of the expression.
            // The content is used only for errors generated by the program instead of errors of the compiler.
            ArrayList<Type> typesSignature = MethodEnvironment.mapping (class_typeAT.getName (), methodName, this.coolClasses, expression);
            
            // Exp_0 exists for sure. So the signature is from Exp_1 to Exp_n.
            // typesSignatureSD contains also the return value
            if (typesSignature.size () != typesExpressions.size ())
                this.printError ("STATIC_DISPATCH", "typesSignatureSD.size () != size_e" + (typesSignature.size () - 1) + 
                                 " != " + (typesExpressions.size () - 1), expression, true);
            
            // Check T_k <= T_k'
            //      - typesExpressions: first expression is not part of the signature;
            //      - typesSignature:   last expression is not part of the signature (return value);
            for (int k = 1; k < typesExpressions.size (); k++) {
                CoolClass class_k       = GenericUtils.getInstanceClassByClassName (coolClasses, typesExpressions.get (k).getType ()),
                          class_k_prime = GenericUtils.getInstanceClassByClassName (coolClasses, typesSignature.get (k - 1).getType ());
                
                TestUtils._assert_not_null ("STATIC_DISPATCH, class_k is null", class_k, true);
                TestUtils._assert_not_null ("STATIC_DISPATCH, class_k_prime is null", class_k_prime, true);
                
                // No conformance between classes
                if (!class_k.conformance (class_k_prime))
                    ErrorMessage.printError (
                            ErrorMessage.Error.CONFORMANCE, 
                            expression.getStart ().getLine (), 
                            class_k.getName (), 
                            class_k_prime.getName (),
                            "STATIC_DISPATCH (2)"
                    );
            }
            
            // typesSignature.size () = n, "0..n-1" formal parameters, "n" return value. So, typesSignature.size () > 0
            Type T_N_plus_1_SD = null;
            
            // We check size anyway
            int sizeTS = typesSignature.size ();
            if (sizeTS == 0)
                this.printError ("STATIC_DISPATCH", "sizeTS is 0", expression, true);
            
            Type typeReturnMethod = typesSignature.get (sizeTS - 1); // T_(N+1)'
            Type T_0_ = typesExpressions.get (0);
            
            T_N_plus_1_SD = typeReturnMethod.getType ().equals ("SELF_TYPE") ? T_0_ : typeReturnMethod;
            
            TestUtils._assert_not_null ("STATIC_DISPATCH, T_N_plus_1_SD is null", T_N_plus_1_SD, true);
            
            return T_N_plus_1_SD;
        }
        // --- Rule [static-dispatch] (pag. 20) - Start
            
        
        // --- Rule [string] (pag. 19) - Start
        case STRING:
        {
            String string_ = (expression != null) ? expression.String ().toString () : null;
            
            TestUtils._assert_not_null ("STRING, string_ is null", string_, true);

            return new String_ (string_);
        }
        // --- Rule [string] (pag. 19) - End
       
        
        // --- Rule [par] (pag. 20) - Start
        case PAR:
        {
            if (size_e != 1)
                this.printError ("PAR", "Number of expressions are " + size_e + " instead of 1", expression, true);
            
            ExpressionContext exp = expressions.get (0);
            
            Type type = this.evaluate (exp, GenericUtils.identifyTypeExpression (exp));
            
            TestUtils._assert_not_null ("PAR, type is null", type, true);
            
            return type;
        }
        // --- Rule [par] (pag. 20) - End
        
        
        // --- Rule [true] (pag. 19) - Start
        case TRUE:
            if (size_e != 0)
                this.printError ("TRUE", "Number of expressions are " + size_e + " instead of 0", expression, true);
            
            return new Bool_ (true);
        // --- Rule [true] (pag. 19) - End  

            
        default:
            this.printError ("UNKNOWN", "Unknown expression: " + expression.toStringTree (), expression, true);
        }

        return null;
    }
    
    
    private void printError (String typeExpression, String message, ExpressionContext expContext, boolean terminate)
    {
        if (expContext != null)
            IOUtils.printError ("Expression type: " + typeExpression + ". Message: " + message + ". Line: " + 
                                expContext.getStart ().getLine () + "\nExpression: " + expContext.toStringTree (), terminate);
        else
            IOUtils.printError ("Expression type: " + typeExpression + ". Message: " + message, terminate);
    }
    
    
    public String getCurrentClass ()
    {
        return this.currentClass;
    }


    public void setCurrentClass (String currentClass)
    {
        this.currentClass = currentClass;
    }
    
    
    public void setCoolClasses (ArrayList<CoolClass> coolClasses)
    {
        this.coolClasses = coolClasses;
    }
    
    
    public void setCurrentScope (Scope currentScope)
    {
        this.currentScope = currentScope;
    }
    
    
    public void setEntireScope (ParseTreeProperty<Scope> entireScope)
    {
        this.entireScope = entireScope;
    }
    
    
    public void setAttributeContext (AttributeDeclarationContext attributeContext)
    {
        this.attributeContext = attributeContext;
    }
    
    
    public void setMethodContext (MethodDeclarationContext methodContext)
    {
        this.methodContext = methodContext;
    }
}
