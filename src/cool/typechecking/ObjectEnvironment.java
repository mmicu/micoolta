package cool.typechecking;

import java.util.ArrayList;

import antlr.coolParser.ExpressionContext;

import utils.TestUtils;

import cool.data.structure.object.CoolClass;
import cool.data.structure.symbols.IdentifierSymbol;
import cool.data.structure.symbols.MethodSymbol;
import cool.data.structure.symbols.Symbol;
import cool.data.structure.types.Type;

import cool.error.ErrorMessage;

import cool.scope.Scope;


/**
 * In the type checking, object environment is a function:
 * 
 *          O (v) = T
 * where:
 *      - v => identifier;
 *      - T => type of identifier "v".
 */
public class ObjectEnvironment
{
    public static Type mapping (ArrayList<CoolClass> coolClasses, String class_, Scope scope, ExpressionContext e, String identifier)
    {
        int line = e.getStart ().getLine ();
        
        // Asserts ~ Start
        TestUtils._assert_not_null ("e is null", e);
        TestUtils._assert_not_null ("class_ is null. Line: " + line, class_);
        TestUtils._assert_not_null ("scope is null. Line: " + line, scope);
        TestUtils._assert_not_null ("identifier is null. Line: " + line, identifier);
        
        TestUtils._assert_false ("coolClasses is not set. Line: " + line, coolClasses == null || coolClasses.size () == 0);
        // Asserts ~ End 
        
        if (identifier.equals ("self"))
            return new Type (class_);
        
        if (scope != null) {
            Symbol sym = scope.resolve (identifier);
            
            TestUtils._assert_not_null ("sym is null on identifier " + identifier + ". Line: " + line, sym);
            
            if (sym instanceof IdentifierSymbol)
                return sym.getType ();
            else { 
                if (sym instanceof MethodSymbol)
                    ErrorMessage.printError (ErrorMessage.Error.ERROR, line, "You cannot use method " + identifier + " as an identifier");
                else
                    ErrorMessage.printError (ErrorMessage.Error.ERROR, line, "Identifier " + identifier + " not found");
                
                System.exit (-1);
            }
        }
        else
            ErrorMessage.printError (ErrorMessage.Error.ERROR, line, "Identifier " + identifier + " not found");
        
        return null;
    }
}
