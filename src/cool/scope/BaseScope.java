// Thanks to https://pragprog.com/book/tpantlr2/the-definitive-antlr-4-reference
package cool.scope;

import java.util.LinkedHashMap;
import java.util.Map;

import cool.data.structure.symbols.Symbol;

public abstract class BaseScope implements Scope 
{
    private Scope enclosingScope; // Null if global (outermost) scope
    
    private Map<String, Symbol> symbols;

    
    public BaseScope (Scope enclosingScope) 
    { 
        this.enclosingScope = enclosingScope;
        this.symbols        = new LinkedHashMap<String, Symbol> ();
    }

    
    public Symbol resolve (String name) 
    {
        Symbol s = symbols.get (name);
        
        if (s != null) 
            return s;
        
        // If not here, check any enclosing scope
        if (enclosingScope != null) 
            return enclosingScope.resolve (name);
        
        return null; // Not found
    }

    
    public void define (Symbol sym) 
    {
        symbols.put (sym.getName (), sym);
        sym.setScope (this); // Track the scope in each symbol
    }
    
    
    public Object remove (String name)
    {
        return symbols.remove (name);
    }

    
    public Scope getEnclosingScope () 
    { 
        return this.enclosingScope;
    }


    @Override
    public String toString ()
    {
        return this.enclosingScope + ": " + this.symbols.keySet ().toString ();
    }
}
