// Thanks to https://pragprog.com/book/tpantlr2/the-definitive-antlr-4-reference
package cool.scope;

public class LocalScope extends BaseScope
{
    public LocalScope (Scope enclosingScope)
    {
        super (enclosingScope);
    }

    
    @Override
    public String getScopeName ()
    {
        return "LocalScope";
    }


    @Override
    public Object remove (String name)
    {
        return super.remove (name);
    }
}
