package cool.scope;

public class ClassScope extends BaseScope
{
    private String className;
    
    
    public ClassScope (Scope enclosingScope)
    {
        this (enclosingScope, null);
    }
    
    
    public ClassScope (Scope enclosingScope, String className)
    {
        super (enclosingScope);
        
        this.className = className;
    }
    
    
    public String getClassName ()
    {
        return this.className;
    }

    
    @Override
    public String getScopeName ()
    {
        return "ClassScope";
    }


    @Override
    public Object remove (String name)
    {
        return super.remove (name);
    }
}
