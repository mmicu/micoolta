// Thanks to https://pragprog.com/book/tpantlr2/the-definitive-antlr-4-reference
package cool.scope;

import cool.data.structure.symbols.Symbol;

public interface Scope
{
    public String getScopeName ();

    public Scope getEnclosingScope ();

    public void define (Symbol sym);

    public Symbol resolve (String name);
    
    public Object remove (String name);
}
