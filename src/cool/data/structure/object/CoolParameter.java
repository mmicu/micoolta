package cool.data.structure.object;

import cool.data.structure.types.Type;

/**
 * This class represents a "Cool" parameter. Parameter is formed by:
 *      - name (lower case string); 
 *      - type (upper case string).
 *      
 * It has same characteristics of "Cool" attribute but it's usage is different.
 */
public class CoolParameter extends CoolVariable
{
    public CoolParameter (String name, Type type)
    {
        super (name, type);
    }   
}
