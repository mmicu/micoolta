package cool.data.structure.object;

import java.util.ArrayList;

import cool.data.structure.types.Type;

/**
 * This class represents a "Cool" method. Method is formed by:
 *      - name (lower case string); 
 *      - list of parameters;
 *      - return value.
 */
public class CoolMethod
{
    private String name;
    
    private ArrayList<CoolParameter> formalParameters;
    
    private Type returnValue;
    
    
    public CoolMethod (String name)
    {
        this (name, null);
    }
    
    
    public CoolMethod (String name, Type returnValue)
    {
        this.name             = name;
        this.returnValue      = returnValue;
        this.formalParameters = new ArrayList<CoolParameter> ();
    }
    
    
    public void addFormalParameter (CoolParameter parameter)
    {
        this.formalParameters.add (parameter);
    }
    
    
    public boolean hasParameter (String parameterName)
    {
        for (int k = 0, size = this.formalParameters.size (); k < size; k++)
            if (this.formalParameters.get (k).getName ().equals (parameterName))
                return true;
        
        return false;
    }

    
    public String getName ()
    {
        return this.name;
    }

    
    public void setName (String name)
    {
        this.name = name;
    }

    
    public ArrayList<CoolParameter> getFormalParameters ()
    {
        return this.formalParameters;
    }

    
    public void setFormalParameters (ArrayList<CoolParameter> formalParameters)
    {
        this.formalParameters = formalParameters;
    }

    
    public Type getReturnValue ()
    {
        return this.returnValue;
    }

    
    public void setReturnValue (Type returnValue)
    {
        this.returnValue = returnValue;
    }


    @Override
    public boolean equals (Object obj)
    {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (this.getClass () != obj.getClass ())
            return false;
        
        CoolMethod other = (CoolMethod) obj;
        
        // Check parameters of methods
        if (formalParameters == null) {
            if (other.formalParameters != null)
                return false;
        }
        // Handle if formal parameters since two CoolParameter are equals if and only if the parameters have same name
        // but in this case we want to control if parameters have same type
        else {
            int size_p_t = this.getFormalParameters ().size (),
                size_p_o = ((CoolMethod) obj).getFormalParameters ().size ();
            
            if (size_p_t != size_p_o)
                return false;
            
            ArrayList<CoolParameter> p_t = this.getFormalParameters (),
                                     p_o = ((CoolMethod) obj).getFormalParameters ();
            
            // p_t and p_o have the same size
            for (int k = 0; k < size_p_t; k++)
                if (!p_t.get (k).getType ().equals (p_o.get (k).getType ()))
                    return false;
            
            // Get here means that the two methods have:
            //      1) same number of parameters;
            //      2) for each parameter i (0 < i < size-of-parameters), P_T_i have the same type of P_O_i
            //         (where P_T_i is the i-th parameter of method-this and P_O_i is the i-th parameter of method-obj
        }
        
        // Check name of methods
        if (name == null) {
            if (other.name != null)
                return false;
        } 
        else if (!name.equals (other.name))
            return false;
        
        // Check return value of methods
        if (returnValue == null) {
            if (other.returnValue != null)
                return false;
        } 
        else if (!returnValue.equals (other.returnValue))
            return false;
        
        return true;
    }


    @Override
    public String toString()
    {
        return "Method [name=" + name + ", formalParameters=" + formalParameters + ", returnValue=" + returnValue + "]";
    }
}
