package cool.data.structure.object;

import java.util.ArrayList;

/**
 * This class represents a "Cool" class. Class is formed by:
 *      - name (lower case string); 
 *      - list of attributes;
 *      - list of methods;
 *      - parent class (if it's not specified, it inherits from Object class).
 */
public class CoolClass
{
    private String name;
    
    private ArrayList<CoolAttribute> attributes;
    
    private ArrayList<CoolMethod> methods;
    
    private CoolClass parent;
    
    
    public CoolClass ()
    {
        this.name       = "";
        this.attributes = new ArrayList<CoolAttribute> ();
        this.methods    = new ArrayList<CoolMethod> ();
    }
    
    
    public CoolClass (String name)
    {
        this ();
        
        this.name = name;
    }
    
    
    public void addAttribute (CoolAttribute attribute)
    {
        this.attributes.add (attribute);
    }
    
    
    public void addMethod (CoolMethod method)
    {
        this.methods.add (method);
    }
    
    
    public boolean hasAttribute (CoolAttribute ca)
    {
        return this.hasAttribute (ca.getName ());
    }
    
    
    public boolean hasAttribute (String attributeName)
    {
        for (int k = 0, size = this.attributes.size (); k < size; k++)
            if (this.attributes.get (k).getName ().equals (attributeName))
                return true;
        
        return false;
    }
    
    
    public boolean hasAttributeInParentsClasses (String attributeName)
    {
        CoolClass parent = this.getParent ();
        
        while (parent != null) {
            if (parent.hasAttribute (attributeName))
                return true;
            
            parent = parent.getParent ();
        }
        
        return false;
    }
    
    
    // For signature we mean:
    //      - return value;
    //      - same parameters:
    //          - same length;
    //          - for each pair, P'_i.type = P''_i.type
    public boolean hasMethodWithDifferentSignatureInParentsClasses (CoolMethod method)
    {
        return this.getMethodWithDifferentSignatureInParentsClasses (method) != null;
    }
    
    
    public String getMethodWithDifferentSignatureInParentsClasses (CoolMethod method)
    {
        if (method == null || method.getName () == null)
            return null;
        
        CoolClass parent = this.getParent ();
        
        while (parent != null) {
            ArrayList<CoolMethod> methodsParent = parent.getMethods ();
            
            for (int k = 0, size_m_p = methodsParent.size (); k < size_m_p; k++) {
                CoolMethod m_p = methodsParent.get (k);
                
                // Methods have same name, we check if they have different signature
                if (m_p.getName ().equals (method.getName ()))
                    if (!m_p.equals (method))
                        return m_p.getName (); // m_p.getName () = method.getName ()
            }
            
            parent = parent.getParent ();
        }
        
        return null;
    }
    
    
    public boolean hasMethod (CoolMethod cm)
    {
        return this.hasMethod (cm.getName ());
    }
    
    
    public boolean hasMethod (String methodName)
    {
        return this.getIndexMethod (methodName) != -1;
    }
    
    
    public int getIndexMethod (String methodName)
    {
        for (int k = 0, size = this.methods.size (); k < size; k++)
            if (this.methods.get (k).getName ().equals (methodName))
                return k;
        
        return -1;
    }
    
    
    // A < B:
    //      - A = this;
    //      - B = otherClass.
    public boolean conformance (CoolClass otherClass)
    {
        if (otherClass == null)
            return false;
        
        // Base case
        if (otherClass.equals (new CoolClass ("Object")))
            return true;
        // First condition: A < A
        if (this.equals (otherClass))
            return true;
        // Second condition: if A inherits from B, then A < B
        if (this.getParent () != null && this.getParent ().equals (otherClass))
            return true;
        
        // Third condition
        CoolClass thisParent = this.getParent ();
        
        while (thisParent != null) {
            if (thisParent.conformance (otherClass))
                return true;
            
            thisParent = thisParent.getParent ();
        }
        
        return false;
    }


    public String getName ()
    {
        return this.name;
    }


    public void setName (String name)
    {
        this.name = name;
    }


    public ArrayList<CoolAttribute> getAttributes ()
    {
        return this.attributes;
    }


    public void setAttributes (ArrayList<CoolAttribute> attributes)
    {
        this.attributes = attributes;
    }


    public ArrayList<CoolMethod> getMethods ()
    {
        return this.methods;
    }


    public void setMethods (ArrayList<CoolMethod> methods)
    {
        this.methods = methods;
    }


    public CoolClass getParent ()
    {
        return this.parent;
    }


    public void setParent (CoolClass parent)
    {
        this.parent = parent;
    }


    // COOL classes are equal if they have the same name
    @Override
    public boolean equals (Object obj)
    {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (getClass () != obj.getClass ())
            return false;
        
        CoolClass other = (CoolClass) obj;
        
        if (name == null) {
            if (other.name != null)
                return false;
        } 
        else if (!name.equals (other.name))
            return false;
        
        return true;
    }


    @Override
    public String toString ()
    {
        String s = "Class: " + this.name + ", " + ((this.parent == null)  ? "no parent class." : "parent class: " + this.parent.getName ());
        
        s += "\n";
        
        // Print attributes
        for (int k = 0, size_a = this.attributes.size (); k < size_a; k++)
            s += (k == 0) ? "List attributes: " : "" + this.attributes.get (k).getName () + " : " + 
                 this.attributes.get (k).getType () + ((k != size_a - 1) ? ", " : "");
         
        // Print methods
        for (int k = 0, size_m = this.methods.size (); k < size_m; k++) {
            CoolMethod m = this.methods.get (k);
            
            s += "\n " + m.getReturnValue ().getType () + " " + m.getName () + " (";
            
            // Print formal parameters
            ArrayList<CoolParameter> params = m.getFormalParameters ();
            
            for (int j = 0, size_p = params.size (); j < size_p; j++)
                s += params.get (j).getName () + " : " + params.get (j).getType () +
                                  ((j == size_p - 1) ? ")" : ", ");
            
            s += (params.size () == 0) ? ")" : "";
        }
        
        return s += "\n";
    }
}
