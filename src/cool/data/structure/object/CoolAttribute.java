package cool.data.structure.object;

import cool.data.structure.types.Type;

/**
 * This class represents a "Cool" attribute. Attribute is formed by:
 *      - name (lower case string); 
 *      - type (upper case string).
 */
public class CoolAttribute extends CoolVariable
{
    public CoolAttribute (String name)
    {
        super (name, new Type ("Object"));
    }
    
    
    public CoolAttribute (String name, Type type)
    {
        super (name, type);
    }   
}
