package cool.data.structure.object;

import cool.data.structure.types.Type;

/**
 * This class represents a "Cool" variable. Variable can be:
 *      - an attribute;
 *      - a parameter.
 *      
 * Both of them are composed by:
 *      - name (lower case string); 
 *      - type (upper case string).
 */
public class CoolVariable
{
    private String name;
    
    private Type type;
    
    
    public CoolVariable (String name, Type type)
    {
        this.name = name;
        this.type = type;
    }


    public String getName ()
    {
        return this.name;
    }


    public void setName (String name)
    {
        this.name = name;
    }


    public Type getType ()
    {
        return this.type;
    }


    public void setType (Type type)
    {
        this.type = type;
    }

    
    /* 
     * Two variable are equal if they have the same name.
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals (Object obj)
    {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (this.getClass () != obj.getClass ())
            return false;
        
        CoolVariable other = (CoolVariable) obj;
        
        if (this.name == null) {
            if (other.name != null)
                return false;
        } 
        else if (this.name.equals (other.name))
            return true;
        
        return false;
    }


    @Override
    public String toString ()
    {
        return "Attribute [name=" + name + ", type=" + type.toString () + "]";
    }
}
