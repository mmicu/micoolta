package cool.data.structure.symbols;

import cool.data.structure.types.Type;

public class IdentifierSymbol extends Symbol
{
    public IdentifierSymbol (String name, Type type)
    {
        super (name, type);
    }
}
