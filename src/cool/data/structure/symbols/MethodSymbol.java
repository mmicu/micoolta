package cool.data.structure.symbols;

import java.util.LinkedHashMap;
import java.util.Map;

import cool.data.structure.types.Type;

import cool.scope.Scope;

public class MethodSymbol extends Symbol implements Scope
{
    private String methodName;
    
    private Map<String, Symbol> formalParameters;
   
    private Scope enclosingScope;

    
    public MethodSymbol (String name, Type returnValue, Scope enclosingScope)
    {
        super (name, returnValue);
        this.methodName       = name;
        this.enclosingScope   = enclosingScope;
        this.formalParameters = new LinkedHashMap<String, Symbol> ();
    }

    
    @Override
    public String getScopeName ()
    {
        return "Method [" + this.methodName + "]";
    }

    
    @Override
    public Scope getEnclosingScope()
    {
        return this.enclosingScope;
    }

    
    @Override
    public void define (Symbol sym)
    {
        this.formalParameters.put (sym.getName (), sym);
        sym.setScope (this); // track the scope in each symbol
    }

    
    @Override
    public Symbol resolve (String name)
    {
        Symbol s = this.formalParameters.get (name);
        if (s != null) 
            return s;
        
        // If not here, check any enclosing scope
        if (this.getEnclosingScope () != null)
            return this.getEnclosingScope ().resolve (name);
        
        return null; // Not found
    }
    
    
    public Map<String, Symbol> getFormalParameters ()
    {
        return this.formalParameters;
    }
    
    
    @Override
    public String toString ()
    {
        return "MethodSymbol <" + this.methodName + " (" + this.formalParameters.values () + ")>";
    }


    @Override
    public Object remove (String name)
    {
        return null;
    }
}
