package cool.data.structure.symbols;

import cool.data.structure.types.Type;

import cool.scope.Scope;

public class Symbol
{
    private String name;
    
    private Type type;
    
    private Scope scope;
    
    
    public Symbol (String name)
    {
        this.name = name;
    }
    
    
    public Symbol (String name, Type type)
    {
        this.name = name;
        this.type = type;
    }


    public String getName ()
    {
        return this.name;
    }


    public void setName (String name)
    {
        this.name = name;
    }


    public Type getType ()
    {
        return this.type;
    }


    public void setType (Type type)
    {
        this.type = type;
    }


    public Scope getScope ()
    {
        return this.scope;
    }


    public void setScope (Scope scope)
    {
        this.scope = scope;
    }
    
    
    @Override
    public String toString ()
    {
        return "<" + this.name + " (" + this.type + ")>";
    }
}
