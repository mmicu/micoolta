package cool.data.structure.types;

public class Type
{
    private String type;
    
    
    public Type ()
    {
        this.type = "";
    }
    
    
    public Type (String value)
    {
        this.type = value;
    }
    
    
    public String getType ()
    {
        return this.type;
    }
    
    
    @Override
    public boolean equals (Object obj)
    {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (this.getClass () != obj.getClass ()) {
            if (obj instanceof String) // new Type ("Int") is equal to new String ("Int")
                return this.type == null ? false : this.type.equals (obj);
            
            return false;
        }
        
        Type other = (Type) obj;
        
        if (this.type == null) {
            if (other.type != null)
                return false;
        } 
        else if (!type.equals (other.type))
            return false;
        
        return true;
    }


    @Override
    public String toString ()
    {
        return this.type;
    }
}
