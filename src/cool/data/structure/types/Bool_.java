package cool.data.structure.types;

public class Bool_ extends Type
{
    private boolean value;
    
    
    public Bool_ ()
    {
        this (false);
    }
    
    
    public Bool_ (boolean value)
    {
        super ("Bool");
        
        this.setDefaultValue (value);
    }

    
    public boolean getDefaultValue ()
    {
        return this.value;
    }

    
    public void setDefaultValue (boolean value)
    {
        this.value = value;
    }


    @Override
    public String toString ()
    {
        return "Bool [value=" + this.value + "]";
    }
}
