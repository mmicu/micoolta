package cool.data.structure.types;

public class Int_ extends Type
{
    private int value;
   
    
    public Int_ ()
    {
        this (0);
    }
    
    
    public Int_ (int value)
    {
        super ("Int");
        
        this.setValue (value);
    }

    
    public int getValue ()
    {
        return this.value;
    }

    
    public void setValue (int value)
    {
        this.value = value;
    }


    @Override
    public String toString ()
    {
        return "Int [value=" + this.value + "]";
    }
}
