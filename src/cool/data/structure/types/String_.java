package cool.data.structure.types;

public class String_ extends Type
{
    private String value;
   
    
    public String_ ()
    {
        this ("");
    }
    
    
    public String_ (String value)
    {
        super ("String");
        
        this.setValue (value);
    }

    
    public String getValue ()
    {
        return this.value;
    }

    
    public void setValue (String value)
    {
        this.value = value;
    }


    @Override
    public String toString ()
    {
        return "String [value=" + this.value + "]";
    }
}
