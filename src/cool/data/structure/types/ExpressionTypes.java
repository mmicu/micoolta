package cool.data.structure.types;

public enum ExpressionTypes
{
    ARITHMETIC,
    ASSIGN,
    ATTR_INIT,
    ATTR_NO_INIT,
    CASE,
    COMPARE,
    DISPATCH,
    EQUAL,
    FALSE,
    IDENTIFIER,
    IF,
    INT,
    ISVOID,
    LET_INIT,
    LET_NO_INIT,
    LOOP,
    METHOD,
    NEG,
    NEW,
    NOT,
    SEQUENCE,
    STATIC_DISPATCH,
    STRING,
    PAR,
    TRUE,
    UNKNOWN
}
