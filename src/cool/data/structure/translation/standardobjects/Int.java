package cool.data.structure.translation.standardobjects;

class Int extends Object {
    public int value;
    
    public Int () {
        this.value = 0;
    }
    
    public Int (int value) {
        this.value = value;
    }

    @Override
    public boolean equals (java.lang.Object obj) {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (this.getClass () != obj.getClass ()) {
            if (obj instanceof Integer)
                return this.value == ((Integer) obj).intValue ();
            
            return false;
        }
        
        Int other = (Int) obj;
        
        if (this.value != other.value)
            return false;
        
        return true;
    }
}
