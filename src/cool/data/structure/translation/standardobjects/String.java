package cool.data.structure.translation.standardobjects;

class String extends Object {
    public java.lang.String value;
    
    public String () {
        this.value = "";
    }
    
    public String (java.lang.String value) {
        this.value = value;
    }
    
    public Integer length () {
        return new Integer (this.value.length ());
    }
    
    public String concat (String s) {
        return new String (this.value.concat (s.value));
    }
    
    // substr returns the substring of its self parameter beginning at position i with length l (string positions are numbered beginning at 0)
    public String substr (Integer i, Integer l) {
        if ((i < 0 || l < 0) ||
            (i + l > this.length ()) ||
            (i > this.length () || l > this.length ()))
            throw new RuntimeException ("substr (...), illegal use of indexes (i=" + i + ", l=" + l + ", len= " + this.length () + ")");
        
        java.lang.String res = "";
        
        for (int k = i; k < (i + l); k++)
            res += new Character (this.value.charAt (k)).toString ();
        
        return new String (res);
    }

    @Override
    public boolean equals (java.lang.Object obj) {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (this.getClass () != obj.getClass ()) {
            if (obj instanceof Boolean)
                return new Boolean (this.value).booleanValue () == ((Boolean) obj).booleanValue ();
            if (obj instanceof Integer)
                return new Integer (this.value).intValue () == ((Integer) obj).intValue ();
            
            return false;
        }
        
        String other = (String) obj;
        
        if (this.value == null) {
            if (other.value != null)
                return false;
        } 
        else if (!this.value.equals (other.value))
            return false;
        
        return true;
    }
}
