package cool.data.structure.translation.standardobjects;

class Bool extends Object {
    public boolean value;
    
    public Bool () {
        this.value = false;
    }
    
    public Bool (boolean value) {
        this.value = value;
    }

    @Override
    public boolean equals (java.lang.Object obj) {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (this.getClass () != obj.getClass ()) {
            if (obj instanceof Boolean)
                return this.value == ((Boolean) obj).booleanValue ();
            
            return false;
        }
        
        Bool other = (Bool) obj;
        
        if (this.value != other.value)
            return false;
        
        return true;
    }
}
