package cool.data.structure.translation.standardobjects;

import java.util.Scanner;

class IO extends Object {
    public IO out_string (String x) {
        System.out.print (x.value);
        
        return this;
    }
    
    public IO out_int (Integer x) {
        System.out.print (x);
        
        return this;
    }
    
    @SuppressWarnings("resource")
    public String in_string () {
        return new String (new Scanner (System.in).nextLine ());
    }
    
    @SuppressWarnings("resource")
    public Integer in_int () {
        return new Integer (new Scanner (System.in).nextInt ());
    }
}
