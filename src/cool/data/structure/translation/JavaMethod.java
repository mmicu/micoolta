package cool.data.structure.translation;

import utils.TranslationUtils;

import cool.data.structure.types.Type;

public class JavaMethod implements Cloneable
{
    private String name;
    
    private Type returnValue;
    
    private String arguments;
    
    private String content;
    
    private boolean isStatic;
    
    private TranslationUtils.Visibility visibility;
    
    
    public JavaMethod (String name, Type returnValue, String arguments)
    {
        this.name        = name;
        this.returnValue = returnValue;
        this.arguments   = arguments;
        this.content     = "";
        this.isStatic    = false;
        this.visibility  = TranslationUtils.STANDARD_VISIBILITY_METHOD;
    }
    
    
    public JavaMethod (String name, Type returnValue, String arguments, boolean isStatic)
    {
        this (name, returnValue, arguments);
        
        this.isStatic = isStatic;
    }
    
    
    public JavaMethod (String name, Type returnValue, String arguments, TranslationUtils.Visibility visibility)
    {
        this (name, returnValue, arguments);
        
        this.visibility = visibility;
    }
    
    
    public JavaMethod (String name, Type returnValue, String arguments, boolean isStatic, TranslationUtils.Visibility visibility)
    {
        this (name, returnValue, arguments, isStatic);
        
        this.visibility = visibility;
    }
    
    
    public void addContent (String content)
    {
        this.content += content;
    }


    public String getName ()
    {
        return this.name;
    }


    public void setName (String name)
    {
        this.name = name;
    }


    public Type getReturnValue ()
    {
        return this.returnValue;
    }


    public void setReturnValue (Type returnValue)
    {
        this.returnValue = returnValue;
    }


    public String getContent ()
    {
        return this.content;
    }


    public void setContent (String content)
    {
        this.content = content;
    }
    
    
    public String getArguments ()
    {
        return this.arguments;
    }


    public void setArguments (String arguments)
    {
        this.arguments = arguments;
    }


    public String getSignature ()
    {
        String visibility = TranslationUtils.getStringVisibility (this.visibility),
               ret        = TranslationUtils.resolveType (this.returnValue.toString ());
        
        return visibility + (this.isStatic ? " static " : " ") + (!ret.equals ("") ? ret + " " : "") + this.getName () + " " + this.arguments;
    }
    
    
    @Override
    public Object clone () throws CloneNotSupportedException 
    {
        return super.clone ();
    }
}
