package cool.data.structure.translation;

public enum Indentation
{
    ATTRIBUTE,
    METHOD,
    BODY,
    COMMENT
}
