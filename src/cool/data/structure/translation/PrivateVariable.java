package cool.data.structure.translation;

public class PrivateVariable
{
    private String realName;
    
    private String newName;
    
    
    public PrivateVariable (String realName)
    {
        this.realName = realName;
    }
    
    
    public PrivateVariable (String realName, String newName)
    {
        this (realName);
        
        this.newName = newName;
    }


    public String getRealName ()
    {
        return this.realName;
    }


    public void setRealName (String realName)
    {
        this.realName = realName;
    }


    public String getNewName ()
    {
        return this.newName;
    }


    public void setNewName (String newName)
    {
        this.newName = newName;
    }


    @Override
    public boolean equals (Object obj)
    {
        if (this == obj)
            return true;
        
        if (obj == null)
            return false;
        
        if (this.getClass () != obj.getClass ())
            return false;
        
        PrivateVariable other = (PrivateVariable) obj;
        
        if (realName == null) {
            if (other.realName != null)
                return false;
        } 
        else if (!realName.equals (other.realName))
            return false;
        
        return true;
    }
}
