package cool.translation.java;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import utils.GenericUtils;
import utils.IOUtils;
import utils.TranslationUtils;

import antlr.coolParser.AttributeDeclarationContext;
import antlr.coolParser.ExpressionContext;
import antlr.coolParser.MethodDeclarationContext;

import cool.data.structure.object.CoolClass;
import cool.data.structure.translation.JavaMethod;
import cool.data.structure.translation.PrivateVariable;
import cool.data.structure.types.ExpressionTypes;
import cool.data.structure.types.Type;

import cool.scope.Scope;

import cool.typechecking.TypeEnvironment;

public class Translation
{
    private TemplateJavaClass currentTemplate;
    
    private Stack<ArrayList<PrivateVariable>> stackVariables;
    
    private ArrayList<String> privateMethods;
    
    private ExpressionContext currentExpression;
    
    private int indexPrivateMethod;
    
    private boolean isProtectedMethod;
    
    private int originalSizePrivateMethods;
    
    private int limit;
    
    private String attribute;
    
    private String returnValue;
    
    private boolean isConstructor;
    
    private String lastStatement;
    
    private ArrayList<TemplateJavaClass> templates;
    
    // Scope variables ~ Start
    private CoolClass currentClass;
    
    private ArrayList<CoolClass> coolClasses;
    
    private ParseTreeProperty<Scope> scope;
    
    private Scope currentScope;
    
    private ParseTreeProperty<Scope> entireScope;
    
    private AttributeDeclarationContext attributeContext;
    
    private MethodDeclarationContext methodContext;
    // Scope variables ~ End
    
    
    
    public Translation (TemplateJavaClass currentTemplate, Stack<ArrayList<PrivateVariable>> stackVariables, ArrayList<TemplateJavaClass> templates)
    {
        this.currentTemplate            = currentTemplate;
        this.stackVariables             = stackVariables;
        this.privateMethods             = new ArrayList<String> ();
        this.indexPrivateMethod         = 0;
        this.isProtectedMethod          = true;
        this.originalSizePrivateMethods = this.currentTemplate.getLengthPrivateMethods ();
        this.returnValue                = "Object";
        this.templates                  = templates;
        
        this.lastStatement = null;
        
        this.isConstructor = false;
    }
    
    
    public TemplateJavaClass applyJavaTranslation (ExpressionContext rootExpression)
    {
        this.handleParentNode (rootExpression, true);
        
        return this.currentTemplate;
    }
    
    
    @SuppressWarnings("incomplete-switch")
    private void handleParentNode (ExpressionContext expression, boolean addReturn)
    {
        if (expression == null)
            IOUtils.printError ("Variable expression is null.", true);
        if (expression.isEmpty ())
            IOUtils.printError ("Variable expression is empty.", true);
        
        List<ExpressionContext> expressions = expression.expression ();
        List<TerminalNode> identifiers      = expression.Identifier ();
        
        if (expressions == null)
            IOUtils.printError ("Variable expressions is null. Line: " + expression.getStart ().getLine (), true);
        if (identifiers == null)
            IOUtils.printError ("Variable identifiers is null. Line: " + expression.getStart ().getLine (), true);
        
        int size_e = expressions.size (),
            size_i = identifiers.size ();
        
        // Last expression we handle
        this.currentExpression = expression;
        
        ExpressionTypes expressionType = GenericUtils.identifyTypeExpression (expression);
        
        if (addReturn)
            this.checkAndAddReturnStatement (expression, expressionType);
        
        // Change the scope
        switch (expressionType) {
        case SEQUENCE:
        case LET_INIT:
        case LET_NO_INIT:
        case CASE:
            this.currentScope = this.entireScope.get (expression);
            
            if (this.currentScope == null)
                IOUtils.printError ("Variable currentScope is null.", true);
        }
        
        switch (expressionType) {
        // --- Rule [arith] (pag. 22) - Start
        // expression '+' expression (';')?
        // |
        // expression '-' expression (';')?
        // |
        // expression '*' expression (';')?
        // |
        // expression '/' expression (';')?
        case ARITHMETIC:
        {
            if (size_e != 2)
                this.printError ("ARITHMETIC", "Number of expressions are " + size_e + " instead of 2", expression, true);
            
            String operator = expression.getChildCount () >= 1 ? expression.getChild (1).toString () : "UNKNOWN_OPERATOR";
            
            this.emit ("new Integer (");
            this.translateExpression (expressions.get (0));
            this.emit (" " + operator + " ");
            this.translateExpression (expressions.get (1));
            this.emit (")");
            
            break;
        }
        // --- Rule arith (pag. 22) - End
            
            
        // --- Rule [assign] (pag. 19) - Start
        // Identifier '<-' expression (';')?
        case ASSIGN:
        {
            if (size_e != 1)
                this.printError ("ASSIGN", "Number of expressions are " + size_e + " instead of 1", expression, true);
            if (size_i != 1)
                this.printError ("ASSIGN", "Number of identifiers are " + size_i + " instead of 1", expression, true);
            
            String id = this.resolveIdentifier (identifiers.get (0).toString ());
            
            this.emit (id + " = ");
            this.translateExpression (expressions.get (0));
            
            break;
        }
        // --- Rule [assign] (pag. 19) - End
            
            
        // We ignore ATTR_INIT and ATTR_NO_INIT
        
            
        // --- Rule [case] (pag. 21) - Start
        // 'case' expression 'of' (Identifier ':' Identifier '=>' expression ';')+ 'esac' (';')?
        case CASE:
        {
            /*
             * Example of a case:
                class_type(var : A_X) : SELF_TYPE {
                  case var of
                     a : A => out_string("Class type is now A\n");
                     b : B => out_string("Class type is now B\n");
                     c : C => out_string("Class type is now C\n");
                     d : D => out_string("Class type is now D\n");
                     e : E => out_string("Class type is now E\n");
                     o : Object => out_string("Oooops\n");
                  esac
                };
             */
            // Min value of size_e (number of expression) are 2. One after 'case' and one after 'of' (there is a '+')
            if (size_e < 2)
                this.printError ("CASE", "Expressions number must be >= 2 instead of " + size_e, expression, true);
            // Min value of size_i (number of identifiers) are 2. After 'of' there is a '+' for each "case"
            if (size_i < 2)
                this.printError ("CASE", "Identifiers number must be >= 2 instead of " + size_i, expression, true);
            if (size_i % 2 != 0)
                this.printError ("CASE", "size_i % 2 != 0", expression, true);
            
            // Case structure:
            // Id : Id => Exp;
            // ...
            // Id : Id => Exp;
            // For each line, first Id is an Identifier while second Id is a Type
            int k = 0;
            
            // Let introduces new scope
            ArrayList<PrivateVariable> stackEntry = new ArrayList<PrivateVariable> ();
            
            this.emit ("{\n");
            int index_exp = 1;
            while (k < size_i) {
                // Identifier k % 2 == 0, else Type
                String id   = identifiers.get (k++).toString (),
                       type = TranslationUtils.resolveType (identifiers.get (k++).toString ());
                
                // Stack has already an entry for this case
                if (k > 2)
                    this.stackVariables.pop ();
                
                stackEntry.add (new PrivateVariable (id, ("temp_case_" + id)));
                
                this.stackVariables.push (stackEntry);
                
                // Enter here for the first time
                if (k <= 2) {
                    this.emit ("if (");
                    this.translateExpression (expressions.get (0));
                    this.emit (" == null) {\n");
                    this.emit ("throw new RuntimeException (\"Case on NULL\");\n");
                    this.emit ("}\n");
                }
                
                this.emit (k <= 2 ? "if (" : "else if (");
                this.translateExpression (expressions.get (0));
                String objInstanceOf = this.lastStatement;
                this.emit (" instanceof " + type + ") {\n");
                //this.emit (type + " " + this.resolveIdentifier (id, true) + " = " + GenericUtils.addDefaultConstructor (type) + ";\n");
                this.emit (type + " " + this.resolveIdentifier (id, true) + " = (" + type + ") " + objInstanceOf + ";\n");
                this.handleParentNode (expressions.get (index_exp++), addReturn);
                this.emit ("}\n");
            }
            this.emit ("else {\n");
            this.emit ("throw new RuntimeException (\"No matching branch in case statement.\");\n");
            this.emit ("}\n"); // End of else
            this.emit ("}\n"); // End of block of case
            
            this.stackVariables.pop ();
            
            break;
        }
        // --- Rule [case] (pag. 21) - End
          
            
        // --- Rule [compare] (pag. 21) - Start
        // expression '<' expression (';')?
        // |
        // expression '<=' expression (';')?
        case COMPARE:
        {
            if (size_e != 2) 
                this.printError ("COMPARE", "Expressions number must be 2 instead of " + size_e, expression, true);
            
            int n_child = expression.getChildCount ();
            if (n_child != 3)
                this.printError ("COMPARE", "getChildCount () must be 3 instead of " + n_child, expression, true);
            
            this.translateExpression (expressions.get (0));
            this.emit (" " + expression.getChild (1).toString () + " ");
            this.translateExpression (expressions.get (1));
            
            break;
        }
        // --- Rule [compare] (pag. 21) - End
            
            
        // --- Rule [dispatch] (pag. 19) - Start
        // &
        // --- Rule [static-dispatch] (pag. 20) - Start
        case DISPATCH:
        case STATIC_DISPATCH:
        {
            // Examples:
            //      - (new C)@A.method5(avar.value())
            //      - c.method6(c.value())
            //      - abort ()
            int n_child = expression.getChildCount (), exp_k = 0;
            
            // First child is an expression
            if (n_child > 0 && expression.getChild (0) instanceof ExpressionContext) {
                this.emit ("(");
                this.translateExpression (expressions.get (0));
                this.emit (").");
                
                exp_k = 1;
            }
            
            // Default value
            String methodToCall = size_i == 1 ? identifiers.get (0).toString () : size_i == 2 ? identifiers.get (1).toString () : "";
            
            // We have '@'
            if (size_i == 2) {
                String atClass    = identifiers.get (0).toString (),
                       methodName = identifiers.get (1).toString (),
                       typeExp    = null;
                
                // Expression, before @, that we must evaluate
                typeExp = this.getTypeExpression (expressions.get (0).expression (0)).toString ();
                
                // May be: (new C)@C.method5 (avar.value ())
                if (!typeExp.equals (atClass)) {
                    // We must get the method (methodName) of atClass
                    // First of all we get the template of the atClass
                    TemplateJavaClass templateAtClass = GenericUtils.getTemplateByClassName (this.templates, atClass); 
                    if (templateAtClass == null)
                        this.printError ("DISPATCH | STATIC_DISPATCH", "templateAtClass is null. atClass = " + atClass, expression, true);
                    
                    TemplateJavaClass templateTypeExp = GenericUtils.getTemplateByClassName (this.templates, typeExp); 
                    if (templateTypeExp == null)
                        this.printError ("DISPATCH | STATIC_DISPATCH", "templateTypeExp is null. typeExp = " + typeExp, expression, true);
                    
                    // We must get the method (methodName) of atClass
                    // Then we can get the Java method from the template
                    JavaMethod jMethod = GenericUtils.getInstanceJavaMethodOfTemplate (templateAtClass, methodName);
                    if (jMethod == null)
                        this.printError ("DISPATCH | STATIC_DISPATCH", "jMethod is null. methodName = " + methodName, expression, true);
                    
                    // We must copy jMethod of class atClass, in the class "typeExp" (since typeExp is a name of a class)
                    // Moreover, we must check if already create the method. Since, in different part of the program, we can encounter
                    // same call (example: (new C)@C.method5 (avar.value ()) in line X and (new C)@C.method5 (avar.value ()) in line Y
                    // where X != Y
                    
                    // Name of new method: atClass + "_" + methodName + "__"
                    String newMethodName = atClass + "_" + methodName + "__";

                    if (GenericUtils.getInstanceJavaMethodOfTemplate (this.currentTemplate, newMethodName) != null)
                        this.printError ("DISPATCH | STATIC_DISPATCH", newMethodName + " already exists in class " + this.currentTemplate.getClassName (), expression, true);

                    try {
                        JavaMethod newJavaMethod = (JavaMethod) jMethod.clone ();
                        
                        newJavaMethod.setName (newMethodName);
                        templateTypeExp.addMethod (newJavaMethod);
                        
                        methodToCall = newMethodName;
                    } 
                    catch (CloneNotSupportedException e) {
                        this.printError ("DISPATCH | STATIC_DISPATCH", "Impossible to clone jMethod", expression, true);
                    }
                }
            } // if (size_i == 2) - end
            this.emit (methodToCall + " (");
            
            for (int k = exp_k; k < size_e; k++) {
                this.translateExpression (expressions.get (k));
                
                this.emit (k < size_e - 1 ? ", " : "");
            }
            
            this.emit (")");
            
            break;
        }
        // --- Rule [dispatch] (pag. 19) - End
        // &
        // --- Rule [static-dispatch] (pag. 20) - Start
            
            
        // --- Rule [equal] (pag. 22) - Start
        // expression '=' expression (';')?
        case EQUAL:
        {
            if (size_e != 2)
                this.printError ("EQUAL", "Number of expressions are " + size_e + " instead of 2", expression, true);
            
            this.translateExpression (expressions.get (0));
            this.emit (".equals (");
            this.translateExpression (expressions.get (1));
            this.emit (")");
            
            break;
        }
        // --- Rule [equal] (pag. 22) - End
            
            
        // --- Rule [false] (pag. 19) - Start
        case FALSE:
        {
            if (size_e != 0)
                this.printError ("FALSE", "Number of expressions are " + size_e + " instead of 0", expression, true);
            
            this.emit ("new Boolean (false)");
            
            break;
        }
        // --- Rule [false] (pag. 19) - End
            
            
        // --- Rule [identifier] (pag. 19) - Start
        case IDENTIFIER:
        {
            if (size_i != 1)
                this.printError ("IDENTIFIER", "Number of identifiers are " + size_i + " instead of 1", expression, true);
            
            this.emit (this.resolveIdentifier (identifiers.get (0).toString ()));
            
            break;
        }
        // --- Rule [identifier] (pag. 19) - End
        
        
        // --- Rule [if] (pag. 20) - Start
        // 'if' expression 'then' expression 'else' expression 'fi' (';')?
        case IF:
        {
            if (size_e != 3)
                this.printError ("IF", "Number of expressions are " + size_e + " instead of 3", expression, true);
            
            this.emit ("if (");
            this.translateExpression (expressions.get (0));
            this.emit (") {\n");
            this.handleParentNode (expressions.get (1), addReturn);
            this.emit ("}\nelse {\n");
            this.handleParentNode (expressions.get (2), addReturn);
            this.emit ("}\n");
            
            break;
        }
        // --- Rule [if] (pag. 20) - End
            
        
        // --- Rule [int] (pag. 19) - Start
        case INT:
            String int_ = (expression != null) ? expression.Integer ().toString () : null;
            
            this.emit ("new Integer (" + int_ + ")");
            
            break;
        // --- Rule [int] (pag. 19) - End
             
            
        // --- Rule [isvoid] (pag. 21) - Start
        // 'isvoid' expression (';')?
        case ISVOID:
        {
            if (size_e != 1)
                this.printError ("ISVOID", "Number of expressions are " + size_e + " instead of 1", expression, true);
            
            this.emit ("new Boolean (");
            this.translateExpression (expressions.get (0));
            this.emit (" == null)");
            
            break;
        }
        // --- Rule [isvoid] (pag. 21) - End
        
        
        // --- Rule [let-init] (pag. 20) - Start
        // 'let' Identifier ':' Identifier ('<-' expression)? 'in' expression (';')? 
        // &
        // --- Rule [let-no-init] (pag. 21) - Start
        // 'let' Identifier ':' Identifier 'in' expression (';')? 
        case LET_INIT:
        case LET_NO_INIT:
        {
            if (size_i < 2)
                this.printError ("LET", "Number of identifiers are " + size_i + " instead of >= 2", expression, true);
            if (size_i % 2 != 0)
                this.printError ("LET", "size_i % 2 != 0", expression, true);

            int n_child = expression.getChildCount ();
            
            ExpressionContext inExp = null;
            
            // In a "let" definition, may be id with same name
            ArrayList<String> idsLet = new ArrayList<String> ();
            
            // Let introduces new scope
            ArrayList<PrivateVariable> stackEntry = new ArrayList<PrivateVariable> ();
            
            for (int k = 0; k < n_child; k++) {
                ParseTree child_k = expression.getChild (k);
                
                String id = "UNKNOWN_ID", type = "UNKNOWN_TYPE";
                
                // ID : TYPE
                if (child_k.toString ().equals (":")) {
                    if (k - 1 >= 0)
                        id = expression.getChild (k - 1).toString ();
                    if (k + 1 < n_child - 1)
                        type = TranslationUtils.resolveType (expression.getChild (k + 1).toString ());
                    
                    if (!idsLet.contains (id)) {
                        String newId = this.generateNewPrivateAttribute (id);
                        
                        this.currentTemplate.addGlobalVariableDeclaration (
                                TranslationUtils.getJavaSyntaxOfAttribute (
                                        newId, 
                                        type,
                                        // Since we can have a '@', so we need to use the attributes of parent class
                                        TranslationUtils.Visibility.PROTECTED
                                )
                        );
                        
                        if (this.currentTemplate.getDefaultConstructor () == null) {
                            String arguments = TranslationUtils.getJavaSyntaxOfArguments (null);
                            
                            this.currentTemplate.setDefaultConstructor (
                                    this.currentTemplate.getClassName (), 
                                    new Type (), 
                                    arguments,
                                    TranslationUtils.Visibility.PUBLIC
                            );
                        }
                        
                        this.currentTemplate.addContentToConstructor ("this." + newId + " = " + TranslationUtils.addDefaultConstructor (type) + "\n");
                        
                        //this.currentTemplate.addContentToMethod ("this." + paramNewName + " = " + paramName + ";\n", this.isProtectedMethod);
                        
                        stackEntry.add (new PrivateVariable (id, newId));
                        
                        this.emit (type + " " + newId + " = " + TranslationUtils.addDefaultConstructor (type) + ";\n");
                        
                        // Stack has already an entry for this let
                        if (idsLet.size () >= 1)
                            this.stackVariables.pop ();
                        
                        this.stackVariables.push (stackEntry);
                        
                        idsLet.add (id);
                    }
                    
                    // There may be an initialization
                    if (k + 2 < n_child - 1 && expression.getChild (k + 2).toString ().equals ("<-")) {
                        this.emit (this.resolveIdentifier (id) + " = ");
                        this.translateExpression ((ExpressionContext) expression.getChild (k + 3));
                        this.emit (";\n");
                    }
                }
                
                if (child_k.toString ().equals ("in") && k + 1 < n_child && expression.getChild (k + 1) instanceof ExpressionContext)
                    inExp = (ExpressionContext) expression.getChild (k + 1);
            }
            
            if (inExp == null)
                this.printError ("LET", "inExp == null", expression, true);
            
            this.handleParentNode (inExp, addReturn);
            
            this.stackVariables.pop ();
            
            break;
        }
        // --- Rule [let-init] (pag. 20) - End
        // &
        // --- Rule [let-no-init] (pag. 21) - End
        
        
        // --- Rule [loop] (pag. 21) - Start
        case LOOP:
        {
            if (size_e != 2)
                this.printError ("LOOP", "Number of expressions are " + size_e + " instead of 2", expression, true);
            
            this.emit ("while (");
            this.translateExpression (expressions.get (0));
            this.emit (") {\n");
            //this.handleParentNode (expressions.get (1), addReturn);
            this.handleParentNode (expressions.get (1), false);
            this.emit ("}\n");
            
            if (addReturn)
                this.emit ("return new Object ();\n");
            
            break;
        }
        // --- Rule [loop] (pag. 21) - End
        
        
        // We ignore METHOD
            
        
        // --- Rule [neg] (pag. 21) - Start
        case NEG:
        {
            if (size_e != 1)
                this.printError ("NEG", "Number of expressions are " + size_e + " instead of 1", expression, true);
            
            this.emit ("~");
            this.translateExpression (expressions.get (0));

            break;
        }
        // --- Rule [neg] (pag. 21) - End
            
        
        // --- Rule [new] (pag. 19) - Start
        case NEW:
        {
            if (size_i != 1)
                this.printError ("NEW", "Number of identifiers are " + size_i + " instead of 1", expression, true);
            
            // Casting the object
            this.emit (TranslationUtils.addDefaultConstructor (TranslationUtils.resolveType (identifiers.get (0).toString (), true)));
            
            break;
        }
        // --- Rule [new] (pag. 19) - End
            
        
        // --- Rule [not] (pag. 21) - Start
        case NOT:
        {
            if (size_e != 1)
                this.printError ("NOT", "Number of expressions are " + size_e + " instead of 1", expression, true);
            
            this.emit ("!");
            this.translateExpression (expressions.get (0));

            break;
        }
        // --- Rule [not] (pag. 21) - End
            
        
        // --- Rule [sequence] (pag. 20) - Start
        case SEQUENCE:
        {
            if (size_e == 0)
                this.printError ("SEQUENCE", "size_e is 0", expression, true);
            
            this.emit ("{\n");
            
            // Number of expressions are >= 1, but we check anyway
            for (int k = 0; k < size_e; k++) {
                //this.translateExpression (expressions.get (k), k == (size_e - 1) ? true : false);
                this.handleParentNode (expressions.get (k), k == (size_e - 1) && addReturn);
                
                this.emit (";\n");
            }
            
            this.emit ("}\n");
            
            break;
        }
        // --- Rule [sequence] (pag. 20) - End
            
        
        // --- Rule [string] (pag. 19) - Start
        case STRING:
        {
            String string_ = (expression != null) ? expression.String ().toString () : null;
            
            this.emit ("new String (" + string_ + ")");
            
            break;
        }
        // --- Rule [string] (pag. 19) - End
       
        
        // --- Rule [par] (pag. 20) - Start
        case PAR:
        {
            if (size_e != 1)
                this.printError ("PAR", "Number of expressions are " + size_e + " instead of 1", expression, true);
            
            ExpressionTypes expTypeChild = GenericUtils.identifyTypeExpression (expressions.get (0));
            
            String openPar  = expTypeChild == ExpressionTypes.LET_INIT || expTypeChild == ExpressionTypes.LET_NO_INIT ? "{\n" : "(",
                   closePar = openPar.equals ("{\n") ? "}\n" : ")";
            
            this.emit (openPar);
            if (openPar.equals ("{\n"))
                this.handleParentNode (expressions.get (0), addReturn);
            else
                this.translateExpression (expressions.get (0), addReturn);
            this.emit (closePar);

            break;
        }
        // --- Rule [par] (pag. 20) - End
        
        
        // --- Rule [true] (pag. 19) - Start
        case TRUE:
        {
            if (size_e != 0)
                this.printError ("TRUE", "Number of expressions are " + size_e + " instead of 0", expression, true);
            
            this.emit ("new Boolean (true)");
            
            break;
        }
        // --- Rule [true] (pag. 19) - End  

        case UNKNOWN:
        default:
            this.printError ("UNKNOWN", "Unknown expression: " + expression.toStringTree (), expression, true);
        }
    }
    
    
    public void handleExpressionConstructor (ExpressionContext expression, String attributeName, String attributeType)
    {
        String methodName = "constructor_for_" + attributeName,
               arguments  = TranslationUtils.getJavaSyntaxOfArguments (null);
        
        this.isConstructor = true;
        
        this.currentTemplate.addConstructorMethod (methodName, new Type (attributeType), arguments);
        
        this.currentTemplate.addContentToConstructor ("this." + attributeName + " = this." + methodName + " ();\n");
        
        this.attribute = attributeName;
        
        this.returnValue = attributeType;
        
        this.translateExpressionForConstructor (expression, true, 0);
    }
    
    
    private String translateExpressionForConstructor (ExpressionContext expression, boolean isRootExpression)
    {
        return this.translateExpressionForConstructor (expression, isRootExpression, -1);
    }
    
    
    private String translateExpressionForConstructor (ExpressionContext expression, boolean isRootExpression, int n)
    {
        if (expression == null)
            IOUtils.printError ("Variable expression is null.", true);
        if (expression.isEmpty ())
            IOUtils.printError ("Variable expression is empty.", true);
        
        ExpressionTypes expressionType = GenericUtils.identifyTypeExpression (expression);
        
        //this.updateReturnValuePrivateMethod (expression, expressionType);
        
        if (n == 0)
            this.emit ("return ");
        
        switch (expressionType) {
        case CASE:
        case IF:
        case LET_INIT:
        case LET_NO_INIT:
        case LOOP:
        case SEQUENCE:
            String methodName = "constructor_" + this.attribute + this.currentTemplate.getLengthPrivateMethods (),
                   arguments  = TranslationUtils.getJavaSyntaxOfArguments (null);
            
            
            this.emit (methodName + " ()");
            
            // Add the method to the template
            this.currentTemplate.addPrivateMethod (methodName, new Type (this.returnValue), arguments);
            
            if (isProtectedMethod) {
                this.isProtectedMethod = false;
                this.limit             = 1;
            }
            else
                this.limit = 0;
            
            this.indexPrivateMethod = this.currentTemplate.getLengthPrivateMethods () - 1;
            
            this.handleParentNode (expression, true);
            
            if (this.limit == 1)
                this.isProtectedMethod = true;
            else
                this.indexPrivateMethod--;
            
            if (this.indexPrivateMethod == this.originalSizePrivateMethods - 1)
                this.isProtectedMethod = true;
            
            
            return "";
            
        case UNKNOWN:
            this.printError ("UNKNOWN", "Unknown expression: " + expression.toStringTree (), expression, true);
            
            break;
            
        default:
            this.handleParentNode (expression, false);
        }
        
        return "";
    }
    
    
    private String translateExpression (ExpressionContext expression)
    {
        return this.translateExpression (expression, false);
    }
    
    
    private String translateExpression (ExpressionContext expression, boolean isRootExpression)
    {
        if (expression == null)
            IOUtils.printError ("Variable expression is null.", true);
        if (expression.isEmpty ())
            IOUtils.printError ("Variable expression is empty.", true);
        
        if (this.isConstructor)
            this.translateExpressionForConstructor (expression, isRootExpression);
        else {
            ExpressionTypes expressionType = GenericUtils.identifyTypeExpression (expression);
            
            switch (expressionType) {
            case CASE:
            case IF:
            case LET_INIT:
            case LET_NO_INIT:
            case LOOP:
            //case SEQUENCE:
                String methodName = this.generateNewPrivateMethod (),
                       arguments  = TranslationUtils.getJavaSyntaxOfArguments (null);
                
                this.emit (methodName + " ()");
                
                // Add the method to the template
                this.currentTemplate.addPrivateMethod (methodName, new Type (this.returnValue), arguments);
                
                if (isProtectedMethod) {
                    this.isProtectedMethod = false;
                    this.limit             = 1;
                }
                else
                    this.limit = 0;
                
                this.indexPrivateMethod = this.currentTemplate.getLengthPrivateMethods () - 1;
                
                this.handleParentNode (expression, true);
                
                if (this.limit == 1)
                    this.isProtectedMethod = true;
                else
                    this.indexPrivateMethod--;
                
                if (this.indexPrivateMethod == this.originalSizePrivateMethods - 1)
                    this.isProtectedMethod = true;
                
                break;
                
            case UNKNOWN:
                this.printError ("UNKNOWN", "Unknown expression: " + expression.toStringTree (), expression, true);
                
                break;
                
            default:
                this.handleParentNode (expression, false);
                
                break;
            }
        }
        
        return "";
    }
    
    
    private boolean checkAndAddReturnStatement (ExpressionContext expression, ExpressionTypes expressionType)
    {
        // Get the Type value of the expression
        Type typeExp = this.getTypeExpression (expression);
        
        if (typeExp == null)
            IOUtils.printError ("[Translation] typeExp is null", true);
        
        switch (expressionType) {
        case ARITHMETIC:
        case ASSIGN:
        case COMPARE:
        case DISPATCH:
        case EQUAL:  
        case FALSE:
        case IDENTIFIER:
        case INT:
        case ISVOID:
        case NEG:
        case NEW:
        case NOT:
        case STATIC_DISPATCH:
        case STRING:
        case TRUE:
            this.emit ("return ");
            
            String casting = "";
            
            if (!this.isProtectedMethod) {
            
                boolean modified = this.currentTemplate.setActiveMethodReturnValue (
                        this.isProtectedMethod, 
                        this.isProtectedMethod ? -1 : this.indexPrivateMethod, 
                        typeExp.getType ()//this.returnValue
                );
                
                if (!modified)
                    this.printError ("", "Impossible to modify return value", expression, true);
                
                String ret = this.currentTemplate.getActiveMethodReturnValue (
                                this.isProtectedMethod, 
                                this.isProtectedMethod ? -1 : this.indexPrivateMethod
                             );
                
                if (ret == null)
                    this.printError ("", "getActiveMethodReturnValue () is null", expression, true);
                
                casting = TranslationUtils.resolveType (ret);
            }
            else
                casting = TranslationUtils.resolveType (this.currentTemplate.getLastProtectedMethod ().getReturnValue ().toString ());
            
            
            this.emit ("(" + casting + ") ");
            
            return true;
            
            
        case CASE:
        case IF:
        case LET_INIT:
        case LET_NO_INIT:
        case LOOP:
        case PAR:
        case SEQUENCE:
            break;
        
        default:
            this.printError ("UNKNOWN", "Unknown expression: " + expression.toStringTree (), expression, true);
            
            break;
        }
        
        return false;
    }
    
    
    private String generateNewPrivateAttribute (String attributeName)
    {
        this.checkIntegrityStack (attributeName);
        
        String methodName = this.currentTemplate.activeMethodIsProtected () ? this.currentTemplate.getLastProtectedMethod ().getName ()
                                                                            : this.currentTemplate.getLastPrivateMethod ().getName ();
        
        String prefixNewName = this.currentTemplate.getClassName () + "_" + methodName + "_" + attributeName + "_";
        
        return prefixNewName + this.generateIdForPrivateAttribute (prefixNewName);
    }
    
    
    private int generateIdForPrivateAttribute (String prefixNewName)
    {
        ArrayList<String> privateAttributes = this.currentTemplate.getGlobalVariablesDeclaration ();
        
        int counter = 0;
        
        for (int k = 0, size_p = privateAttributes.size (); k < size_p; k++) {
            // privateAttributes are declared as: VISIBILITY TYPE NAME_ATTRIBUTE; 
            String[] split_ = privateAttributes.get (k).split (" ");
            
            if (split_.length != 3)
                IOUtils.printError ("Translation, split is different from 3", true);
            
            String nameVariable = split_[2].replace (";", "");

            if ((prefixNewName + new Integer (counter).toString ()).equals (nameVariable))
                counter++;
        }
        
        return counter;
    }
    
    
    private String generateNewPrivateMethod ()
    {
        // Name of private methods are: NAME_OF_THE_CLASS + "_" + PARENT_METHOD_NAME + KEY
        // KEY is an auto increment value
        String methodName = this.currentTemplate.getClassName () + "_" + this.currentTemplate.getLastProtectedMethod ().getName () + 
                            "_" + this.privateMethods.size ();
        
        this.privateMethods.add (methodName);
        
        return methodName;
    }
    
    
    private void checkIntegrityStack (String attributeName)
    {
        try {
            ArrayList<PrivateVariable> entry = this.stackVariables.peek ();
            
            for (int k = 0, size = entry.size (); k < size; k++)
                if (attributeName.equals (entry.get (k).getRealName ()))
                    IOUtils.printError ("[Translation] Duplicate attributes value in the stack [" + attributeName + "]. Class=" + this.currentTemplate.getClassName (), true);
        }
        catch (EmptyStackException e) {
            IOUtils.printError ("EmptyStackException on checkIntegrityStack (...)");
        }
    }
    
    
    private String resolveIdentifier (String identifier)
    {
        return this.resolveIdentifier (identifier, false);
    }
    
    
    private String resolveIdentifier (String identifier, boolean insideCase)
    {
        if (identifier.equals ("self"))
            return "this";
        // identifier can be an attribute
        else if (this.currentTemplate.containsAttribute (identifier))
            return identifier;
        
        // identifier is a variable
        Iterator<ArrayList<PrivateVariable>> iter = this.stackVariables.iterator ();
        
        String newName = null;
        
        while (iter.hasNext ()) {
            ArrayList<PrivateVariable> pt_k = iter.next ();
            
            for (int k = 0, size = pt_k.size (); k < size; k++) {
                if (identifier.equals (pt_k.get (k).getRealName ())) {
                    newName = ((insideCase) ? "" : "") + pt_k.get (k).getNewName ();
                    
                    break;
                }
            }
        }

        if (newName == null)
            IOUtils.printError ("[Translation] Identifier not found [" + identifier + "]. Exp=" + this.currentExpression.toStringTree () +
                                "line=" + this.currentExpression.getStart ().getLine (), true);
        
        return newName;
    }
    
    
    public void setIsConstructor (boolean isConstructor)
    {
        this.isConstructor = isConstructor;
    }
    
    
    private void emit (String content)
    {
        if (this.isConstructor) {
            if (this.isProtectedMethod)
                this.currentTemplate.addContentToMethodOfConstructor (content);
            else
                this.currentTemplate.addContentToMethod (content, false, this.indexPrivateMethod);
        }
        else
            this.currentTemplate.addContentToMethod (content, this.isProtectedMethod, this.isProtectedMethod ? -1 : this.indexPrivateMethod);
        
        this.lastStatement = content;
    }
    
    
    private void printError (String typeExpression, String message, ExpressionContext expContext, boolean terminate)
    {
        if (expContext != null)
            IOUtils.printError ("[Translation] Expression type: " + typeExpression + ". Message: " + message + ". Line: " + 
                                expContext.getStart ().getLine () + "\nExpression: " + expContext.toStringTree (), terminate);
        else
            IOUtils.printError ("[Translation] Expression type: " + typeExpression + ". Message: " + message, terminate);
    }
    
    
    private Type getTypeExpression (ExpressionContext expression)
    {
        // Create type environment for attribute or method
        TypeEnvironment te = new TypeEnvironment (this.currentClass.getName (), this.coolClasses, this.scope);
        
        // Set the current scope
        te.setCurrentScope (this.currentScope);
        
        // Set the entire scope
        te.setEntireScope (this.scope);
        
        // Set attribute context
        te.setAttributeContext (this.attributeContext);
        
        // Set method context
        te.setMethodContext (this.methodContext);
        
        // Evaluate expression
        return te.evaluate (expression, GenericUtils.identifyTypeExpression (expression));
    }


    public void setCurrentClass (CoolClass currentClass)
    {
        this.currentClass = currentClass;
    }


    public void setCoolClasses (ArrayList<CoolClass> coolClasses)
    {
        this.coolClasses = coolClasses;
    }


    public void setScope (ParseTreeProperty<Scope> scope)
    {
        this.scope = scope;
    }


    public void setCurrentScope (Scope currentScope)
    {
        this.currentScope = currentScope;
    }


    public void setEntireScope (ParseTreeProperty<Scope> entireScope)
    {
        this.entireScope = entireScope;
    }


    public void setAttributeContext (AttributeDeclarationContext attributeContext)
    {
        this.attributeContext = attributeContext;
    }


    public void setMethodContext (MethodDeclarationContext methodContext)
    {
        this.methodContext = methodContext;
    }
}
