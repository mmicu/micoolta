package cool.translation.java;

import java.util.ArrayList;

import utils.IOUtils;
import utils.TranslationUtils;

import cool.data.structure.translation.Indentation;
import cool.data.structure.translation.JavaMethod;
import cool.data.structure.types.Type;

public class TemplateJavaClass
{
    private String className;
    
    private String classDeclaration;
    
    private ArrayList<String> attributesDeclaration;
    
    private ArrayList<String> globalVariablesDeclaration;
    
    private JavaMethod defaultConstructor;
    
    private ArrayList<JavaMethod> constructorMethods;
    
    private ArrayList<JavaMethod> methods;
    
    private ArrayList<JavaMethod> privateMethods;
    
    private boolean activeMethodIsStandardMethod;
    
    
    public TemplateJavaClass (String className, String classDeclaration)
    {
        this.className                    = className;
        this.classDeclaration             = classDeclaration;
        this.attributesDeclaration        = new ArrayList<String> ();
        this.globalVariablesDeclaration   = new ArrayList<String> ();
        this.defaultConstructor           = null;
        this.constructorMethods           = new ArrayList<JavaMethod> ();
        this.methods                      = new ArrayList<JavaMethod> ();
        this.privateMethods               = new ArrayList<JavaMethod> ();
        this.activeMethodIsStandardMethod = true;
    }
    
    
    public void addAttributeDeclaration (String attributeDeclaration)
    {
        this.attributesDeclaration.add (attributeDeclaration);
    }
    
    
    public void addGlobalVariableDeclaration (String globalVariable)
    {
        this.globalVariablesDeclaration.add (globalVariable);
    }
    
    
    public void setDefaultConstructor (String name, Type returnValue, String arguments)
    {
        this.setDefaultConstructor (name, returnValue, arguments, TranslationUtils.STANDARD_VISIBILITY_METHOD);
    }
    
    
    public void setDefaultConstructor (String name, Type returnValue, String arguments, TranslationUtils.Visibility visibility)
    {
        this.defaultConstructor = new JavaMethod (this.className, new Type (), arguments, visibility);
    }
    
    
    public void addConstructorMethod (String name, Type returnValue, String arguments)
    {
        this.constructorMethods.add (new JavaMethod (name, returnValue, arguments));
        
        this.activeMethodIsStandardMethod = false;
    }
    
    
    public void addMethod (String name, Type returnValue, String arguments)
    {
        this.addMethod (name, returnValue, arguments, TranslationUtils.STANDARD_VISIBILITY_METHOD);
    }
    
    
    public void addMethod (String name, Type returnValue, String arguments, TranslationUtils.Visibility visibility)
    {
        this.methods.add (new JavaMethod (name, returnValue, arguments, visibility));
        
        this.activeMethodIsStandardMethod = true;
    }
    
    
    public void addMethod (JavaMethod jm)
    {
        this.methods.add (jm);
    }
    
    
    public void addPrivateMethod (String name, Type returnValue, String arguments)
    {
        this.addPrivateMethod (name, returnValue, arguments, TranslationUtils.STANDARD_VISIBILITY_METHOD);
    }
    
    
    public void addPrivateMethod (String name, Type returnValue, String arguments, TranslationUtils.Visibility visibility)
    {
        this.privateMethods.add (new JavaMethod (name, returnValue, arguments, visibility));
        
        this.activeMethodIsStandardMethod = false;
    }
    
    
    public void addContentToMethod (String statement, boolean isCoolMethod)
    {
        this.addContentToMethod (statement, isCoolMethod, -1);
    }
    
    
    public void addContentToMethod (String statement, boolean isCoolMethod, int index)
    {
        // Get last declared method and add the statement to it.
        // isCoolMethod = true  -> add to attribute methods;
        // isCoolMethod = false -> add to attribute privateMethods.
        if (isCoolMethod) {
            index = (index == - 1) ? this.methods.size () - 1 : index;
            this.methods.get (index).addContent (statement);
            
            this.activeMethodIsStandardMethod = true;
        }
        else {
            index = (index == -1) ? this.privateMethods.size () - 1 : index;
            this.privateMethods.get (index).addContent (statement);
            
            this.activeMethodIsStandardMethod = false;
        }
    }
    
    
    public void addContentToConstructor (String statement)
    {
        this.defaultConstructor.addContent (statement);
    }
    
    
    public void addContentToMethodOfConstructor (String statement)
    {
        this.addContentToMethodOfConstructor (statement, -1);
    }
    
    
    public void addContentToMethodOfConstructor (String statement, int index)
    {
        index = (index == -1) ? this.constructorMethods.size () - 1 : index;
        
        this.constructorMethods.get (index).addContent (statement);
    }
    
    
    public String getClassName ()
    {
        return this.className;
    }
    
    
    public JavaMethod getActiveMethod (boolean isCoolMethod)
    {
        return isCoolMethod ? this.getLastProtectedMethod () : this.getLastPrivateMethod ();
    }
    
    
    public String getActiveMethodReturnValue (boolean isCoolMethod, int index)
    {
        JavaMethod jm = null;
        
        if (isCoolMethod) {
            index = (index == -1) ? this.methods.size () - 1 : index;
            
            if (index >= this.methods.size ())
                return null;
            
            jm = this.methods.get (index);
        }
        else {
            index = (index == -1) ? this.privateMethods.size () - 1 : index;
            
            if (index >= this.privateMethods.size ())
                return null;
            
            jm = this.privateMethods.get (index);
        }
        
        return jm.getReturnValue ().toString ();
    }
    
    
    public boolean setActiveMethodReturnValue (boolean isCoolMethod, int index, String returnValue)
    {
        JavaMethod jm;
        
        if (isCoolMethod) {
            index = (index == -1) ? this.methods.size () - 1 : index;
            
            if (index >= this.methods.size ())
                return false;
            
            jm = this.methods.get (index);
            jm.setReturnValue (new Type (returnValue));
            
            this.methods.set (index, jm);
        }
        else {
            index = (index == -1) ? this.privateMethods.size () - 1 : index;
            
            if (index >= this.privateMethods.size ())
                return false;
            
            jm = this.privateMethods.get (index);
            jm.setReturnValue (new Type (returnValue));
            
            this.privateMethods.set (index, jm);
        }
        
        return true;
    }
    
    
    public JavaMethod getLastProtectedMethod ()
    {
        return this.methods.get (this.methods.size () - 1);
    }
    
    
    public JavaMethod getLastPrivateMethod ()
    {
        return this.privateMethods.get (this.privateMethods.size () - 1);
    }
    
    
    public boolean activeMethodIsProtected ()
    {
        return this.activeMethodIsStandardMethod;
    }
    
    
    public int getLengthPrivateMethods ()
    {
        return this.privateMethods.size ();
    }
    
    
    public int getLengthConstructorMethods ()
    {
        return this.constructorMethods.size ();
    }
    
    
    public JavaMethod getDefaultConstructor ()
    {
        return this.defaultConstructor;
    }
    
    
    public ArrayList<String> getGlobalVariablesDeclaration ()
    {
        return this.globalVariablesDeclaration;
    }


    public boolean containsAttribute (String attributeName)
    {
        for (int k = 0, size = this.attributesDeclaration.size (); k < size; k++) {
            String[] parts = this.attributesDeclaration.get (k).split (" ");
            
            if (parts.length >= 2 && parts[2].length () >= 1) {
                String attributeNameApp = parts[2].substring (0, parts[2].length () - 1);
                
                if (attributeNameApp.equals (attributeName))
                    return true;
            }
        }
        
        return false;
    }


    public ArrayList<JavaMethod> getMethods ()
    {
        return this.methods;
    }


    @Override
    public String toString ()
    {
        String s = "\n" + this.classDeclaration + " {\n";
        
        int size_a  = this.attributesDeclaration.size (),
            size_g  = this.globalVariablesDeclaration.size (),
            size_cm = this.constructorMethods.size (),
            size_m  = this.methods.size (),
            size_pm = this.privateMethods.size ();
        
        
        // Attributes
        s += IOUtils.getIndentedCode (size_a == 0 ? "Attributes not specified" : "List attributes - Start\n", Indentation.COMMENT);
        for (int k = 0; k < size_a; k++)
            s += IOUtils.getIndentedCode (this.attributesDeclaration.get (k) + "\n", Indentation.ATTRIBUTE);
        if (size_a > 0)
            s += IOUtils.getIndentedCode ("List attributes - End", Indentation.COMMENT);
        s += "\n\n";
        
        
        // Global variables
        s += IOUtils.getIndentedCode (size_g == 0 ? "Global variable not specified" : "Global variables - Start\n", Indentation.COMMENT);
        for (int k = 0; k < size_g; k++)
            s += IOUtils.getIndentedCode (this.globalVariablesDeclaration.get (k) + "\n", Indentation.ATTRIBUTE);
        if (size_g > 0)
            s += IOUtils.getIndentedCode ("Global variables - End\n", Indentation.COMMENT);
        s += "\n\n";
        
        
        // Default constructor
        if (this.getDefaultConstructor () != null) {
            s += IOUtils.getIndentedCode ("Default constructor - Start\n", Indentation.COMMENT);
            s += IOUtils.getIndentedCode (this.defaultConstructor.getSignature () + " {", Indentation.METHOD);
            s += IOUtils.getIndentedCode (this.defaultConstructor.getContent (), Indentation.BODY);
            s += IOUtils.getIndentedCode ("}\n", Indentation.METHOD);
        }
        else
            s += IOUtils.getIndentedCode ("Default constructor not specified\n", Indentation.COMMENT);
        s += "\n\n";
        
        
        // Constructor Methods
        s += IOUtils.getIndentedCode (size_cm == 0 ? "Constructor Methods not specified" : "Constructor Methods - Start\n", Indentation.COMMENT);
        for (int k = 0; k < size_cm; k++) {
            s += IOUtils.getIndentedCode (this.constructorMethods.get (k).getSignature () + " {", Indentation.METHOD);
            s += IOUtils.getIndentedCode (this.constructorMethods.get (k).getContent (), Indentation.BODY);
            s += IOUtils.getIndentedCode ("}\n", Indentation.METHOD);
        }
        if (size_cm > 0)
            s += IOUtils.getIndentedCode ("Constructor Methods - End", Indentation.COMMENT);
        s += "\n\n";
        
        
        // Methods
        s += IOUtils.getIndentedCode (size_m == 0 ? "Methods not specified" : "Methods - Start\n", Indentation.COMMENT);
        for (int k = 0; k < size_m; k++) {
            s += IOUtils.getIndentedCode (this.methods.get (k).getSignature () + " {", Indentation.METHOD);
            s += IOUtils.getIndentedCode (this.methods.get (k).getContent (), Indentation.BODY);
            s += IOUtils.getIndentedCode ("}\n", Indentation.METHOD);
        }
        if (size_m > 0)
            s += IOUtils.getIndentedCode ("Methods - End", Indentation.COMMENT);
        s += "\n\n";
        
        
        // Private methods
        s += IOUtils.getIndentedCode (size_pm == 0 ? "Private methods not specified\n" : "Private methods - Start\n", Indentation.COMMENT);
        for (int k = 0; k < size_pm; k++) {
            s += IOUtils.getIndentedCode (this.privateMethods.get (k).getSignature () + " {", Indentation.METHOD);
            s += IOUtils.getIndentedCode (this.privateMethods.get (k).getContent (), Indentation.BODY);
            s += IOUtils.getIndentedCode ("}\n", Indentation.METHOD);
        }
        if (size_pm > 0)
            s += IOUtils.getIndentedCode ("Private methods - End\n", Indentation.COMMENT);

        
        return s + "}\n";
    }
}
