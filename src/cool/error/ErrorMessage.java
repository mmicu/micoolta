package cool.error;

import java.util.ArrayList;

import utils.IOUtils;

public class ErrorMessage
{
    public static boolean ERROR_PRINTED = false;
    
    
    public enum Error 
    {
        FILE_NOT_EXISTS (0, "File \"%s\" does not exist"),
        
        // Rule (1)
        FEATURE_NAME_LOWER (1, "Feature name \"%s\" must begin whit a lowercase letter"),
                
        // Rule (2)
        METHOD_REPEATED (2, "Method \"%s\" can be defined only once"),
        
        // Rule (2.1)
        METHOD_REPEATED_CHILD_CLASS (3, "Method \"%s\", in class \"%s\", has already been defined in parent class"),
                
        // Rule (3)
        ATTRIBUTE_REPEATED (4, "Attribute \"%s\" can be defined only once"),
                
        // Rule (4.1)
        CLASS_NAME_UPPER_CASE (5, "Class \"%s\" must begin with an uppercase letter"),
                
        // Rule (4.2)
        CLASS_REDEFINED (6, "Class \"%s\" is already defined"),
                
        // Rule (5)
        ATTRIBUTE_REDEFINED (7, "Attribute \"%s\" is already declared in the parent class"),
                
        // Rule (6)
        IDENTIFIERS_IN_FORMAL_PARAMETER (8, "Identifiers used in the list of formal parameters must be distinct \"%s\""),
                
        // Rule (7)
        STRING_CONSTANT_LIMIT (9, "String constants may be at most 1024 characters long"),
                
        // Rule (8.1), %s belongs to {"let", "case", "formal parameter", "assign"}
        SELF_ASSING_BIND (10, "It is not possible to assign \"self\" or to bind \"self\" in a \"%s\" expression"),
                
        // Rule (8.2)
        SELF_ATTRIBUTE_NAME (11, "It is illegal to have an attribute named self"),
                
        // Rule (9)
        STATIC_TYPE_EXPRESSION (12, "Static type of the expression must conform to the declared type of the identifier"),
                
        // Rule (10)
        VOID_DISPATCH_1 (13, "Type \"void\" is not admitted in the dispatch"),
                
        // Rule (11)
        STATIC_TYPE_ACTUAL_PARAMETER (14, "Static type of the actual parameter must conform to the declared type of the formal parameter"),
                
        // Rule (12)
        STATIC_TYPE_DISPATCH_3 (15, "Static type to the left of \"@\" must conform to the type specified to the right of \"@\""),
                
        // Rule (13)
        EXPRESSION_CASE (16, "Expression of \"case\" cannot be \"void\""),
                
        // Rule (14)
        INTEGER_EXPRESSIONS (17, "Static types of the two sub-expressions must be Integer (Int)"),
                
        // Rule (15-16-17), %s belongs to {"Int", "String", "Bool"}
        CLASS_STD_ERROR_INHERIT (18, "It is an error to inherit from \"%s\""),
                
        // Rule (15-16-17-18), %s belongs to {"IO", "Int", "String", "Bool"}
        CLASS_STD_ERROR_REDEFINE (19, "It is an error redefine \"%s\""),
                
        // Rule (19.1)
        TYPE_IDENTIFIER_CAPITAL_LETTER (20, "Type identifier \"%s\" must begin with a capital letter"),
                
        // Rule (19.2)
        OBJECT_IDENTIFIER_LOWER_LETTER (21, "Object identifier \"%s\" must begin with a lower case letter"),
        
        //
        CLASS_PARENT_NOT_DEFINED (22, "Class parent of \"%s\" (\"%s\") is not defined"),
        
        //
        JAVA_KEYWORD (23, "You cannot use Java keyword \"%s\" for %s"),
        
        //
        CONFORMANCE (24, "No conformance between types \"%s\" and \"%s\" (expression type: %s)"),
        
        //
        CYCLIC_INHERITANCE (25, "Cyclic inheritance between \"%s\" and \"%s\""),
        
        //
        ATTRIBUTE_METHOD_SAME_NAME (26, "Attribute and method have the same name (\"%s\")"),
        
        //
        ERROR (27, "%s");

        private final int code;
        
        private final String description;

        
        private Error (int code, String description) 
        {
            this.code        = code;
            this.description = description;
        }
        

        public String getDescription () 
        {
           return this.description;
        }
        

        public int getCode () 
        {
            return this.code;
        }

        
        @Override
        public String toString () 
        {
            return this.description;
        }
    }
    
    
    public static void printError (ErrorMessage.Error e, String ... arguments)
    {
        ErrorMessage.printError (e, 0, arguments);
    }
    
    
    public static void printError (ErrorMessage.Error e, int line, String ... arguments)
    {
        int countArguments     = countOccurrencesReplaced (e.getDescription ());
        ArrayList<String> args = new ArrayList<String> ();
        
        for (String arg : arguments) {
            args.add (arg);
        }
        
        if (countArguments != args.size ())
            IOUtils.printWarning ("Message with code " + e.getCode () + ", number of arguments do not match");
        else
            IOUtils.printError (String.format (e.getDescription (), arguments) + ((line == 0) ? "" : ". Line " + line));
        
        if (!ErrorMessage.ERROR_PRINTED)
            ErrorMessage.ERROR_PRINTED = true;
    }
    
    
    private static int countOccurrencesReplaced (String s)
    {
        return (s == null) ? 0 : (s.split ("%s", -1).length) - 1;
    }
}
