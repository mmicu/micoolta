package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import cool.data.structure.translation.JavaMethod;
import cool.data.structure.types.Type;

import cool.translation.java.TemplateJavaClass;

public class FileUtils
{
    public static boolean fileExists (String file)
    {
        return file != null && new File (file).exists () && new File (file).isFile ();
    }
    
    
    public static boolean pathExists (String path)
    {
        return path != null && new File (path).exists () && new File (path).isDirectory ();
    }
    
    
    public static String getExtension (String file)
    {
        int k = file.lastIndexOf ('.');

        return (k >= 0) ? file.substring (k + 1) : "";
    }
    
    
    public static String getFileNameByPath (String path)
    {
        int k = path.lastIndexOf (File.separator);

        return (k == -1) ? path : path.substring (k + 1 <= path.length () ? k + 1 : k, path.length ());
    }
    
    
    public static void writeStandardClassesIntoOutputFile (PrintWriter writer) throws IOException
    {
        FileUtils.writeStandardClassesIntoOutputFile (writer, null);
    }
    
    
    public static void writeStandardClassesIntoOutputFile (PrintWriter writer, String package_) throws IOException
    {
        String fileObject = "src/cool/data/structure/translation/standardobjects/Object.java",
               //fileInt    = "src/cool/data/structure/translation/standardobjects/Int.java",
               //fileBool   = "src/cool/data/structure/translation/standardobjects/Bool.java",
               fileString = "src/cool/data/structure/translation/standardobjects/String.java",
               fileIO     = "src/cool/data/structure/translation/standardobjects/IO.java";
        
        writer.write (TranslationUtils.getJavaSyntaxOfComment (" File generated on " + GenericUtils.getCurrentDateAndTime () + "\n\n"));
        
        if (package_ != null)
            writer.write ("package " + package_ + ";\n\n");
        
        // For IO methods (in_string () and in_int ())
        writer.write ("import java.util.Scanner;\n\n");
        
        FileUtils.writeStandardClassesIntoOutputFile (fileObject, writer);
        //FileUtils.writeStandardClassesIntoOutputFile (fileInt, writer);
        //FileUtils.writeStandardClassesIntoOutputFile (fileBool, writer);
        FileUtils.writeStandardClassesIntoOutputFile (fileString, writer);
        FileUtils.writeStandardClassesIntoOutputFile (fileIO, writer);
        
        writer.close ();
    }
    
    
    private static void writeStandardClassesIntoOutputFile (String file, PrintWriter writer) throws IOException
    {
        if (!FileUtils.fileExists (file))
            IOUtils.printError ("[FileUtils] writeStandardClassesIntoOutputFile (...): \"" + file + "\" does not exist", true);
        
        String content = "", line = "";
        
        @SuppressWarnings("resource")
        BufferedReader br = new BufferedReader (new FileReader (file));
        
        while ((line = br.readLine ()) != null) {
            if ((line.length () >= 6 && line.substring (0, 6).equals ("import")) ||
                (line.length () >= 7 && line.substring (0, 7).equals ("package")))
                continue;
            
            if (line.length () > 0)  
                content += line + "\n";
        }
        
        writer.write ("\n" + content);
    }
    
    
    public static void writeMainClassIntoOutputFile (PrintWriter writer, String className)
    {
        TemplateJavaClass templateJClass = new TemplateJavaClass (
                className,
                TranslationUtils.getJavaSyntaxOfClass (className, "Object")
        );
        
        String arguments = TranslationUtils.getJavaArgumentsOfMainMethod ();

        // Add the method to the template
        templateJClass.addMethod (new JavaMethod ("main", new Type ("void"), arguments, true, TranslationUtils.Visibility.PUBLIC));
        
        String content = "Main m = new Main ();\nm.main ();";
        
        templateJClass.addContentToMethod (content, true);
        
        writer.write (templateJClass.toString ());
        
        writer.close ();
    }
    
    
    // We already checked if the file exists
    public static void deleteContentFile (String file) throws FileNotFoundException
    {
        PrintWriter writer = new PrintWriter (file);
        
        writer.print ("");
        writer.close ();
    }
}
