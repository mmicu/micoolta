package utils;

public class TestUtils
{
    private static final boolean EXIT = true;
    
    private static void fail (String message)
    {
        System.out.println (IOUtils.getColoredString ("[Assert FAIL] ", IOUtils.Color.RED) + message);
    }
    
    
    public static void _assert (String message, boolean condition)
    {
        TestUtils._assert (message, condition, TestUtils.EXIT);
    }
    
    
    public static void _assert (String message, boolean condition, boolean exit)
    {
        if (!condition) {
            TestUtils.fail (message);
            
            if (exit)
                System.exit (-1);
        }
    }
    
    
    public static void _assert_not_null (String message, Object obj)
    {
        TestUtils._assert_not_null (message, obj, TestUtils.EXIT);
    }
    
    
    public static void _assert_not_null (String message, Object obj, boolean exit)
    {
        if (obj == null) {
            TestUtils.fail (message);
            
            if (exit)
                System.exit (-1);
        }
    }
    
    
    public static void _assert_true (String message, boolean condition)
    {
        TestUtils._assert (message, condition, TestUtils.EXIT);
    }
    
    
    public static void _assert_true (String message, boolean condition, boolean exit)
    {
        TestUtils._assert (message, condition, exit);
    }
    
    
    public static void _assert_false (String message, boolean condition)
    {
        TestUtils._assert (message, !condition, TestUtils.EXIT);
    }
    
    
    public static void _assert_false (String message, boolean condition, boolean exit)
    {
        TestUtils._assert (message, !condition, exit);
    }
}
