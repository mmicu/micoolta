package utils;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import antlr.coolParser.ExpressionContext;

import cool.data.structure.object.CoolClass;
import cool.data.structure.object.CoolMethod;
import cool.data.structure.translation.JavaMethod;
import cool.data.structure.types.ExpressionTypes;
import cool.data.structure.types.Type;

import cool.error.ErrorMessage;

import cool.scope.ClassScope;

import cool.translation.java.TemplateJavaClass;

public class GenericUtils
{
    public static CoolClass getInstanceClassByClassName (ArrayList<CoolClass> coolClasses, String className)
    {
        if (coolClasses == null || className == null)
            return null;
        
        for (int k = 0, size = coolClasses.size (); k < size; k++)
            if (coolClasses.get (k).getName ().equals (className))
                return coolClasses.get (k);
        
        return null;
    }
    
    
    public static boolean classExists (ArrayList<CoolClass> coolClasses, CoolClass class_)
    {
        return GenericUtils.getInstanceClassByClassName (coolClasses, class_ != null ? class_.getName () : null) != null;
    }
    
    
    public static boolean classExists (ArrayList<CoolClass> coolClasses, String className)
    {
        if (className.equals ("SELF_TYPE"))
            return true;
        
        return GenericUtils.getInstanceClassByClassName (coolClasses, className) != null;
    }
    
    
    public static ClassScope getClassScopeByClassName (ArrayList<ClassScope> classScope, String className)
    {
        if (classScope == null)
            return null;
        
        for (int k = 0, size = classScope.size (); k < size; k++)
            if (classScope.get (k).getClassName ().equals (className))
                return classScope.get (k);
        
        return null;
    }
    
    
    public static Type getTypeFromMethodName (ArrayList<CoolClass> coolClasses, String methodName)
    {
        if (coolClasses == null)
            return null;
        
        for (int k = 0, size_c = coolClasses.size (); k < size_c; k++) {
            ArrayList<CoolMethod> methods = coolClasses.get (k).getMethods ();
            
            for (int j = 0, size_m = methods.size (); j < size_m; j++)
                if (methods.get (j).getName ().equals (methodName))
                    return new Type (coolClasses.get (k).getName ());
        }
        
        return null;
    }
    
    
    public static CoolMethod getMethodFromParentsClasses (CoolClass class_, String methodName)
    {
        CoolClass parentClass = class_.getParent ();
        
        if (parentClass == null)
            return null;
        
        ArrayList<CoolMethod> methods = parentClass.getMethods ();
        for (int k = 0, size_m = methods.size (); k < size_m; k++)
            if (methods.get (k).getName ().equals (methodName))
                return methods.get (k);
        
        return GenericUtils.getMethodFromParentsClasses (parentClass, methodName);
    }
    
    
    public static boolean parentClassesHaveMethod (CoolClass class_, String methodName)
    {
        CoolClass parentClass = class_.getParent ();
        
        if (parentClass == null)
            return false;
        
        ArrayList<CoolMethod> methods = parentClass.getMethods ();
        for (int k = 0, size_m = methods.size (); k < size_m; k++)
            if (methods.get (k).getName ().equals (methodName))
                return true;
        
        return GenericUtils.parentClassesHaveMethod (parentClass, methodName);
    }
    
    
    public static Type join (ArrayList<CoolClass> coolClasses)
    {
        if (coolClasses == null || coolClasses.size () == 0)
            return new Type ("Object");
        
        ArrayList<CoolClass> parentClasses = new ArrayList<CoolClass> ();
        for (int k = 1, size_c = coolClasses.size (); k < size_c; k++) {
            if (!coolClasses.get (0).equals (coolClasses.get (k))) {
                for (int j = 0; j < size_c; j++) {
                    CoolClass parent = coolClasses.get (j).getParent ();
                    
                    parentClasses.add (parent == null ? new CoolClass ("Object") : parent);
                }
            }
        }
        
        return (parentClasses.size () > 0) ? GenericUtils.join (parentClasses) : new Type (coolClasses.get (0).getName ());
    }
    
    
    public static void resolveMapInstanceParentClass (ArrayList<CoolClass> coolClasses, Map<String, String> mapInstanceParentClass)
    {
        for (String childClass : mapInstanceParentClass.keySet ()) {
            String parentClass = mapInstanceParentClass.get (childClass);
            
            int size_c = coolClasses.size ();
            
            for (int k = 0; k < size_c; k++)
                if (coolClasses.get (k).getName ().equals (childClass))
                    for (int j = 0; j < size_c; j++)
                        if (coolClasses.get (j).getName ().equals (parentClass))
                            coolClasses.get (k).setParent (coolClasses.get (j));
        }
    }
    
    
    public static void checkCyclicInheritance (ArrayList<CoolClass> coolClasses)
    {
        if (coolClasses == null)
            return;
        
        int size_c = coolClasses.size ();
        
        for (int k = 0; k < size_c; k++) {
            CoolClass classParent = coolClasses.get (k).getParent ();
            
            while (classParent != null) {
                if (GenericUtils.containsClassInParents (classParent, coolClasses.get (k)))
                    ErrorMessage.printError (ErrorMessage.Error.CYCLIC_INHERITANCE, classParent.getName (), coolClasses.get (k).getName ());
                
                classParent = classParent.getParent ();
            }
        }
    }
    
    
    private static boolean containsClassInParents (CoolClass parent, CoolClass class_)
    {
        if (parent == null || class_ == null)
            return false;
        
        while (parent != null) {
            if (parent.equals (class_))
                return true;
            
            parent = parent.getParent ();
        }
        
        return false;
    }
    
    
    public static boolean conformance (CoolClass classA, CoolClass classB, ArrayList<CoolClass> coolClasses)
    {
        if (classA == null || classB == null || coolClasses.size () == 0)
            return false;
        
        // First condition
        //if (classA)
        return true;
    }
    
    
    public static ExpressionTypes identifyTypeExpression (ExpressionContext expression)
    {
        int childNumber = expression.getChildCount (),
            numberExpr  = expression.expression ().size ();
        
        ArrayList<String> listNodes = GenericUtils.getListChildNodeFromExpressionContext (expression);
        
        String firstChild = (childNumber > 0) ? expression.getChild (0).toString ().toLowerCase () : null;
        
        if (firstChild == null)
            IOUtils.printError ("firstChild is null", true);
        
        // |Expression| = 0
        if (numberExpr == 0 && childNumber > 0) {
            if (firstChild.equals ("true"))
                return ExpressionTypes.TRUE;
            else if (firstChild.equals ("false"))
                return ExpressionTypes.FALSE;
            else if (expression.String () != null || firstChild.equals ("\""))
                return ExpressionTypes.STRING;
            else if (StringUtils.isNumber (firstChild))
                return ExpressionTypes.INT;
            else if (firstChild.equals ("new"))
                return ExpressionTypes.NEW;
            // Example: hello_world ();     ---> method without arguments
            else if (StringUtils.isIdentifier (firstChild) && !listNodes.contains ("("))
                return ExpressionTypes.IDENTIFIER;
            else if (listNodes.contains ("("))
                return ExpressionTypes.DISPATCH;
        }
        // |Expression| = 1
        else if (numberExpr == 1 && childNumber > 0) {
            if (firstChild.equals ("("))
                return ExpressionTypes.PAR;
            else if (firstChild.equals ("not"))
                return ExpressionTypes.NOT;
            else if (firstChild.equals ("~"))
                return ExpressionTypes.NEG;
            else if (firstChild.equals ("isvoid"))
                return ExpressionTypes.ISVOID;
            else if (listNodes.contains ("<-"))
                return ExpressionTypes.ASSIGN;
        }
        // |Expression| = 2
        else if (numberExpr == 2 && childNumber > 0) {
            if (listNodes.contains ("*") || listNodes.contains ("/") || listNodes.contains ("+") || listNodes.contains ("-"))
                return ExpressionTypes.ARITHMETIC;
            else if (listNodes.contains ("="))
                return ExpressionTypes.EQUAL;
            else if (listNodes.contains ("<=") || listNodes.contains ("<"))
                return ExpressionTypes.COMPARE;
            else if (firstChild.equals ("while"))
                return ExpressionTypes.LOOP;
        }
        // |Expression| = 3
        else if (numberExpr == 3 && childNumber > 0) {
            if (firstChild.equals ("if"))
                return ExpressionTypes.IF;
        }
        // |Expression| >= 0
        if (numberExpr >= 0) {
            if (StringUtils.isIdentifier (firstChild) && expression.getChild (1).toString ().equals ("("))
                return ExpressionTypes.DISPATCH;
        }
        // |Expression| >= 1
        if (numberExpr >= 1) {
            // Dispatch
            if (firstChild.equals ("{"))
                return ExpressionTypes.SEQUENCE;
            else if (firstChild.equals ("let"))
                return ExpressionTypes.LET_INIT;
            else if (listNodes.contains ("."))
                return GenericUtils.whichDispatch (expression);
        }
        // |Expression| >= 2
        if (numberExpr >= 2) {
            // Dispatch
            if (firstChild.equals ("case"))
                return ExpressionTypes.CASE;
        }
        
        return ExpressionTypes.UNKNOWN;
    }
    
    
    private static ExpressionTypes whichDispatch (ExpressionContext e)
    {
        for (int k = 0, size = e.getChildCount (); k < size; k++) {
            if (e.getChild (k).toString ().equals (".")) {
                // X.f (...). X can assume:
                //      - Exp value (Exp could be an identifier)    => StaticDispatch
                //      - Identifier but is not an expression       => Dispatch (Example: out_string ("Hello"))
                //      - X can contain '@'                         => StaticDispatch
                for (int j = 0; j < k; j++)
                    if (e.getChild (j).toString ().equals ("@"))
                        return ExpressionTypes.STATIC_DISPATCH;
                
                // No '@' but there is '.'. It's a DISPATCH
                return ExpressionTypes.DISPATCH;
            }
        }
        
        return ExpressionTypes.UNKNOWN;
    }
    
    
    public static ArrayList<String> getListChildNodeFromExpressionContext (ExpressionContext e)
    {
        ArrayList<String> res = new ArrayList<String> ();
        
        for (int k = 0, size = e.getChildCount (); k < size; k++)
            res.add (e.getChild (k).toString ());
        
        return res;
    }
    
    
    public static TemplateJavaClass getTemplateByClassName (ArrayList<TemplateJavaClass> templates, String className)
    {
        if (templates == null)
            return null;
        
        for (int k = 0, size_t = templates.size (); k < size_t; k++)
            if (templates.get (k).getClassName ().equals (className))
                return templates.get (k);
        
        return null;
    }
    
    
    public static JavaMethod getInstanceJavaMethodOfTemplate (TemplateJavaClass template, String methodName)
    {
        if (template == null || methodName == null)
            return null;
        
        ArrayList<JavaMethod> methods = template.getMethods ();
        
        for (int k = 0, size_m = methods.size (); k < size_m; k++)
            if (methods.get (k).getName ().equals (methodName))
                return methods.get (k);
        
        return null;
    }
    
    
    public static String getCurrentDateAndTime ()
    {
        return new SimpleDateFormat ("dd/MM/yyyy HH:mm:ss").format (new Date ());
    }
}
