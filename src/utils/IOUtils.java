package utils;

import java.util.ArrayList;

import cool.data.structure.translation.Indentation;

public class IOUtils
{
    public enum Color 
    {
        BLACK,
        RED,
        GREEN,
        YELLOW,
        BLUE,
        PURPLE,
        CYAN,
        WHITE
    }
    
    private static final String ANSI_RESET   = "\u001B[0m";
    
    private static final String ANSI_BLACK   = "\u001B[30m";
    
    private static final String ANSI_RED     = "\u001B[31m";
    
    private static final String ANSI_GREEN   = "\u001B[32m";
    
    private static final String ANSI_YELLOW  = "\u001B[33m";
    
    private static final String ANSI_BLUE    = "\u001B[34m";
    
    private static final String ANSI_PURPLE  = "\u001B[35m";
    
    private static final String ANSI_CYAN    = "\u001B[36m";
    
    private static final String ANSI_WHITE   = "\u001B[37m";
    
    
    public static void printError (String message)
    {
        IOUtils.printError (message, false);
    }
    
    
    public static void printError (String message, boolean exit)
    {
        String point = (message.charAt (message.length () - 1) != '.' && message.charAt (message.length () - 1) != '\n') ? "." : "";
        
        System.err.println ("\t" + IOUtils.getColoredString ("[ERROR] ", IOUtils.Color.RED) + message + point);
        
        if (exit)
            System.exit (-1);
    }
    
    
    public static void printWarning (String message)
    {
        String point = (message.charAt (message.length () - 1) != '.') ? "." : "";
        
        System.out.println ("\t" + IOUtils.getColoredString ("[WARNING] ", IOUtils.Color.YELLOW) + message + point);
    }
    
    
    public static void printInfo (String message)
    {
        String point = (message.charAt (message.length () - 1) != '.') ? "." : "";
        
        System.out.println ("\t" + IOUtils.getColoredString ("[INFO] ", IOUtils.Color.CYAN) + message + point);
    }
    
    
    public static void printSuccess (String message)
    {
        String point = (message.charAt (message.length () - 1) != '.') ? "." : "";
        
        System.out.println ("\t" + IOUtils.getColoredString ("[SUCCESS] ", IOUtils.Color.GREEN) + message + point);
    }
    
    
    public static void printColoredMessage (String message, Color color)
    {
        System.out.println (IOUtils.getColoredString (message, color));
    }
    
    
    public static String getColoredString (String message, Color color)
    {
        switch (color) {
        case RED:
            return IOUtils.ANSI_RED    + message + IOUtils.ANSI_RESET;
        case GREEN:
            return IOUtils.ANSI_GREEN  + message + IOUtils.ANSI_RESET;
        case YELLOW:
            return IOUtils.ANSI_YELLOW + message + IOUtils.ANSI_RESET;
        case BLUE:
            return IOUtils.ANSI_BLUE   + message + IOUtils.ANSI_RESET;
        case PURPLE:
            return IOUtils.ANSI_PURPLE + message + IOUtils.ANSI_RESET;
        case CYAN:
            return IOUtils.ANSI_CYAN   + message + IOUtils.ANSI_RESET;
        case WHITE:
            return IOUtils.ANSI_WHITE  + message + IOUtils.ANSI_RESET;
        default:
            return IOUtils.ANSI_BLACK  + message + IOUtils.ANSI_RESET;
        }
    }
    
    
    public static String getIndentedCode (String code, Indentation indentation)
    {
        if (code.equals (""))
            return "";
        
        boolean useTabs = false;
        
        int n = 2;
        
        String res                = "",
               patternIndentation = StringUtils.repeat (useTabs ? "\t" : " ", n);
        
        switch (indentation) {
        case ATTRIBUTE:
        case METHOD:
            res += patternIndentation + code;
            
            break;
            
        case BODY:
            // { -> increment level;
            // } -> decrement level;
            // ; -> \n
            String[] statements = code.split ("\n");
            
            ArrayList<String> statements_app = new ArrayList<String> ();
            
            int level = 1;
            
            for (int k = 0; k < statements.length; k++) {
                int len = statements[k].length ();
                
                if (len > 1 && statements[k].charAt (len - 1) == '}') {
                    statements_app.add (statements[k].substring (0, len - 1));
                    statements_app.add ("}");
                    
                    continue;
                }
                
                statements_app.add (statements[k]);
            }
            
            for (int k = 0, size = statements_app.size (); k < size; k++) {
                String s = statements_app.get (k);
                
                int len = s.length ();

                if (len > 0 && (s.charAt (0) == '{' || s.charAt (len - 1) == '{')) {
                    res += ((k == 0) ? "\n" : "") + StringUtils.repeat (StringUtils.repeat (useTabs ? "\t" : " ", n), level) + s + "\n";
                    
                    level++;
                    
                    continue;
                }
                else if (len > 0 && s.charAt (0) == '}')
                    level--;
                else if (len == 1 && s.charAt (0) == ';')
                    continue;
                
                // Indentation of the statement
                res += ((k == 0) ? "\n" : "") + StringUtils.repeat (StringUtils.repeat (useTabs ? "\t" : " ", n), level) + s;
                
                // Add ';'
                res += (len > 0 && s.charAt (len - 1) != '{' && s.charAt (len - 1) != '}' && s.charAt (len - 1) != ';') ? ";\n" : "\n";
            }
            
            break;
            
        case COMMENT:
            res += patternIndentation + "// " + code;
            
            break;
        }

        return res;
    }
}
