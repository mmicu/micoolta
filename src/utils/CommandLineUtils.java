package utils;

import java.util.ArrayList;

public class CommandLineUtils
{
    public static boolean hasOption (String[] args, String optionName)
    {
        String optionNameApp = "-" + optionName;
        
        for (int k = 0; k < args.length; k++)
            if (optionNameApp.equals (args[k]))
                return true;
        
        return false;
    }
    
    
    public static boolean hasOptions (String[] args, String[] optionsNames)
    {
        for (int k = 0; k < optionsNames.length; k++)
            if (!CommandLineUtils.hasOption (args, optionsNames[k]))
                return false;
        
        return true;
    }
    
    
    public static boolean noOptions (String[] args, String[] optionsNames)
    {
        for (int k = 0; k < optionsNames.length; k++)
            if (CommandLineUtils.hasOption (args, optionsNames[k]))
                return false;
        
        return true;
    }
    
    
    public static ArrayList<String> getOptionValues (String[] args, String optionName, String[] onlyOptions)
    {
        int index             = CommandLineUtils.getIndexOption (args, optionName);
        ArrayList<String> res = new ArrayList<String> ();
        
        // Surely no args for option "optionName"
        if (index == -1 || (index + 1) >= args.length)
            return res;
        
        for (int k = index + 1; k < args.length; k++) {
            for (int j = 0; j < onlyOptions.length; j++)
                if (args[k].equals ("-" + onlyOptions[j]) || (args[k].length () > 0 && args[k].charAt (0) == '-'))
                    return res;
            
            res.add (args[k]);
        }
        
        return res;
    }
    
    
    public static int getIndexOption (String[] args, String optionName)
    {
        String optionNameApp = "-" + optionName;
        
        for (int k = 0; k < args.length; k++)
            if (optionNameApp.equals (args[k]))
                return k;
        
        return -1;
    }
}
