package utils;

import java.util.List;

import org.antlr.v4.runtime.tree.TerminalNode;

import antlr.coolParser.FormalContext;

public class TranslationUtils
{
    public static enum Visibility 
    {
        PUBLIC,
        PRIVATE,
        PROTECTED,
        EMPTY
    }
    
    public static Visibility STANDARD_VISIBILITY_ATTRIBUTE = Visibility.PROTECTED;
    
    public static Visibility STANDARD_VISIBILITY_METHOD    = Visibility.PROTECTED;
    
    
    public static String getJavaSyntaxOfClass (String className)
    {
        return TranslationUtils.getJavaSyntaxOfClass (className, null, Visibility.EMPTY);
    }
    
    
    public static String getJavaSyntaxOfClass (String className, String classParentName)
    {
        return TranslationUtils.getJavaSyntaxOfClass (className, classParentName, Visibility.EMPTY);
    }
    
    
    public static String getJavaSyntaxOfClass (String className, String classParentName, Visibility visibility)
    {
        return TranslationUtils.getStringVisibility (visibility) + "class " + className + 
               ((classParentName == null) ? "" : " extends " + classParentName);
    }
    
    
    public static String getJavaSyntaxOfAttribute (String attributeName, String attributeType)
    {
        return TranslationUtils.getJavaSyntaxOfAttribute (attributeName, attributeType, TranslationUtils.STANDARD_VISIBILITY_ATTRIBUTE);
    }
    
    
    public static String getJavaSyntaxOfAttribute (String attributeName, String attributeType, Visibility visibility)
    {
        return TranslationUtils.getStringVisibility (visibility) + " " + TranslationUtils.resolveType (attributeType) + " " + attributeName + ";";
    }   
    
    
    public static String getJavaSyntaxOfMethod (String methodName, String methodReturnValue, List<FormalContext> formalParameters)
    {
        return TranslationUtils.getJavaSyntaxOfMethod (methodName, TranslationUtils.resolveType (methodReturnValue), formalParameters, TranslationUtils.STANDARD_VISIBILITY_METHOD, false);
    }
    
    
    public static String getJavaSyntaxOfMethod (String methodName, String methodReturnValue, 
            List<FormalContext> formalParameters, Visibility visibility)
    {
        return TranslationUtils.getJavaSyntaxOfMethod (methodName, TranslationUtils.resolveType (methodReturnValue), formalParameters, visibility, false);
    }
    
    
    public static String getJavaSyntaxOfMethod (String methodName, String methodReturnValue, 
                                                List<FormalContext> formalParameters, Visibility visibility, boolean isStatic)
    {
        String res = TranslationUtils.getStringVisibility (visibility, isStatic) + " ";
        
        res += (methodReturnValue.equals ("") ? "" : methodReturnValue + " ") + methodName + " (";
        
        if (formalParameters != null) {
            for (int k = 0, size = formalParameters.size (); k < size; k++) {
                List<TerminalNode> params = formalParameters.get (k).Identifier ();
                
                String paramName = (params.size () > 0) ? params.get (0).toString () : null;
                String paramType = (params.size () > 1) ? params.get (1).toString () : null;
                
                if (paramName == null)
                    IOUtils.printError ("TranslationsUtils, parameter name is null", true);
                if (paramType == null)
                    IOUtils.printError ("TranslationsUtils, parameter type is null", true);
                
                res += TranslationUtils.resolveType (paramType) + " " + paramName + ((k == size - 1) ? "" : ", ");
            }
        }
        
        return res + ")";
    }
    
    
    public static String getJavaSyntaxOfArguments (List<FormalContext> formalParameters)
    {
        String res = "(";
        
        if (formalParameters != null) {
            for (int k = 0, size = formalParameters.size (); k < size; k++) {
                List<TerminalNode> params = formalParameters.get (k).Identifier ();
                
                String paramName = (params.size () > 0) ? params.get (0).toString () : null;
                String paramType = (params.size () > 1) ? params.get (1).toString () : null;
                
                if (paramName == null)
                    IOUtils.printError ("TranslationsUtils, parameter name is null", true);
                if (paramType == null)
                    IOUtils.printError ("TranslationsUtils, parameter type is null", true);
                
                res += TranslationUtils.resolveType (paramType) + " " + paramName + ((k == size - 1) ? "" : ", ");
            }
        }
        
        return res + ")";
    }
    
    
    public static String getJavaArgumentsOfMainMethod ()
    {
        return "(java.lang.String[] args)";
    }
    
    
    public static String getJavaSyntaxOfComment (String comment)
    {
        return "//" + comment;
    }
    
    
    public static String getStringVisibility (Visibility visibility)
    {
        return TranslationUtils.getStringVisibility (visibility, false);
    }
    
    
    @SuppressWarnings("incomplete-switch")
    public static String getStringVisibility (Visibility visibility, boolean isStatic)
    {
        String ret = "";
        
        switch (visibility) {
        case PUBLIC:    ret = "public";    break;
        case PRIVATE:   ret = "private";   break;
        case PROTECTED: ret = "protected"; break;
        }
        
        return ret + (isStatic ? " static" : "");
    }
    
    
    public static String resolveType (String type)
    {
        return TranslationUtils.resolveType (type, false);
    }
    
    
    public static String resolveType (String type, boolean isNew)
    {
        if (type.equals ("Int"))
            return "Integer";
        else if (type.equals ("Bool"))
            return "Boolean";
        else if (type.equals ("Object"))
            return isNew ? "Object" : "java.lang.Object";
        
        return type;
    }
    
    
    public static String addDefaultConstructor (String type)
    {
        if (type.equals ("Integer"))
            return "new Integer (0)";
        else if (type.equals ("Boolean"))
            return "new Boolean (false)";
        
        return "new " + type + " ()";
    }
}
