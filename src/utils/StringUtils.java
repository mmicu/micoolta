package utils;

import java.util.Arrays;

public class StringUtils
{
    // Thanks to:
    // http://grepcode.com/file_/repo1.maven.org/maven2/org.apache.axis2/axis2-kernel/1.4/org/apache/axis2/util/JavaUtils.java/?v=source
    public static final String keywords[] =
    {
        "abstract", "assert", "boolean", "break", "byte", "case",
        "catch", "char", "class", "const", "continue",
        "default", "do", "double", "else", "extends",
        "false", "final", "finally", "float", "for",
        "goto", "if", "implements", "import", "instanceof",
        "int", "interface", "long", "native", "new",
        "null", "package", "private", "protected", "public",
        "return", "short", "static", "strictfp", "super",
        "switch", "synchronized", "this", "throw", "throws",
        "transient", "true", "try", "void", "volatile",
        "while"
    };
    
    
    public static boolean isUpperCaseString (String s)
    {
        if (s == null)
            return false;
        
        for (int k = 0, size = s.length (); k < size; k++)
            if (!(s.charAt (k) >= 'A' && s.charAt (k) <= 'Z'))
                return false;
        
        return true;
    }
    
    
    public static boolean isLowerCaseString (String s)
    {
        if (s == null)
            return false;
        
        for (int k = 0, size = s.length (); k < size; k++)
            if (!(s.charAt (k) >= 'a' && s.charAt (k) <= 'z'))
                return false;
        
        return true;
    }
    
    
    public static boolean isUpperCaseChar (char c)
    {
        return StringUtils.isUpperCaseString (String.valueOf (c));
    }
    
    
    public static boolean isLowerCaseChar (char c)
    {
        return StringUtils.isLowerCaseString (String.valueOf (c));
    }
    
    
    public static boolean isNumber (String s)
    {
        return s != null && s.matches ("[0-9]+");
    }
    
    
    public static boolean isIdentifier (String s)
    {
        return s != null && s.matches ("[A-Za-z][A-Za-z0-9_]*");
    }
    
    
    public static String repeat (String s, int times)
    {
        if (times <= 0)
            return "";
        
        String app = s;
        
        for (int k = 0; k < times; k++)
            app += s;
        
        return app;
    }
    
    
    // Thanks to:
    // http://grepcode.com/file_/repo1.maven.org/maven2/org.apache.axis2/axis2-kernel/1.4/org/apache/axis2/util/JavaUtils.java/?v=source
    public static boolean isJavaKeyword (String keyword) 
    {
        return Arrays.binarySearch (StringUtils.keywords, keyword) >= 0;
    }
}
