// Generated from cool.g4 by ANTLR 4.4
package antlr;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link coolParser}.
 */
public interface coolListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link coolParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull coolParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link coolParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull coolParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link coolParser#formal}.
	 * @param ctx the parse tree
	 */
	void enterFormal(@NotNull coolParser.FormalContext ctx);
	/**
	 * Exit a parse tree produced by {@link coolParser#formal}.
	 * @param ctx the parse tree
	 */
	void exitFormal(@NotNull coolParser.FormalContext ctx);
	/**
	 * Enter a parse tree produced by {@link coolParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(@NotNull coolParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link coolParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(@NotNull coolParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link coolParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(@NotNull coolParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link coolParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(@NotNull coolParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link coolParser#feature}.
	 * @param ctx the parse tree
	 */
	void enterFeature(@NotNull coolParser.FeatureContext ctx);
	/**
	 * Exit a parse tree produced by {@link coolParser#feature}.
	 * @param ctx the parse tree
	 */
	void exitFeature(@NotNull coolParser.FeatureContext ctx);
	/**
	 * Enter a parse tree produced by {@link coolParser#attributeDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterAttributeDeclaration(@NotNull coolParser.AttributeDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link coolParser#attributeDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitAttributeDeclaration(@NotNull coolParser.AttributeDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link coolParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterMethodDeclaration(@NotNull coolParser.MethodDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link coolParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitMethodDeclaration(@NotNull coolParser.MethodDeclarationContext ctx);
}