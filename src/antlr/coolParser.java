// Generated from cool.g4 by ANTLR 4.4
package antlr;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class coolParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__26=1, T__25=2, T__24=3, T__23=4, T__22=5, T__21=6, T__20=7, T__19=8, 
		T__18=9, T__17=10, T__16=11, T__15=12, T__14=13, T__13=14, T__12=15, T__11=16, 
		T__10=17, T__9=18, T__8=19, T__7=20, T__6=21, T__5=22, T__4=23, T__3=24, 
		T__2=25, T__1=26, T__0=27, CASE=28, CLASS=29, ESAC=30, ELSE=31, FALSE=32, 
		FI=33, IF=34, IN=35, INHERITS=36, ISVOID=37, LET=38, LOOP=39, NEW=40, 
		NOT=41, OF=42, POOL=43, THEN=44, TRUE=45, WHILE=46, Identifier=47, Integer=48, 
		String=49, True=50, False=51, Whitespace=52, Newline=53, BlockComment=54, 
		LineComment=55;
	public static final String[] tokenNames = {
		"<INVALID>", "'loop'", "'.'", "')'", "'in'", "'=>'", "','", "'of'", "'+'", 
		"'*'", "'-'", "'@'", "':'", "'('", "'esac'", "'fi'", "'<'", "'='", "';'", 
		"'<='", "'{'", "'then'", "'else'", "'/'", "'~'", "'}'", "'<-'", "'pool'", 
		"CASE", "CLASS", "ESAC", "ELSE", "FALSE", "FI", "IF", "IN", "INHERITS", 
		"ISVOID", "LET", "LOOP", "NEW", "NOT", "OF", "POOL", "THEN", "TRUE", "WHILE", 
		"Identifier", "Integer", "String", "True", "False", "Whitespace", "Newline", 
		"BlockComment", "LineComment"
	};
	public static final int
		RULE_program = 0, RULE_classDeclaration = 1, RULE_feature = 2, RULE_methodDeclaration = 3, 
		RULE_attributeDeclaration = 4, RULE_formal = 5, RULE_expression = 6;
	public static final String[] ruleNames = {
		"program", "classDeclaration", "feature", "methodDeclaration", "attributeDeclaration", 
		"formal", "expression"
	};

	@Override
	public String getGrammarFileName() { return "cool.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public coolParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public ClassDeclarationContext classDeclaration(int i) {
			return getRuleContext(ClassDeclarationContext.class,i);
		}
		public List<ClassDeclarationContext> classDeclaration() {
			return getRuleContexts(ClassDeclarationContext.class);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).exitProgram(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(17); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(14); classDeclaration();
				setState(15); match(T__9);
				}
				}
				setState(19); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==CLASS );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClassDeclarationContext extends ParserRuleContext {
		public List<FeatureContext> feature() {
			return getRuleContexts(FeatureContext.class);
		}
		public FeatureContext feature(int i) {
			return getRuleContext(FeatureContext.class,i);
		}
		public TerminalNode Identifier(int i) {
			return getToken(coolParser.Identifier, i);
		}
		public List<TerminalNode> Identifier() { return getTokens(coolParser.Identifier); }
		public TerminalNode INHERITS() { return getToken(coolParser.INHERITS, 0); }
		public TerminalNode CLASS() { return getToken(coolParser.CLASS, 0); }
		public ClassDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_classDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).enterClassDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).exitClassDeclaration(this);
		}
	}

	public final ClassDeclarationContext classDeclaration() throws RecognitionException {
		ClassDeclarationContext _localctx = new ClassDeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_classDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(21); match(CLASS);
			setState(22); match(Identifier);
			setState(25);
			_la = _input.LA(1);
			if (_la==INHERITS) {
				{
				setState(23); match(INHERITS);
				setState(24); match(Identifier);
				}
			}

			setState(27); match(T__7);
			setState(33);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Identifier) {
				{
				{
				setState(28); feature();
				setState(29); match(T__9);
				}
				}
				setState(35);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(36); match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FeatureContext extends ParserRuleContext {
		public AttributeDeclarationContext attributeDeclaration() {
			return getRuleContext(AttributeDeclarationContext.class,0);
		}
		public MethodDeclarationContext methodDeclaration() {
			return getRuleContext(MethodDeclarationContext.class,0);
		}
		public FeatureContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_feature; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).enterFeature(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).exitFeature(this);
		}
	}

	public final FeatureContext feature() throws RecognitionException {
		FeatureContext _localctx = new FeatureContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_feature);
		try {
			setState(40);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(38); methodDeclaration();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(39); attributeDeclaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodDeclarationContext extends ParserRuleContext {
		public FormalContext formal(int i) {
			return getRuleContext(FormalContext.class,i);
		}
		public List<FormalContext> formal() {
			return getRuleContexts(FormalContext.class);
		}
		public TerminalNode Identifier(int i) {
			return getToken(coolParser.Identifier, i);
		}
		public List<TerminalNode> Identifier() { return getTokens(coolParser.Identifier); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public MethodDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methodDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).enterMethodDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).exitMethodDeclaration(this);
		}
	}

	public final MethodDeclarationContext methodDeclaration() throws RecognitionException {
		MethodDeclarationContext _localctx = new MethodDeclarationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_methodDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42); match(Identifier);
			setState(43); match(T__14);
			setState(52);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
				setState(44); formal();
				setState(49);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__21) {
					{
					{
					setState(45); match(T__21);
					setState(46); formal();
					}
					}
					setState(51);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(54); match(T__24);
			setState(55); match(T__15);
			setState(56); match(Identifier);
			setState(57); match(T__7);
			setState(58); expression(0);
			setState(59); match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeDeclarationContext extends ParserRuleContext {
		public TerminalNode Identifier(int i) {
			return getToken(coolParser.Identifier, i);
		}
		public List<TerminalNode> Identifier() { return getTokens(coolParser.Identifier); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AttributeDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).enterAttributeDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).exitAttributeDeclaration(this);
		}
	}

	public final AttributeDeclarationContext attributeDeclaration() throws RecognitionException {
		AttributeDeclarationContext _localctx = new AttributeDeclarationContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_attributeDeclaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61); match(Identifier);
			setState(62); match(T__15);
			setState(63); match(Identifier);
			setState(66);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(64); match(T__1);
				setState(65); expression(0);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalContext extends ParserRuleContext {
		public TerminalNode Identifier(int i) {
			return getToken(coolParser.Identifier, i);
		}
		public List<TerminalNode> Identifier() { return getTokens(coolParser.Identifier); }
		public FormalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).enterFormal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).exitFormal(this);
		}
	}

	public final FormalContext formal() throws RecognitionException {
		FormalContext _localctx = new FormalContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_formal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68); match(Identifier);
			setState(69); match(T__15);
			setState(70); match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public TerminalNode String() { return getToken(coolParser.String, 0); }
		public TerminalNode Integer() { return getToken(coolParser.Integer, 0); }
		public TerminalNode ISVOID() { return getToken(coolParser.ISVOID, 0); }
		public TerminalNode TRUE() { return getToken(coolParser.TRUE, 0); }
		public List<TerminalNode> Identifier() { return getTokens(coolParser.Identifier); }
		public TerminalNode Identifier(int i) {
			return getToken(coolParser.Identifier, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public TerminalNode WHILE() { return getToken(coolParser.WHILE, 0); }
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode IF() { return getToken(coolParser.IF, 0); }
		public TerminalNode NEW() { return getToken(coolParser.NEW, 0); }
		public TerminalNode LET() { return getToken(coolParser.LET, 0); }
		public TerminalNode NOT() { return getToken(coolParser.NOT, 0); }
		public TerminalNode CASE() { return getToken(coolParser.CASE, 0); }
		public TerminalNode FALSE() { return getToken(coolParser.FALSE, 0); }
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof coolListener ) ((coolListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		return expression(0);
	}

	private ExpressionContext expression(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExpressionContext _localctx = new ExpressionContext(_ctx, _parentState);
		ExpressionContext _prevctx = _localctx;
		int _startState = 12;
		enterRecursionRule(_localctx, 12, RULE_expression, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(169);
			switch ( getInterpreter().adaptivePredict(_input,14,_ctx) ) {
			case 1:
				{
				setState(73); match(LET);
				setState(74); match(Identifier);
				setState(75); match(T__15);
				setState(76); match(Identifier);
				setState(79);
				_la = _input.LA(1);
				if (_la==T__1) {
					{
					setState(77); match(T__1);
					setState(78); expression(0);
					}
				}

				setState(91);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__21) {
					{
					{
					setState(81); match(T__21);
					setState(82); match(Identifier);
					setState(83); match(T__15);
					setState(84); match(Identifier);
					setState(87);
					_la = _input.LA(1);
					if (_la==T__1) {
						{
						setState(85); match(T__1);
						setState(86); expression(0);
						}
					}

					}
					}
					setState(93);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(94); match(T__23);
				setState(95); expression(16);
				}
				break;
			case 2:
				{
				setState(96); match(T__3);
				setState(97); expression(13);
				}
				break;
			case 3:
				{
				setState(98); match(ISVOID);
				setState(99); expression(12);
				}
				break;
			case 4:
				{
				setState(100); match(NOT);
				setState(101); expression(8);
				}
				break;
			case 5:
				{
				setState(102); match(Identifier);
				setState(103); match(T__1);
				setState(104); expression(6);
				}
				break;
			case 6:
				{
				setState(105); match(Identifier);
				setState(106); match(T__14);
				setState(115);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__7) | (1L << T__3) | (1L << CASE) | (1L << FALSE) | (1L << IF) | (1L << ISVOID) | (1L << LET) | (1L << NEW) | (1L << NOT) | (1L << TRUE) | (1L << WHILE) | (1L << Identifier) | (1L << Integer) | (1L << String))) != 0)) {
					{
					setState(107); expression(0);
					setState(112);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__21) {
						{
						{
						setState(108); match(T__21);
						setState(109); expression(0);
						}
						}
						setState(114);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(117); match(T__24);
				}
				break;
			case 7:
				{
				setState(118); match(IF);
				setState(119); expression(0);
				setState(120); match(T__6);
				setState(121); expression(0);
				setState(122); match(T__5);
				setState(123); expression(0);
				setState(124); match(T__12);
				}
				break;
			case 8:
				{
				setState(126); match(WHILE);
				setState(127); expression(0);
				setState(128); match(T__26);
				setState(129); expression(0);
				setState(130); match(T__0);
				}
				break;
			case 9:
				{
				setState(132); match(T__7);
				setState(136); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(133); expression(0);
					setState(134); match(T__9);
					}
					}
					setState(138); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__7) | (1L << T__3) | (1L << CASE) | (1L << FALSE) | (1L << IF) | (1L << ISVOID) | (1L << LET) | (1L << NEW) | (1L << NOT) | (1L << TRUE) | (1L << WHILE) | (1L << Identifier) | (1L << Integer) | (1L << String))) != 0) );
				setState(140); match(T__2);
				}
				break;
			case 10:
				{
				setState(142); match(CASE);
				setState(143); expression(0);
				setState(144); match(T__20);
				setState(152); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(145); match(Identifier);
					setState(146); match(T__15);
					setState(147); match(Identifier);
					setState(148); match(T__22);
					setState(149); expression(0);
					setState(150); match(T__9);
					}
					}
					setState(154); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==Identifier );
				setState(156); match(T__13);
				}
				break;
			case 11:
				{
				setState(158); match(NEW);
				setState(159); match(Identifier);
				}
				break;
			case 12:
				{
				setState(160); match(T__14);
				setState(161); expression(0);
				setState(162); match(T__24);
				}
				break;
			case 13:
				{
				setState(164); match(TRUE);
				}
				break;
			case 14:
				{
				setState(165); match(FALSE);
				}
				break;
			case 15:
				{
				setState(166); match(Identifier);
				}
				break;
			case 16:
				{
				setState(167); match(Integer);
				}
				break;
			case 17:
				{
				setState(168); match(String);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(201);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(199);
					switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
					case 1:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(171);
						if (!(precpred(_ctx, 11))) throw new FailedPredicateException(this, "precpred(_ctx, 11)");
						setState(172);
						_la = _input.LA(1);
						if ( !(_la==T__18 || _la==T__4) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(173); expression(12);
						}
						break;
					case 2:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(174);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(175);
						_la = _input.LA(1);
						if ( !(_la==T__19 || _la==T__17) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(176); expression(11);
						}
						break;
					case 3:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(177);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(178);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__10) | (1L << T__8))) != 0)) ) {
						_errHandler.recoverInline(this);
						}
						consume();
						setState(179); expression(10);
						}
						break;
					case 4:
						{
						_localctx = new ExpressionContext(_parentctx, _parentState);
						pushNewRecursionContext(_localctx, _startState, RULE_expression);
						setState(180);
						if (!(precpred(_ctx, 21))) throw new FailedPredicateException(this, "precpred(_ctx, 21)");
						setState(183);
						_la = _input.LA(1);
						if (_la==T__16) {
							{
							setState(181); match(T__16);
							setState(182); match(Identifier);
							}
						}

						setState(185); match(T__25);
						setState(186); match(Identifier);
						setState(187); match(T__14);
						setState(196);
						_la = _input.LA(1);
						if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__7) | (1L << T__3) | (1L << CASE) | (1L << FALSE) | (1L << IF) | (1L << ISVOID) | (1L << LET) | (1L << NEW) | (1L << NOT) | (1L << TRUE) | (1L << WHILE) | (1L << Identifier) | (1L << Integer) | (1L << String))) != 0)) {
							{
							setState(188); expression(0);
							setState(193);
							_errHandler.sync(this);
							_la = _input.LA(1);
							while (_la==T__21) {
								{
								{
								setState(189); match(T__21);
								setState(190); expression(0);
								}
								}
								setState(195);
								_errHandler.sync(this);
								_la = _input.LA(1);
							}
							}
						}

						setState(198); match(T__24);
						}
						break;
					}
					} 
				}
				setState(203);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,19,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 6: return expression_sempred((ExpressionContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expression_sempred(ExpressionContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return precpred(_ctx, 11);
		case 1: return precpred(_ctx, 10);
		case 2: return precpred(_ctx, 9);
		case 3: return precpred(_ctx, 21);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\39\u00cf\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\3\2\3\2\6\2\24\n\2"+
		"\r\2\16\2\25\3\3\3\3\3\3\3\3\5\3\34\n\3\3\3\3\3\3\3\3\3\7\3\"\n\3\f\3"+
		"\16\3%\13\3\3\3\3\3\3\4\3\4\5\4+\n\4\3\5\3\5\3\5\3\5\3\5\7\5\62\n\5\f"+
		"\5\16\5\65\13\5\5\5\67\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6"+
		"\3\6\5\6E\n\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\bR\n\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\5\bZ\n\b\7\b\\\n\b\f\b\16\b_\13\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\bq\n\b\f\b\16\bt\13"+
		"\b\5\bv\n\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\6\b\u008b\n\b\r\b\16\b\u008c\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\6\b\u009b\n\b\r\b\16\b\u009c\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00ac\n\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00ba\n\b\3\b\3\b\3\b\3\b\3\b\3\b\7"+
		"\b\u00c2\n\b\f\b\16\b\u00c5\13\b\5\b\u00c7\n\b\3\b\7\b\u00ca\n\b\f\b\16"+
		"\b\u00cd\13\b\3\b\2\3\16\t\2\4\6\b\n\f\16\2\5\4\2\13\13\31\31\4\2\n\n"+
		"\f\f\4\2\22\23\25\25\u00ec\2\23\3\2\2\2\4\27\3\2\2\2\6*\3\2\2\2\b,\3\2"+
		"\2\2\n?\3\2\2\2\fF\3\2\2\2\16\u00ab\3\2\2\2\20\21\5\4\3\2\21\22\7\24\2"+
		"\2\22\24\3\2\2\2\23\20\3\2\2\2\24\25\3\2\2\2\25\23\3\2\2\2\25\26\3\2\2"+
		"\2\26\3\3\2\2\2\27\30\7\37\2\2\30\33\7\61\2\2\31\32\7&\2\2\32\34\7\61"+
		"\2\2\33\31\3\2\2\2\33\34\3\2\2\2\34\35\3\2\2\2\35#\7\26\2\2\36\37\5\6"+
		"\4\2\37 \7\24\2\2 \"\3\2\2\2!\36\3\2\2\2\"%\3\2\2\2#!\3\2\2\2#$\3\2\2"+
		"\2$&\3\2\2\2%#\3\2\2\2&\'\7\33\2\2\'\5\3\2\2\2(+\5\b\5\2)+\5\n\6\2*(\3"+
		"\2\2\2*)\3\2\2\2+\7\3\2\2\2,-\7\61\2\2-\66\7\17\2\2.\63\5\f\7\2/\60\7"+
		"\b\2\2\60\62\5\f\7\2\61/\3\2\2\2\62\65\3\2\2\2\63\61\3\2\2\2\63\64\3\2"+
		"\2\2\64\67\3\2\2\2\65\63\3\2\2\2\66.\3\2\2\2\66\67\3\2\2\2\678\3\2\2\2"+
		"89\7\5\2\29:\7\16\2\2:;\7\61\2\2;<\7\26\2\2<=\5\16\b\2=>\7\33\2\2>\t\3"+
		"\2\2\2?@\7\61\2\2@A\7\16\2\2AD\7\61\2\2BC\7\34\2\2CE\5\16\b\2DB\3\2\2"+
		"\2DE\3\2\2\2E\13\3\2\2\2FG\7\61\2\2GH\7\16\2\2HI\7\61\2\2I\r\3\2\2\2J"+
		"K\b\b\1\2KL\7(\2\2LM\7\61\2\2MN\7\16\2\2NQ\7\61\2\2OP\7\34\2\2PR\5\16"+
		"\b\2QO\3\2\2\2QR\3\2\2\2R]\3\2\2\2ST\7\b\2\2TU\7\61\2\2UV\7\16\2\2VY\7"+
		"\61\2\2WX\7\34\2\2XZ\5\16\b\2YW\3\2\2\2YZ\3\2\2\2Z\\\3\2\2\2[S\3\2\2\2"+
		"\\_\3\2\2\2][\3\2\2\2]^\3\2\2\2^`\3\2\2\2_]\3\2\2\2`a\7\6\2\2a\u00ac\5"+
		"\16\b\22bc\7\32\2\2c\u00ac\5\16\b\17de\7\'\2\2e\u00ac\5\16\b\16fg\7+\2"+
		"\2g\u00ac\5\16\b\nhi\7\61\2\2ij\7\34\2\2j\u00ac\5\16\b\bkl\7\61\2\2lu"+
		"\7\17\2\2mr\5\16\b\2no\7\b\2\2oq\5\16\b\2pn\3\2\2\2qt\3\2\2\2rp\3\2\2"+
		"\2rs\3\2\2\2sv\3\2\2\2tr\3\2\2\2um\3\2\2\2uv\3\2\2\2vw\3\2\2\2w\u00ac"+
		"\7\5\2\2xy\7$\2\2yz\5\16\b\2z{\7\27\2\2{|\5\16\b\2|}\7\30\2\2}~\5\16\b"+
		"\2~\177\7\21\2\2\177\u00ac\3\2\2\2\u0080\u0081\7\60\2\2\u0081\u0082\5"+
		"\16\b\2\u0082\u0083\7\3\2\2\u0083\u0084\5\16\b\2\u0084\u0085\7\35\2\2"+
		"\u0085\u00ac\3\2\2\2\u0086\u008a\7\26\2\2\u0087\u0088\5\16\b\2\u0088\u0089"+
		"\7\24\2\2\u0089\u008b\3\2\2\2\u008a\u0087\3\2\2\2\u008b\u008c\3\2\2\2"+
		"\u008c\u008a\3\2\2\2\u008c\u008d\3\2\2\2\u008d\u008e\3\2\2\2\u008e\u008f"+
		"\7\33\2\2\u008f\u00ac\3\2\2\2\u0090\u0091\7\36\2\2\u0091\u0092\5\16\b"+
		"\2\u0092\u009a\7\t\2\2\u0093\u0094\7\61\2\2\u0094\u0095\7\16\2\2\u0095"+
		"\u0096\7\61\2\2\u0096\u0097\7\7\2\2\u0097\u0098\5\16\b\2\u0098\u0099\7"+
		"\24\2\2\u0099\u009b\3\2\2\2\u009a\u0093\3\2\2\2\u009b\u009c\3\2\2\2\u009c"+
		"\u009a\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u009e\3\2\2\2\u009e\u009f\7\20"+
		"\2\2\u009f\u00ac\3\2\2\2\u00a0\u00a1\7*\2\2\u00a1\u00ac\7\61\2\2\u00a2"+
		"\u00a3\7\17\2\2\u00a3\u00a4\5\16\b\2\u00a4\u00a5\7\5\2\2\u00a5\u00ac\3"+
		"\2\2\2\u00a6\u00ac\7/\2\2\u00a7\u00ac\7\"\2\2\u00a8\u00ac\7\61\2\2\u00a9"+
		"\u00ac\7\62\2\2\u00aa\u00ac\7\63\2\2\u00abJ\3\2\2\2\u00abb\3\2\2\2\u00ab"+
		"d\3\2\2\2\u00abf\3\2\2\2\u00abh\3\2\2\2\u00abk\3\2\2\2\u00abx\3\2\2\2"+
		"\u00ab\u0080\3\2\2\2\u00ab\u0086\3\2\2\2\u00ab\u0090\3\2\2\2\u00ab\u00a0"+
		"\3\2\2\2\u00ab\u00a2\3\2\2\2\u00ab\u00a6\3\2\2\2\u00ab\u00a7\3\2\2\2\u00ab"+
		"\u00a8\3\2\2\2\u00ab\u00a9\3\2\2\2\u00ab\u00aa\3\2\2\2\u00ac\u00cb\3\2"+
		"\2\2\u00ad\u00ae\f\r\2\2\u00ae\u00af\t\2\2\2\u00af\u00ca\5\16\b\16\u00b0"+
		"\u00b1\f\f\2\2\u00b1\u00b2\t\3\2\2\u00b2\u00ca\5\16\b\r\u00b3\u00b4\f"+
		"\13\2\2\u00b4\u00b5\t\4\2\2\u00b5\u00ca\5\16\b\f\u00b6\u00b9\f\27\2\2"+
		"\u00b7\u00b8\7\r\2\2\u00b8\u00ba\7\61\2\2\u00b9\u00b7\3\2\2\2\u00b9\u00ba"+
		"\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00bc\7\4\2\2\u00bc\u00bd\7\61\2\2"+
		"\u00bd\u00c6\7\17\2\2\u00be\u00c3\5\16\b\2\u00bf\u00c0\7\b\2\2\u00c0\u00c2"+
		"\5\16\b\2\u00c1\u00bf\3\2\2\2\u00c2\u00c5\3\2\2\2\u00c3\u00c1\3\2\2\2"+
		"\u00c3\u00c4\3\2\2\2\u00c4\u00c7\3\2\2\2\u00c5\u00c3\3\2\2\2\u00c6\u00be"+
		"\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8\u00ca\7\5\2\2\u00c9"+
		"\u00ad\3\2\2\2\u00c9\u00b0\3\2\2\2\u00c9\u00b3\3\2\2\2\u00c9\u00b6\3\2"+
		"\2\2\u00ca\u00cd\3\2\2\2\u00cb\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc"+
		"\17\3\2\2\2\u00cd\u00cb\3\2\2\2\26\25\33#*\63\66DQY]ru\u008c\u009c\u00ab"+
		"\u00b9\u00c3\u00c6\u00c9\u00cb";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}