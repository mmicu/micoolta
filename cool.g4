grammar cool;

program                     :       (classDeclaration ';')+;

classDeclaration            :       CLASS Identifier (INHERITS Identifier)? '{' (feature ';')* '}';

feature                     :       methodDeclaration | attributeDeclaration;

methodDeclaration           :       Identifier '(' (formal (',' formal)*)? ')' ':' Identifier '{' expression '}';

attributeDeclaration        :       Identifier ':' Identifier ('<-' expression)?;

formal                      :       Identifier ':' Identifier;

/*
Precedence (highest to lowest)
.
@
~
isvoid
*   /
+   -
<=  <   =
not
<- (right-associative)

All binary operations are left-associative, with the exception of assignment (<-), which is right-associative and the
three comparison operations, which do not associate
*/
expression                  :       expression ('@' Identifier)? '.' Identifier '(' (expression (',' expression)*)? ')'
                                    |
                                    Identifier '(' (expression (',' expression)*)? ')'
                                    |
                                    IF expression 'then' expression 'else' expression 'fi'
                                    |
                                    WHILE expression 'loop' expression 'pool'
                                    |
                                    '{' (expression ';')+ '}'
                                    |
                                    LET Identifier ':' Identifier ('<-' expression)? (',' Identifier ':' Identifier ('<-' expression)?)* 'in' expression 
                                    |
                                    CASE expression 'of' (Identifier ':' Identifier '=>' expression ';')+ 'esac'
                                    |
                                    NEW Identifier
                                    |
                                    '~' expression
                                    |
                                    ISVOID expression
                                    |
                                    expression ('*' | '/') expression
                                    |
                                    expression ('+' | '-') expression
                                    |
                                    expression ('<=' | '<' | '=') expression
                                    |
                                    NOT expression
                                    |
                                    '(' expression ')'
                                    |
                                    <assoc=right> Identifier '<-' expression
                                    |
                                    TRUE
                                    |
                                    FALSE
                                    |
                                    Identifier
                                    |
                                    Integer
                                    |
                                    String;


// Keywords
CASE        : C A S E;
CLASS       : C L A S S;
ESAC        : E S A C;
ELSE        : E L S E;
FALSE       : 'f' A L S E;
FI          : F I;
IF          : I F;
IN          : I N;
INHERITS    : I N H E R I T S;
ISVOID      : I S V O I D;
LET         : L E T;
LOOP        : L O O P;
NEW         : N E W;
NOT         : N O T;
OF          : O F;
POOL        : P O O L;
THEN        : T H E N;
TRUE        : 't' R U E;
WHILE       : W H I L E;

fragment A: ('a'|'A');
fragment B: ('b'|'B');
fragment C: ('c'|'C');
fragment D: ('d'|'D');
fragment E: ('e'|'E');
fragment F: ('f'|'F');
fragment G: ('g'|'G');
fragment H: ('h'|'H');
fragment I: ('i'|'I');
fragment J: ('j'|'J');
fragment K: ('k'|'K');
fragment L: ('l'|'L');
fragment M: ('m'|'M');
fragment N: ('n'|'N');
fragment O: ('o'|'O');
fragment P: ('p'|'P');
fragment Q: ('q'|'Q');
fragment R: ('r'|'R');
fragment S: ('s'|'S');
fragment T: ('t'|'T');
fragment U: ('u'|'U');
fragment V: ('v'|'V');
fragment W: ('w'|'W');
fragment X: ('x'|'X');
fragment Y: ('y'|'Y');
fragment Z: ('z'|'Z');


//

Identifier      :   Letter LetterOrDigit*;

fragment
Letter          :   [a-zA-Z$_];

fragment
LetterOrDigit   :   [a-zA-Z0-9$_];

Integer         :   Digit+;

fragment
Digit           :   [0-9];

fragment 
ESCAPED_QUOTE   :   '\\"';

String          :   '"' ( ESCAPED_QUOTE | ~('\n'|'\r') )*? '"';

True            :   't'('R'|'r')('U'|'u')('E'|'e');

False           :   'f'('A'|'a')('L'|'l')('S'|'s')('E'|'e');

Whitespace      :   [ \t]+ -> skip;

Newline         :   (   '\r' '\n'? |   '\n') -> skip;

BlockComment    :   '(*' .*? '*)' -> skip;

LineComment     :   '--' ~[\r\n]* -> skip;
