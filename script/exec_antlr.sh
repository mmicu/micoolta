#!/bin/bash

ANT="/opt/local/share/java/apache-ant/bin/ant"

export CLASSPATH=".:/usr/local/lib/antlr-4.4-complete.jar:$CLASSPATH"
alias antlr4='java -jar /usr/local/lib/antlr-4.4-complete.jar'
alias grun='java org.antlr.v4.runtime.misc.TestRig'

compile_grammar ()
{
    echo "$ Executing antlr4"
    antlr4 -o ./src/antlr -package "antlr" cool.g4
}

compile_java_file ()
{
    echo "$ Executing ant"
    $ANT clean
    $ANT compile jar
}

compile_grammar
compile_java_file
