#!/bin/bash

OUTPUT_FILE=Examples
COOL_FILE=cells.cl

clear

echo "$ ant compile jar"
ant compile jar

echo "\n$  java -jar build/jar/micoolta.jar -o src/test/output/$OUTPUT_FILE.java examples/cool/$COOL_FILE"
java -jar build/jar/micoolta.jar -o src/test/output/$OUTPUT_FILE.java examples/cool-originals-examples/$COOL_FILE -package test.output
