#!/bin/bash

COUNTER=0
FILE_PREFIX="Class"
PACKAGE_PREFIX="p"

clear

echo "$ ant compile jar"
ant compile jar

for i in $(ls examples/examples-with-errors/*.cl); do
#for i in $(ls examples/cool/*.cl); do
	echo "\n$  java -jar build/jar/micoolta.jar -o src/test/output/$FILE_PREFIX$COUNTER.java $i -package test.output.$PACKAGE_PREFIX$COUNTER"
	java -jar build/jar/micoolta.jar -o src/test/output/$PACKAGE_PREFIX$COUNTER/$FILE_PREFIX$COUNTER.java $i -package test.output.$PACKAGE_PREFIX$COUNTER
	
	COUNTER=$[$COUNTER + 1]
done;
