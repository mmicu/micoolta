#!/bin/bash

export CLASSPATH=".:/usr/local/lib/antlr-4.4-complete.jar:$CLASSPATH"
alias antlr4='java -jar /usr/local/lib/antlr-4.4-complete.jar'
alias grun='java org.antlr.v4.runtime.misc.TestRig'

compile_grammar ()
{
    echo "$ Executing antlr4"
    cp cool.g4 build/antlr/cool.g4 && cd build/antlr && antlr4 cool.g4
}

compile_java_file ()
{
    echo "$ Compile java file"
    javac *.java
}

grun_tree ()
{
    echo "$ Executing grun"
    grun cool program -tree -gui ../../examples/cool/hello-world.cl
    rm cool.g4
}

clear
compile_grammar
compile_java_file
grun_tree
