class Arith {
	add (a : Int, b : Int) : Int {
		a + b
	};
	
	sub (a : Int, b : Int) : Int {
		a - b
	};
	
	mul (a : Int, b : Int) : Int {
		a * b
	};
	
	div (a : Int, b : Int) : Int {
		a / b
	};
};
