class Main inherits IO {
	main () : Object {
		{
			let a : Int, a : Int<-12, b : Int, flag : Bool <- true, char_ : String, operator : String in
			{
				while (flag) loop
					{
						out_string ("Insert first number:  ");
						a <- in_int ();
						out_string ("Insert operator (+, -, *, /):  ");
						operator <- in_string ();
						out_string ("Insert second number:  ");
						b <- in_int ();
						
						if operator = "+" then
							out_int (new Arith.add (a, b))
						else
							if operator = "-" then
								out_int (new Arith.sub (a, b))
							else
								if operator = "*" then
									out_int (new Arith.mul (a, b))
								else
									if operator = "/" then
										out_int (new Arith.div (a, b))
									else
										{
											out_string ("\nUnknown operator!\n");
											abort ();
										}
									fi
								fi
							fi
						fi;
						
						out_string ("\nQuit ('y' to quit)? ");
						char_ <- in_string ();
						
						if char_ = "y" then
							{
								flag <- false;
								out_string ("\nQuit!\n");
							}
						else
							flag <- true
						fi;
					}
				pool;
			};
		}
	};
};
