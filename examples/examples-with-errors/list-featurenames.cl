(*
 *  This file shows how to implement a list data type for lists of integers.
 *  It makes use of INHERITANCE and DYNAMIC DISPATCH.
 *
 *  The List class has 4 operations defined on List objects. If 'l' is
 *  a list, then the methods dispatched on 'l' have the following effects:
 *
 *    isNil() : Bool		Returns true if 'l' is empty, false otherwise.
 *    head()  : Int		Returns the integer at the head of 'l'.
 *				If 'l' is empty, execution aborts.
 *    tail()  : List		Returns the remainder of the 'l',
 *				i.e. without the first element.
 *    cons(i : Int) : List	Return a new list containing i as the
 *				first element, followed by the
 *				elements in 'l'.
 *
 *  There are 2 kinds of lists, the empty list and a non-empty
 *  list. We can think of the non-empty list as a specialization of
 *  the empty list.
 *  The class List defines the operations on empty list. The class
 *  Cons inherits from List and redefines things to handle non-empty
 *  lists.
 *  FEATURE NAMES DON'T  BEGIN WITH LOWERCASE LETTER.
 *)
 
 
 class List {
   
   isNil() : Bool { true };
   
   head()  : Int { { abort(); 0; } };
   
   tail()  : List { { abort(); self; } };
   
   cons(I : Int) : List {
      (new Cons).init(I, self)
   };

};



class Cons inherits List {

   Car : Int;	

   Cdr : List;	

   isNil() : Bool { false };

   head()  : Int { Car };

   tail()  : List { Cdr };

   init(I : Int, Rest : List) : List {
      {
	 Car <- I;
	 Cdr <- Rest;
	 self;
      }
   };

};



class Main inherits IO {

   Mylist : List;

   print_list(L : List) : Object {
      if L.isNil() then out_string("\n")
                   else {
			   out_int(L.head());
			   out_string(" ");
			   print_list(L.tail());
		        }
      fi
   };

   
   main() : Object {
      {
	 Mylist <- new List.cons(1).cons(2).cons(3).cons(4).cons(5);
	 while (not Mylist.isNil()) loop
	    {
	       print_list(Mylist);
	       Mylist <- Mylist.tail();
	    }
	 pool;
      }
   };

};



